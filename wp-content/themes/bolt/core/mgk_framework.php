<?php 
// call theme skins function 
require_once(MAGIKBOLT_THEME_PATH . '/includes/layout.php');
require_once(MAGIKBOLT_THEME_PATH . '/core/resize.php');
require_once(MAGIKBOLT_THEME_PATH . '/includes/mgk_menu.php');
require_once(MAGIKBOLT_THEME_PATH . '/includes/widget.php');
require_once(MAGIKBOLT_THEME_PATH . '/includes/mgk_widget.php');
require_once(MAGIKBOLT_THEME_PATH .'/core/social_share.php');

add_action('init','magikBolt_theme_layouts');
 /* Include theme variation functions */  

  if ( ! function_exists ( 'magikBolt_theme_layouts' ) ) {
 function magikBolt_theme_layouts()
 {
 global $bolt_Options;
 if (isset($bolt_Options['theme_layout']) && !empty($bolt_Options['theme_layout'])) {
include ( get_template_directory(). '/skins/' . $bolt_Options['theme_layout'] . '/functions.php');   
} else {
include ( get_template_directory(). '/skins/default/functions.php');   
}
 }
}


 /* Include theme variation header */  
   if ( ! function_exists ( 'magikBolt_theme_header' ) ) { 
   function magikBolt_theme_header()
 {
 global $bolt_Options; 

  if (isset($bolt_Options['theme_layout']) && !empty($bolt_Options['theme_layout'])) {
include(get_template_directory() . '/skins/' . $bolt_Options['theme_layout'] . '/header.php');
} else {
include(get_template_directory() . '/skins/default/header.php');
}
 }
}

/* Include theme variation homepage */ 

   if ( ! function_exists ( 'magikBolt_theme_homepage' ) ) { 
  function magikBolt_theme_homepage()
 {  
 global $bolt_Options;   
 if (isset($bolt_Options['theme_layout']) && !empty($bolt_Options['theme_layout'])) {
include(get_template_directory() . '/skins/' . $bolt_Options['theme_layout'] . '/homepage.php');
} else {
include(get_template_directory() . '/skins/default/homepage.php');
}
 }
}

 /* Include theme variation footer */ 

if ( ! function_exists ( 'magikBolt_theme_footer' ) ) { 
function magikBolt_theme_footer()
{
     
 global $bolt_Options;   
  if (isset($bolt_Options['theme_layout']) && !empty($bolt_Options['theme_layout'])) {
include(get_template_directory() . '/skins/' . $bolt_Options['theme_layout'] . '/footer.php');
} else {
include(get_template_directory() . '/skins/default/footer.php');
} 
}
}

 /* Include theme  backtotop */

 if ( ! function_exists ( 'magikBolt_backtotop' ) ) { 
  function magikBolt_backtotop()
 {
    
 global $bolt_Options;   
 if (isset($bolt_Options['back_to_top']) && !empty($bolt_Options['back_to_top'])) {
    ?>
   <script type="text/javascript">
    jQuery(document).ready(function($){ 
        jQuery().UItoTop();
    });
    </script>
<?php
}
 }
}


 if ( ! function_exists ( 'magikBolt_layout_breadcrumb' ) ) {
function magikBolt_layout_breadcrumb() {
$MagikBolt = new MagikBolt();
 global $bolt_Options; 

 if (isset($bolt_Options['theme_layout']) && $bolt_Options['theme_layout']=='version3')
  {
  ?>
<div class="page-heading">
  <div class="page-title">
       <h2>
       <?php $MagikBolt->magikBolt_page_title(); ?>
       </h2>
       </div>
       <div class="breadcrumbs">
            <div class="container">
              <div class="row">
                <div class="col-xs-12">
                   <?php $MagikBolt->magikBolt_breadcrumbs(); ?>
              </div>
            </div>
           </div>
       </div>    
</div>
     
<?php 
}
else
{ 
?>
 <div class="breadcrumbs">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
          <?php $MagikBolt->magikBolt_breadcrumbs(); ?>
          </div>
          <!--col-xs-12--> 
        </div>
        <!--row--> 
      </div>
      <!--container--> 
    </div>
<?php

}

}
}


 if ( ! function_exists ( 'magikBolt_singlepage_breadcrumb' ) ) {
function magikBolt_singlepage_breadcrumb() {
 $MagikBolt = new MagikBolt();
 global $bolt_Options; 

 if (isset($bolt_Options['theme_layout']) && $bolt_Options['theme_layout']=='version3')
{
?>

<div class="page-heading"> 
       <div class="breadcrumbs">
            <div class="container">
              <div class="row">
                <div class="col-xs-12">
                   <?php $MagikBolt->magikBolt_breadcrumbs(); ?>
              </div>
            </div>
           </div>
       </div>    
</div>
     
<?php }
else{ ?>
 <div class="breadcrumbs">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
          <?php $MagikBolt->magikBolt_breadcrumbs(); ?>
          </div>
          <!--col-xs-12--> 
        </div>
        <!--row--> 
      </div>
      <!--container--> 
    </div>


<?php

}

}
}


if ( ! function_exists ( 'magikBolt_simple_product_link' ) ) {
function magikBolt_simple_product_link()
{
  global $product,$class,$bolt_Options;
  $product_type = $product->get_type();
  $product_id=$product->get_id();
  
   if (isset($bolt_Options['theme_layout']) && $bolt_Options['theme_layout']=='version3' )
 {  /*version3*/ 
   if($product->get_price() =='')
  { ?>
    <a class="button btn-cart" data-toggle="tooltip" data-placement="right" title='<?php echo esc_html($product->add_to_cart_text()); ?>'
       onClick='window.location.assign("<?php echo esc_js(get_permalink($product_id)); ?>")' >
    <span><?php echo esc_html($product->add_to_cart_text()); ?> </span>
    </a>
   <?php  }
   else{
  ?>
   <div class="add_cart" data-placement="right" title='<?php echo esc_html($product->add_to_cart_text()); ?>'  data-toggle="tooltip">
   <a class="single_add_to_cart_button add_to_cart_button  product_type_simple ajax_add_to_cart button btn-cart" data-quantity="1" data-product_id="<?php echo esc_attr($product->get_id()); ?>"
      href='<?php echo esc_url($product->add_to_cart_url()); ?>'>
    <span><?php echo esc_html($product->add_to_cart_text()); ?> </span>
    </a>
  </div>
  <?php
  }
 }

 else
 {/*default,version2 and version4*/
   if($product->get_price() =='')
  { ?>
    <a class="button btn-cart" title='<?php echo esc_html($product->add_to_cart_text()); ?>'
       onClick='window.location.assign("<?php echo esc_js(get_permalink($product_id)); ?>")' >
    <span><?php echo esc_html($product->add_to_cart_text()); ?> </span>
    </a>
   <?php  }
   else{
  ?>
   <a class="single_add_to_cart_button add_to_cart_button  product_type_simple ajax_add_to_cart button btn-cart" title='<?php echo esc_html($product->add_to_cart_text()); ?>' data-quantity="1" data-product_id="<?php echo esc_attr($product->get_id()); ?>"
      href='<?php echo esc_url($product->add_to_cart_url()); ?>'>
    <span><?php echo esc_html($product->add_to_cart_text()); ?> </span>
    </a>
  <?php
  }
 }

 
 
}
}

if ( ! function_exists ( 'magikBolt_allowedtags' ) ) {
function magikBolt_allowedtags() {
    // Add custom tags to this string
        return '<script>,<style>,<br>,<em>,<i>,<ul>,<ol>,<li>,<a>,<p>,<img>,<video>,<audio>,<h1>,<h2>,<h3>,<h4>,<h5>,<h6>,<b>,<blockquote>,<strong>,<figcaption>'; 
    }
}

if ( ! function_exists( 'magikBolt_wp_trim_excerpt' ) ) : 

    function magikBolt_wp_trim_excerpt($wpse_excerpt) {
    $raw_excerpt = $wpse_excerpt;
        if ( '' == $wpse_excerpt ) {

            $wpse_excerpt = get_the_content('');
            $wpse_excerpt = strip_shortcodes( $wpse_excerpt );
            $wpse_excerpt = apply_filters('the_content', $wpse_excerpt);
            $wpse_excerpt = str_replace(']]>', ']]&gt;', $wpse_excerpt);
            $wpse_excerpt = strip_tags($wpse_excerpt, magikBolt_allowedtags()); /*IF you need to allow just certain tags. Delete if all tags are allowed */

            //Set the excerpt word count and only break after sentence is complete.
                $excerpt_word_count = 75;
                $excerpt_length = apply_filters('excerpt_length', $excerpt_word_count); 
                $tokens = array();
                $excerptOutput = '';
                $count = 0;

                // Divide the string into tokens; HTML tags, or words, followed by any whitespace
                preg_match_all('/(<[^>]+>|[^<>\s]+)\s*/u', $wpse_excerpt, $tokens);

                foreach ($tokens[0] as $token) { 

                    if ($count >= $excerpt_length && preg_match('/[\,\;\?\.\!]\s*$/uS', $token)) { 
                    // Limit reached, continue until , ; ? . or ! occur at the end
                        $excerptOutput .= trim($token);
                        break;
                    }

                    // Add words to complete sentence
                    $count++;

                    // Append what's left of the token
                    $excerptOutput .= $token;
                }

            $wpse_excerpt = trim(force_balance_tags($excerptOutput));

                $excerpt_end = ' '; 
                $excerpt_more = apply_filters('excerpt_more', ' ' . $excerpt_end); 

                $wpse_excerpt .= $excerpt_more; /*Add read more in new paragraph */

            return $wpse_excerpt;   

        }
        return apply_filters('magikBolt_wp_trim_excerpt', $wpse_excerpt, $raw_excerpt);
    }

endif; 

remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'magikBolt_wp_trim_excerpt');

if ( ! function_exists ( 'magikBolt_disable_srcset' ) ) {
function magikBolt_disable_srcset( $sources ) {
return false;
}
}
add_filter( 'wp_calculate_image_srcset', 'magikBolt_disable_srcset' );

if ( ! function_exists ( 'magikBolt_body_classes' ) ) {
function magikBolt_body_classes( $classes ) 
{
  // Adds a class to body.
global $bolt_Options; 

if ((is_front_page() && is_home()) || is_front_page())
{
  $classes[]='cms-index-index cms-bolt-home cms-home-page';
}
else
{
  $classes[]='cms-index-index cms-bolt-other';
}

  return $classes;
}
}

add_filter( 'body_class', 'magikBolt_body_classes');

if ( ! function_exists ( 'magikBolt_post_classes' ) ) {
function magikBolt_post_classes( $classes ) 
{
  // add custom post classes.
if(class_exists('WooCommerce') && is_woocommerce())
{ 
$classes[]='notblog';
if(is_product_category())
{
 $classes[]='notblog'; 
} 
}
else if(is_category() || is_archive() || is_search() || is_tag() || is_home())
{
$classes[] = 'blog-post container-paper';
}
else
{
$classes[]='notblog';
} 

  return $classes;
}
}
add_filter( 'post_class', 'magikBolt_post_classes');


if ( ! function_exists ( 'magikBolt_get_link_url' ) ) {
function magikBolt_get_link_url() {
  $has_url = get_url_in_content( get_the_content() );

  return $has_url ? $has_url : apply_filters( 'the_permalink', get_permalink() );
}
}


  // magik mini cart
  function magikBolt_mini_cart()
{
    global $woocommerce,$bolt_Options;

    ?>
<div class="mini-cart">

   <?php  if (isset($bolt_Options['theme_layout']) && $bolt_Options['theme_layout']=='version4') { ?>

      <div data-hover="dropdown"  class="basket dropdown-toggle">
         <a href="<?php echo esc_url(wc_get_cart_url()); ?>"> 
          <?php  esc_attr_e('My Cart','bolt'); ?>
          <span class="cart_count"><?php echo esc_html($woocommerce->cart->cart_contents_count); ?> </span></a>
      </div>


   <?php } else {?>

      <div data-hover="dropdown"  class="basket dropdown-toggle">
         <a href="<?php echo esc_url(wc_get_cart_url()); ?>"> 
          <span class="cart_count"><?php echo esc_html($woocommerce->cart->cart_contents_count); ?> </span>
          <span class="price"><?php  esc_attr_e('My Cart','bolt'); ?> /
          <?php echo htmlspecialchars_decode(WC()->cart->get_cart_subtotal()); ?></span> </a>
      </div>

    <?php } ?>
<div>    
  <div class="top-cart-content" <?php  if (isset($bolt_Options['theme_layout']) && $bolt_Options['theme_layout']=='version3')
                       {?> style="display: none;"<?php  } ?>>


           <?php if (isset($bolt_Options['theme_layout']) && $bolt_Options['theme_layout']=='version3')
                       {?>  
          <?php if (sizeof(WC()->cart->get_cart()) > 0) : $i = 0; ?>        
         <div class="actions">
                      <button class="btn-checkout" title="<?php esc_attr_e('Checkout','bolt') ;?>" type="button" 
                      onClick="window.location.assign('<?php echo esc_js(wc_get_checkout_url()); ?>')">
                      <span><?php esc_attr_e('Checkout','bolt') ;?></span> </button>

                      <a class="view-cart" type="button"
                     onClick="window.location.assign('<?php echo esc_js(wc_get_cart_url()); ?>')">
                     <span><?php esc_attr_e('View Cart','bolt') ;?></span> </a>
                     
          
         </div>  
         <?php endif; ?> 
         <?php } ?>

         <?php  if (isset($bolt_Options['theme_layout']) && $bolt_Options['theme_layout']=='version4')
               {?>
                
                <div class="block-subtitle">
                <div class="top-subtotal"><?php echo esc_html($woocommerce->cart->cart_contents_count); ?>  <?php  esc_attr_e('items','bolt'); ?> , <span class="price"><?php echo htmlspecialchars_decode(WC()->cart->get_cart_subtotal()); ?></span> </div>
                 </div>

        <?php } ?>



         <?php if (sizeof(WC()->cart->get_cart()) > 0) : $i = 0; ?>
         <ul class="mini-products-list" id="cart-sidebar" >
            <?php foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) : ?>
            <?php
               $_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
               $product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);
               
               if ($_product && $_product->exists() && $cart_item['quantity'] > 0
                   && apply_filters('woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key)
               ) :
               
               
                   $product_name      = apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key );
                 $thumbnail         = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
                   $product_price     = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
          $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
                   $cnt = sizeof(WC()->cart->get_cart());
                   $rowstatus = $cnt % 2 ? 'odd' : 'even';
                   ?>
            <li class="item<?php if ($cnt - 1 == $i) { ?>last<?php } ?>">
              <div class="item-inner">
               <a class="product-image"
                  href="<?php echo esc_url($product_permalink); ?>"  title="<?php echo esc_html($product_name); ?>"> <?php echo str_replace(array('http:', 'https:'), '', htmlspecialchars_decode($thumbnail)); ?> </a>
             

                  <div class="product-details">
                       <div class="access">
                        <a class="btn-edit" title="<?php esc_attr_e('Edit item','bolt') ;?>"
                        href="<?php echo esc_url(wc_get_cart_url()); ?>"><i
                        class="icon-pencil"></i><span
                        class="hidden"><?php esc_attr_e('Edit item','bolt') ;?></span></a>
               <a href="<?php echo esc_url(wc_get_cart_remove_url($cart_item_key)); ?>"
                        title="<?php esc_attr_e('Remove This Item','bolt') ;?>" onClick="" 
                        class="btn-remove1"><?php esc_attr_e('Remove','bolt') ;?></a> 

                         </div>
                      <strong><?php echo esc_html($cart_item['quantity']); ?>
                  </strong> x <span class="price"><?php echo htmlspecialchars_decode($product_price); ?></span>
                     <p class="product-name"><a href="<?php echo esc_url($product_permalink); ?>"
                        title="<?php echo esc_html($product_name); ?>"><?php echo esc_html($product_name); ?></a> </p>
                  </div>
                 <?php echo wc_get_formatted_cart_item_data( $cart_item ); ?>
                     </div>
              
            </li>
            <?php endif; ?>
            <?php $i++; endforeach; ?>
         </ul> 
         <!--actions-->
        
        <?php if (isset($bolt_Options['theme_layout']) && ($bolt_Options['theme_layout']=='default' || $bolt_Options['theme_layout']=='version2' || $bolt_Options['theme_layout']=='version4'))
                       {?>   
          <?php if (sizeof(WC()->cart->get_cart()) > 0) : $i = 0; ?>       
         <div class="actions">
                      <button class="btn-checkout" title="<?php esc_attr_e('Checkout','bolt') ;?>" type="button" 
                      onClick="window.location.assign('<?php echo esc_js(wc_get_checkout_url()); ?>')">
                      <span><?php esc_attr_e('Checkout','bolt') ;?></span> </button>

                      <a class="view-cart" type="button"
                    onClick="window.location.assign('<?php echo esc_js(wc_get_cart_url()); ?>')">
                     <span><?php esc_attr_e('View Cart','bolt') ;?></span> </a>
                     
          
         </div>  
          <?php endif; ?> 
         <?php } ?>
         
         <?php else:?>
         <p class="a-center noitem">
            <?php esc_attr_e('Sorry, nothing in cart.', 'bolt');?>
         </p>
         <?php endif; ?>
      
   </div>
 </div>
</div>
<?php
}
 

 
  //social links
  function magikBolt_social_media_links()
  {
    global $bolt_Options;
    if(isset($bolt_Options
  ['enable_social_link_footer']) && !empty($bolt_Options['enable_social_link_footer']))
    {?>
 <div class="social">

  <h4><?php esc_attr_e('Follow Us', 'bolt'); ?></h4>
  
  <ul>
  <?php
    if (isset($bolt_Options
  ['social_facebook']) && !empty($bolt_Options['social_facebook'])) {
      echo "<li class=\"fb pull-left\"><a target=\"_blank\" href='".  esc_url($bolt_Options['social_facebook']) ."'></a></li>";
    }

    if (isset($bolt_Options['social_twitter']) && !empty($bolt_Options['social_twitter'])) {
      echo "<li class=\"tw pull-left\"><a target=\"_blank\" href='".  esc_url($bolt_Options['social_twitter']) ."'></a></li>";
    }

    if (isset($bolt_Options['social_googlep']) && !empty($bolt_Options['social_googlep'])) {
      echo "<li class=\"googleplus pull-left\"><a target=\"_blank\" href='".  esc_url($bolt_Options['social_googlep'])."'></a></li>";
    }

    if (isset($bolt_Options['social_rss']) && !empty($bolt_Options['social_rss'])) {
      echo "<li class=\"rss pull-left\"><a target=\"_blank\" href='".  esc_url($bolt_Options['social_rss'])."'></a></li>";
    }

    if (isset($bolt_Options['social_pinterest']) && !empty($bolt_Options['social_pinterest'])) {
      echo "<li class=\"pintrest pull-left\"><a target=\"_blank\" href='".  esc_url($bolt_Options['social_pinterest'])."'></a></li>";
    }

    if (isset($bolt_Options['social_linkedin']) && !empty($bolt_Options['social_linkedin'])) {
      echo "<li class=\"linkedin pull-left\"><a target=\"_blank\" href='".  esc_url($bolt_Options['social_linkedin'])."'></a></li>";
    }
     if (isset($bolt_Options['social_instagram']) && !empty($bolt_Options['social_instagram'])) {
      echo "<li class=\"instagram pull-left\"><a target=\"_blank\" href='".  esc_url($bolt_Options['social_instagram'])."'></a></li>";
    }
    if (isset($bolt_Options['social_youtube']) && !empty($bolt_Options['social_youtube'])) {
      echo "<li class=\"youtube pull-left\"><a target=\"_blank\" href='".  esc_url($bolt_Options['social_youtube'])."'></a></li>";
    }
    ?>
 </ul>
 </div>

 <?php }
  }


  //add to cart function
function magikBolt_woocommerce_product_add_to_cart_text() {
    global $product,$bolt_Options;
    $product_type = $product->get_type();
    $product_id=$product->get_id();

 if (isset($bolt_Options['theme_layout']) && $bolt_Options['theme_layout']=='version3' )
 {  /*version3*/  

    if($product->is_in_stock())
    {
    switch ( $product_type ) {
    case 'external':
    ?>
     <div class="add_cart" data-placement="right" title='<?php echo esc_html($product->add_to_cart_text()); ?>'  data-toggle="tooltip">
    <a class="button btn-cart"  onClick='window.location.assign("<?php echo esc_js(get_permalink($product_id)); ?>")'>
    <span> <?php echo esc_html($product->add_to_cart_text()); ?></span>
    </a>
  </div>
    <?php
       break;
       case 'grouped':
        ?>
       <div class="add_cart" data-placement="right" title='<?php echo esc_html($product->add_to_cart_text()); ?>'  data-toggle="tooltip">  
    <a class="button btn-cart" data-toggle="tooltip" data-placement="right" title='<?php echo esc_html($product->add_to_cart_text()); ?>'
       onClick='window.location.assign("<?php echo esc_js(get_permalink($product_id)); ?>")' >
    <span><?php echo esc_html($product->add_to_cart_text()); ?> </span>
    </a>
      </div>
    <?php
       break;
       case 'simple':
        ?>
    <?php magikBolt_simple_product_link();?>
    <?php
       break;
       case 'variable':
        ?>
    <div class="add_cart" data-placement="right" title='<?php echo esc_html($product->add_to_cart_text()); ?>'  data-toggle="tooltip">
    <a class="button btn-cart" data-toggle="tooltip" data-placement="right" title='<?php echo esc_html($product->add_to_cart_text()); ?> '
       onClick='window.location.assign("<?php echo esc_js(get_permalink($product_id)); ?>")'>
    <span>
    <?php echo esc_html($product->add_to_cart_text()); ?>
    </span> 
    </a>
  </div>
    <?php
       break;
       default:
        ?>
      <div class="add_cart" data-placement="right" title='<?php esc_attr_e("Read more",'bolt'); ?>'  data-toggle="tooltip">   
    <a class="button btn-cart" data-toggle="tooltip" data-placement="right" title='<?php esc_attr_e("Read more",'bolt'); ?>'
       onClick='window.location.assign("<?php echo esc_js(get_permalink($product_id)); ?>")'>
    <span><?php esc_attr_e('Read more', 'bolt'); ?></span> 
    </a>
  </div>
    <?php
       break;
       
       }
       }
       else
       {
       ?>
          <div class="add_cart" data-placement="right" title='<?php esc_attr_e('Out of stock', 'bolt'); ?>'  data-toggle="tooltip">  
    <a type='button' class="button btn-cart" data-toggle="tooltip" data-placement="right" title='<?php esc_attr_e('Out of stock', 'bolt'); ?>'
       onClick='window.location.assign("<?php echo esc_js(get_permalink($product_id)); ?>")'
       class='button btn-cart'>
    <span> <?php esc_attr_e('Out of stock', 'bolt'); ?> </span>
    </a>
      </div>
    <?php
    }
 }
 else
 {
   if($product->is_in_stock())
    {
    switch ( $product_type ) {
    case 'external':
    ?>
    <a class="button btn-cart" title='<?php echo esc_html($product->add_to_cart_text()); ?>'
       onClick='window.location.assign("<?php echo esc_js(get_permalink($product_id)); ?>")'>
    <span> <?php echo esc_html($product->add_to_cart_text()); ?></span>
    </a>
    <?php
       break;
       case 'grouped':
        ?>
    <a class="button btn-cart" title='<?php echo esc_html($product->add_to_cart_text()); ?>'
       onClick='window.location.assign("<?php echo esc_js(get_permalink($product_id)); ?>")' >
    <span><?php echo esc_html($product->add_to_cart_text()); ?> </span>
    </a>
    <?php
       break;
       case 'simple':
        ?>
    <?php magikBolt_simple_product_link();?>
    <?php
       break;
       case 'variable':
        ?>
    <a class="button btn-cart"  title='<?php echo esc_html($product->add_to_cart_text()); ?>'
       onClick='window.location.assign("<?php echo esc_js(get_permalink($product_id)); ?>")'>
    <span>
    <?php echo esc_html($product->add_to_cart_text()); ?>
    </span> 
    </a>
    <?php
       break;
       default:
        ?>
    <a class="button btn-cart" title='<?php esc_attr_e("Read more",'bolt'); ?>'
       onClick='window.location.assign("<?php echo esc_js(get_permalink($product_id)); ?>")'>
    <span><?php esc_attr_e('Read more', 'bolt'); ?></span> 
    </a>
    <?php
       break;
       
       }
       }
       else
       {
       ?>
    <a type='button' class="button btn-cart" title='<?php esc_attr_e('Out of stock', 'bolt'); ?> '
       onClick='window.location.assign("<?php echo esc_js(get_permalink($product_id)); ?>")'
       class='button btn-cart'>
    <span> <?php esc_attr_e('Out of stock', 'bolt'); ?> </span>
    </a>
    <?php
    }
 }
 
   
 
}


 // bottom cpyright text 
  function magikBolt_footer_text()
  {
    global $bolt_Options;
    if (isset($bolt_Options['bottom-footer-text']) && !empty($bolt_Options['bottom-footer-text'])) {
      echo htmlspecialchars_decode ($bolt_Options['bottom-footer-text']);
    }
  }


function  magikBolt_curPageURL() {
            $pageURL = 'http';
            if(isset($_SERVER["HTTPS"]))
            if ($_SERVER["HTTPS"] == "on") {
                $pageURL .= "s";
            }
            $pageURL .= "://";
            if ($_SERVER["SERVER_PORT"] != "80") {
                $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
            } else {
                $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
            }
            return $pageURL;
        }

?>