<?php
    /**
     * ReduxFramework Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }


    // This is your option name where all the Redux data is stored.
    $opt_name = "mgk_option";

    // This line is only for altering the demo. Can be easily removed.
    $opt_name = apply_filters( 'redux_demo/opt_name', $opt_name );

    /*
     *
     * --> Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
     *
     */

    $sampleHTML = '';
    if ( file_exists( dirname( __FILE__ ) . '/info-html.html' ) ) {
        Redux_Functions::initWpFilesystem();

        global $wp_filesystem;

        $sampleHTML = $wp_filesystem->get_contents( dirname( __FILE__ ) . '/info-html.html' );
    }

    // Background Patterns Reader
    $sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
    $sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
    $sample_patterns      = array();
    
    if ( is_dir( $sample_patterns_path ) ) {

        if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) {
            $sample_patterns = array();

            while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

                if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                    $name              = explode( '.', $sample_patterns_file );
                    $name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                    $sample_patterns[] = array(
                        'alt' => $name,
                        'img' => $sample_patterns_url . $sample_patterns_file
                    );
                }
            }
        }
    }

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title' => esc_html__('Bolt Options', 'bolt'),
        'page_title' => esc_html__('Bolt Options', 'bolt'),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => 'bolt_Options',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => false,
        // Show the time the page took to load, etc
        'update_notice'        => true,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
   
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '',
        // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'red',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

   



    // Panel Intro text -> before the form
    if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
        if ( ! empty( $args['global_variable'] ) ) {
            $v = $args['global_variable'];
        } else {
            $v = str_replace( '-', '_', $args['opt_name'] );
        }
        $args['intro_text'] = sprintf(__( '<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', 'bolt' ), $v );
    } else {
        $args['intro_text'] = __( '<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'bolt' );
    }

    // Add content after the form.
    $args['footer_text'] = __( '<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', 'bolt' );

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */


    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => esc_html__( 'Theme Information 1', 'bolt' ),
            'content' => esc_html__( '<p>This is the tab content, HTML is allowed.</p>', 'bolt' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => esc_html__( 'Theme Information 2', 'bolt' ),
            'content' => esc_html__( '<p>This is the tab content, HTML is allowed.</p>', 'bolt' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = esc_html__( '<p>This is the sidebar content, HTML is allowed.</p>', 'bolt' );
    Redux::setHelpSidebar( $opt_name, $content );


    /*
     * <--- END HELP TABS
     */


    /*
     *
     * ---> START SECTIONS
     *
     */

    /*

        As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for


     */
  global $woocommerce;
               $cat_arg=array();
               $cat_data='';
                if(class_exists('WooCommerce')) {
                   
                     $cat_data='terms';
                    $cat_arg=array('taxonomies'=>'product_cat', 'args'=>array());
                }
    // -> START Basic Fields
    Redux::setSection( $opt_name, array(
                'title' => esc_html__('Home Settings', 'bolt'),
                'desc' => esc_html__('Home page settings ', 'bolt'),
                'icon' => 'el-icon-home',
                // 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
                'fields' => array( 

                    array(
                        'id' => 'theme_layout',
                        'type' => 'image_select',                        
                        'title' => esc_html__('Theme Variation', 'bolt'),
                        'subtitle' => esc_html__('Select the variation you want to apply on your store.', 'bolt'),
                        'options' => array(
                            'default' => array(
                                'title' => esc_html__('Default', 'bolt'),
                                'alt' => esc_html__('Default', 'bolt'),
                                'img' => get_template_directory_uri() . '/images/variations/version1.jpg'
                            ),
                            'version2' => array(
                                'title' => esc_html__('Version2', 'bolt'),
                                'alt' => esc_html__('Version 2', 'bolt'),
                                'img' => get_template_directory_uri() . '/images/variations/version2.jpg'
                            ),
                            'version3' => array(
                                'title' => esc_html__('Version3', 'bolt'),
                                'alt' => esc_html__('Version 3', 'bolt'),
                                'img' => get_template_directory_uri() . '/images/variations/version3.jpg'
                            ),
                            'version4' => array(
                                'title' => esc_html__('Version4', 'bolt'),
                                'alt' => esc_html__('Version 4', 'bolt'),
                                'img' => get_template_directory_uri() . '/images/variations/version4.jpg'
                            ),
                                                    
                           
                        ),
                        'default' => 'default'
                    ),  
                      array(
                        'id' => 'enable_welcome_msg',
                        'type' => 'switch',
                        'title' => esc_html__('Enable welcome message', 'bolt'),
                        'subtitle' => esc_html__('You can enable/disable welcome message', 'bolt')
                    ), 
                    array(
                        'id' => 'welcome_msg',
                        'type' => 'text',
                        'required' => array('enable_welcome_msg', '=', '1'),
                        'title' => esc_html__('Enter your welcome message here', 'bolt'),
                        'subtitle' => esc_html__('Enter your welcome message here.', 'bolt'),
                        'desc' => esc_html__('', 'bolt'),
                        
                    
                         ),             
                    array(
                        'id' => 'enable_home_gallery',
                        'type' => 'switch',
                        'title' => esc_html__('Enable Home Page Gallery', 'bolt'),
                        'subtitle' => esc_html__('You can enable/disable Home page Gallery', 'bolt')
                    ),

                    array(
                        'id' => 'home-page-slider',
                        'type' => 'slides',
                        'title' => esc_html__('Home Slider Uploads', 'bolt'),
                        'required' => array('enable_home_gallery', '=', '1'),
                        'subtitle' => esc_html__('Unlimited slide uploads with drag and drop sortings.', 'bolt'),
                        'placeholder' => array(
                            'title' => esc_html__('This is a title', 'bolt'),
                            'description' => esc_html__('Description Here', 'bolt'),
                            'url' => esc_html__('Give us a link!', 'bolt'),
                        ),

                    ),
                
                    array(
                        'id' => 'enable_home_page_side_banner',
                        'type' => 'switch',
                        'required' => array('theme_layout', '=', 'default'),
                        'title' => esc_html__('Enable Home Page Side Banner', 'bolt'),
                        'subtitle' => esc_html__('You can enable/disable Home Page Side Banner', 'bolt')
                    ),

                    array(
                        'id' => 'home_side_banner',
                        'type' => 'media',
                        'required' => array('enable_home_page_side_banner', '=', '1'),
                        'title' => esc_html__('Home Page Side Banner', 'bolt'),
                        'desc' => esc_html__('', 'bolt'),
                        'subtitle' => esc_html__('Upload Home Page Side Banner image', 'bolt'),
                    ),

                    array(
                        'id' => 'home_side_banner_url',
                        'type' => 'text',
                        'required' => array('enable_home_page_side_banner', '=', '1'),
                        'title' => esc_html__('Home Page Side Banner URL', 'bolt'),
                        'subtitle' => esc_html__('URL For Home Page Side Banner.', 'bolt'),
                    ),
                   
                  array(
                        'id' => 'enable_home_page_sidebar_banner',
                        'type' => 'switch',
                        'required' => array('theme_layout', '=', 'version4'),
                        'title' => esc_html__('Enable Home Page Sidebar Banner', 'bolt'),
                        'subtitle' => esc_html__('You can enable/disable Home Page Sidebar Banner', 'bolt')
                    ),

                    array(
                        'id' => 'home_sidebar_banner',
                        'type' => 'media',
                        'required' => array('enable_home_page_sidebar_banner', '=', '1'),
                        'title' => esc_html__('Home Page Sidebar Banner', 'bolt'),
                        'desc' => esc_html__('', 'bolt'),
                        'subtitle' => esc_html__('Upload Home Page Sidebar Banner image', 'bolt'),
                    ),

                    array(
                        'id' => 'home_sidebar_banner_url',
                        'type' => 'text',
                        'required' => array('enable_home_page_sidebar_banner', '=', '1'),
                        'title' => esc_html__('Home Page Sidebar Banner URL', 'bolt'),
                        'subtitle' => esc_html__('URL For Home Page Sidebar Banner.', 'bolt'),
                    ),


                   
                    array(
                        'id' => 'enable_home_sub_banners',
                        'type' => 'switch',
                        'required' => array(array('theme_layout', '!=', 'version3'),array('theme_layout', '!=', 'version4')),
                        'title' => esc_html__('Enable Home Page Slider sub Banners', 'bolt'),
                        'subtitle' => esc_html__('You can enable/disable Home page Slider Bottom Banners', 'bolt')
                    ),

                    array(
                        'id' => 'home-sub-banner1',
                        'type' => 'media',
                        'required' => array('enable_home_sub_banners', '=', '1'),
                        'title' => esc_html__('Home Sub Banner 1', 'bolt'),
                        'desc' => esc_html__('', 'bolt'),
                        'subtitle' => esc_html__('Upload banner to appear on bottom of slider', 'bolt'),
                    ),

                    array(
                        'id' => 'home-sub-banner1-url',
                        'type' => 'text',
                        'required' => array('enable_home_sub_banners', '=', '1'),
                        'title' => esc_html__('Home Sub Banner 1 URL', 'bolt'),
                        'subtitle' => esc_html__('URL for the banner.', 'bolt'),
                    ),

                     array(
                        'id' => 'home-sub-banner1-text',
                        'type' => 'text',
                        'required' => array('enable_home_sub_banners', '=', '1'),
                        'title' => esc_html__('Home slider sub Banner-1 text', 'bolt'),
                        'subtitle' => esc_html__('Title for the banner-1.', 'bolt'),
                    ),
                    

                     array(
                        'id' => 'home-sub-banner2-text',
                        'type' => 'text',
                        'required' => array('enable_home_sub_banners', '=', '1'),
                        'title' => esc_html__('Home slider sub Banner-2 text', 'bolt'),
                        'subtitle' => esc_html__('Title for the banner-2.', 'bolt'),
                    ),

                    array(
                        'id' => 'home-sub-banner3',
                        'type' => 'media',
                        'required' => array('enable_home_sub_banners', '=', '1'),
                        'title' => esc_html__('Home Sub Banner 3', 'bolt'),
                        'desc' => esc_html__('', 'bolt'),
                        'subtitle' => esc_html__('Upload banner to appear on bottom of slider', 'bolt'),
                    ),

                    array(
                        'id' => 'home-sub-banner3-url',
                        'type' => 'text',
                        'required' => array('enable_home_sub_banners', '=', '1'),
                        'title' => esc_html__('Home Sub Banner 3 URL', 'bolt'),
                        'subtitle' => esc_html__('URL for the banner.', 'bolt'),
                    ),

                     array(
                        'id' => 'home-sub-banner4-text',
                        'type' => 'text',
                        'required' => array('enable_home_sub_banners', '=', '1'),
                        'title' => esc_html__('Home slider sub Banner-4 text', 'bolt'),
                        'subtitle' => esc_html__('Title for the banner-4.', 'bolt'),
                    ), 

                    array(
                        'id' => 'home-sub-banner5',
                        'type' => 'media',
                        'required' => array('enable_home_sub_banners', '=', '1'),
                        'title' => esc_html__('Home Sub Banner 5', 'bolt'),
                        'desc' => esc_html__('', 'bolt'),
                        'subtitle' => esc_html__('Upload banner to appear on bottom of slider', 'bolt'),
                    ),

                    array(
                        'id' => 'home-sub-banner5-url',
                        'type' => 'text',
                        'required' => array('enable_home_sub_banners', '=', '1'),
                        'title' => esc_html__('Home Sub Banner 5 URL', 'bolt'),
                        'subtitle' => esc_html__('URL for the banner.', 'bolt'),
                    ),
                    

                     array(
                        'id' => 'home-sub-banner5-text',
                        'type' => 'text',
                        'required' => array('enable_home_sub_banners', '=', '1'),
                        'title' => esc_html__('Home slider sub Banner-5 text', 'bolt'),
                        'subtitle' => esc_html__('Title for the banner-5.', 'bolt'),
                    ),

                  array(
                        'id' => 'enable_home_offer_banners',
                        'type' => 'switch',
                        'required' => array('theme_layout', '=', 'version3'),              
                        'title' => esc_html__('Enable Home Page Offer Banners', 'bolt'),
                        'subtitle' => esc_html__('You can enable/disable Home page offer Banners', 'bolt')
                    ),
                    array(
                        'id' => 'home-offer-banner1',
                        'type' => 'media',
                        'required' => array('enable_home_offer_banners', '=', '1'),
                        'title' => esc_html__('Home offer Banner 1', 'bolt'),
                        'desc' => esc_html__('', 'bolt'),
                        'subtitle' => esc_html__('Upload offer banner to appear on  home page', 'bolt'),                                    
                    ),   
                    array(
                        'id' => 'home-offer-banner1-url',
                        'type' => 'text',
                        'required' => array('enable_home_offer_banners', '=', '1'),
                        'title' => esc_html__('Home offer Banner-1 URL', 'bolt'),
                        'subtitle' => esc_html__('URL for the offer banner.', 'bolt'),
                    ), 
                    array(
                        'id' => 'home-offer-banner2',
                        'type' => 'media',
                        'required' => array('enable_home_offer_banners', '=', '1'),
                        'title' => esc_html__('Home offer Banner 2', 'bolt'),
                        'desc' => esc_html__('', 'bolt'),
                        'subtitle' => esc_html__('Upload offer banner to appear on  home page', 'bolt')
                    ),
                    array(
                        'id' => 'home-offer-banner2-url',
                        'type' => 'text',
                        'required' => array('enable_home_offer_banners', '=', '1'),
                        'title' => esc_html__('Home offer Banner-2 URL', 'bolt'),
                        'subtitle' => esc_html__('URL for the offer banner.', 'bolt'),
                    ),                     
                    array(
                        'id' => 'home-offer-banner3',
                        'type' => 'media',
                        'required' => array('enable_home_offer_banners', '=', '1'),
                        'title' => esc_html__('Home offer Banner 3', 'bolt'),
                        'desc' => esc_html__('', 'bolt'),
                        'subtitle' => esc_html__('Upload offer banner to appear on  home page', 'bolt')
                    ),
                    array(
                        'id' => 'home-offer-banner3-url',
                        'type' => 'text',
                        'required' => array('enable_home_offer_banners', '=', '1'),
                        'title' => esc_html__('Home offer Banner-3 URL', 'bolt'),
                        'subtitle' => esc_html__('URL for the offer banner.', 'bolt'),
                    ),

                   array(
                        'id' => 'enable_home_offer_slider',
                        'type' => 'switch',
                        'required' => array('theme_layout', '=', 'version3'),              
                        'title' => esc_html__('Enable Home Page Full banner', 'bolt'),
                        'subtitle' => esc_html__('You can enable/disable Home page Full banner', 'bolt')
                    ),
                       array(
                            'id' => 'home_offer_slider_image',
                            'type' => 'media',
                            'required' => array('enable_home_offer_slider', '=', '1'),
                            'title' => esc_html__('Home page Full banner background image', 'bolt'),
                            'desc' => esc_html__('', 'bolt'),
                            'subtitle' => esc_html__('Upload Home page Full banner background image', 'bolt')
                    ),
                   
                   array(
                        'id' => 'home_offer_slider_text',
                        'type' => 'text',
                        'required' => array('enable_home_offer_slider', '=', '1'),
                        'title' => esc_html__('Home  Offer Slider Text', 'bolt'),
                        'subtitle' => esc_html__('Text for the  Offer Slider.', 'bolt'),
                    ), 
                     
                   array(
                        'id' => 'home_offer_slider_url',
                        'type' => 'text',
                        'required' => array('enable_home_offer_slider', '=', '1'),
                        'title' => esc_html__('Home offer Offer Slider URL', 'bolt'),
                        'subtitle' => esc_html__('URL for the Offer Slider.', 'bolt'),
                    ), 
             
                        
                  array(
                        'id' => 'enable_home_hotdeal_products',
                        'type' => 'switch',
                        'required' => array('theme_layout', '!=', 'version3'),
                        'title' => esc_html__('Show Hot Deal Product', 'bolt'),
                        'subtitle' => esc_html__('You can show Hot Deal product on home page.', 'bolt')
                    ),

                       array(
                        'id' => 'enable_home_hotdeal_on_products_page',
                        'type' => 'switch',
                        
                        'title' => esc_html__('Show Hot Deal on Product Detail Page', 'bolt'),
                        'subtitle' => esc_html__('You can show Hot Deal on Product Detail page.', 'bolt')
                    ),
                                         
                        
                    
                     array(
                            'id' => 'header_breadcrumb',
                            'type' => 'media',
                            'required' => array('theme_layout', '=', 'version3'),
                            'title' => esc_html__('Header breadcrumb image', 'bolt'),
                            'desc' => esc_html__('', 'bolt'),
                            'subtitle' => esc_html__('Upload Header breadcrumb image', 'bolt')
                    ),

                     array(
                        'id' => 'enable_footer_banner_section',
                        'type' => 'switch',
                        'required' => array('theme_layout', '=', 'version4'),
                        'title' => esc_html__('Enable Footer Banner Section', 'bolt'),
                        'subtitle' => esc_html__('You can enable/disable Footer Banner Section', 'bolt')
                    ),

                    array(
                        'id' => 'footer_banner1',
                        'type' => 'media',
                        'required' => array('enable_footer_banner_section', '=', '1'),
                        'title' => esc_html__('Home Bottom Banner 1', 'bolt'),
                        'desc' => esc_html__('', 'bolt'),
                        'subtitle' => esc_html__('Upload Home Bottom Banner image', 'bolt'),
                    ),

                    array(
                        'id' => 'footer_banner_url1',
                        'type' => 'text',
                        'required' => array('enable_footer_banner_section', '=', '1'),
                        'title' => esc_html__('Home Bottom Banner-1 URL', 'bolt'),
                        'subtitle' => esc_html__('URL For Home Bottom Banner.', 'bolt'),
                    ),

                     array(
                        'id' => 'footer_banner2',
                        'type' => 'media',
                        'required' => array('enable_footer_banner_section', '=', '1'),
                        'title' => esc_html__('Home Bottom Banner 2', 'bolt'),
                        'desc' => esc_html__('', 'bolt'),
                        'subtitle' => esc_html__('Upload Home Bottom Banner image', 'bolt'),
                    ),

                    array(
                        'id' => 'footer_banner_url2',
                        'type' => 'text',
                        'required' => array('enable_footer_banner_section', '=', '1'),
                        'title' => esc_html__('Home Bottom Banner-2 URL', 'bolt'),
                        'subtitle' => esc_html__('URL For Home Bottom Banner.', 'bolt'),
                    ),

                     array(
                        'id' => 'footer_banner3',
                        'type' => 'media',
                        'required' => array('enable_footer_banner_section', '=', '1'),
                        'title' => esc_html__('Home Bottom Banner 3', 'bolt'),
                        'desc' => esc_html__('', 'bolt'),
                        'subtitle' => esc_html__('Upload Home Bottom Banner image', 'bolt'),
                    ),

                    array(
                        'id' => 'footer_banner_url3',
                        'type' => 'text',
                        'required' => array('enable_footer_banner_section', '=', '1'),
                        'title' => esc_html__('Home Bottom Banner-3 URL', 'bolt'),
                        'subtitle' => esc_html__('URL For Home Bottom Banner.', 'bolt'),
                    ),

                     array(
                        'id' => 'footer_banner4',
                        'type' => 'media',
                        'required' => array('enable_footer_banner_section', '=', '1'),
                        'title' => esc_html__('Home Bottom Banner 4', 'bolt'),
                        'desc' => esc_html__('', 'bolt'),
                        'subtitle' => esc_html__('Upload Home Bottom Banner image', 'bolt'),
                    ),

                    array(
                        'id' => 'footer_banner_url4',
                        'type' => 'text',
                        'required' => array('enable_footer_banner_section', '=', '1'),
                        'title' => esc_html__('Home Bottom Banner-4 URL', 'bolt'),
                        'subtitle' => esc_html__('URL For Home Bottom Banner.', 'bolt'),
                    ),


                    array(
                        'id' => 'enable_home_recommended_products',
                        'type' => 'switch',
                        'required' => array('theme_layout', '=', 'version3'),
                        'title' => esc_html__('Show recommeded Products', 'bolt'),
                        'subtitle' => esc_html__('You can show Show recommeded Products on home page.', 'bolt')
                    ),   

                    array(
                        'id' => 'recommended_products_per_page',
                        'type' => 'text',
                        'required' => array('enable_home_recommended_products', '=', '1'),
                        'title' => esc_html__('Number of recommeded Products', 'bolt'),
                        'subtitle' => esc_html__('Number of recommeded products on home page.', 'bolt')
                    ),

                    array(
                        'id' => 'enable_home_new_products',
                        'type' => 'switch',
                        'title' => esc_html__('Show New Products', 'bolt'),
                        'subtitle' => esc_html__('You can show Show New Products on home page.', 'bolt')
                    ),   

                    array(
                        'id' => 'new_products_per_page',
                        'type' => 'text',
                        'required' => array(array('enable_home_new_products', '=', '1'),array('theme_layout', '!=', 'default'),array('theme_layout', '!=', 'version2')),
                        'title' => esc_html__('Number of New Products', 'bolt'),
                        'subtitle' => esc_html__('Number of New products on home page.', 'bolt')
                    ), 

                     array(
                            'id'=>'home_newproduct_categories',
                            'type' => 'select',
                            'multi'=> true,                        
                            'data' => $cat_data,                            
                            'args' => $cat_arg,
                            'title' => esc_html__('New Product Category', 'bolt'), 
                            'required' => array(array('enable_home_new_products', '=', '1'),array('theme_layout', '!=', 'version3'),array('theme_layout', '!=', 'version4')),
                            'subtitle' => esc_html__('Please choose New Product Category to show  its product in home page.', 'bolt'),
                            'desc' => '',
                        ),

                    array(
                        'id' => 'enable_home_bestseller_products',
                        'type' => 'switch',
                        'title' => esc_html__('Show Best Seller Products', 'bolt'),
                        'subtitle' => esc_html__('You can show best seller products on home page.', 'bolt')
                    ),

                   
                    
                    array(
                        'id' => 'home_bestseller_products-text',
                        'type' => 'text',
                        'required' => array(array('enable_home_bestseller_products', '=', '1'),array('theme_layout', '=', 'default')),
                        'title' => esc_html__('Home bestseller products text', 'bolt'),
                         'desc' => esc_html__('', 'bolt'),
                         'subtitle' => esc_html__('home page bestseller_products-text ', 'bolt')
                    
                    ),
                        
                    array(
                            'id' => 'bestseller_image',
                            'type' => 'media',
                            'required' => array(array('enable_home_bestseller_products', '=', '1'),array('theme_layout', '=', 'version2')),
                            'title' => esc_html__('Home Best Seller image', 'bolt'),
                            'desc' => esc_html__('', 'bolt'),
                            'subtitle' => esc_html__('Upload bestseller image appear to the left of best seller on  home page ', 'bolt')
                    ),
                    array(
                        'id' => 'bestseller_image_text',
                        'type' => 'text',
                        'required' => array(array('enable_home_bestseller_products', '=', '1'),array('theme_layout', '=', 'version2')),
                        'title' => esc_html__('Home bestseller image text', 'bolt'),
                         'desc' => esc_html__('', 'bolt'),
                         'subtitle' => esc_html__('bestseller image text ', 'bolt')
                    
                    ),

                    array(
                        'id' => 'bestseller_product_url',
                        'type' => 'text',
                        'required' =>array(array('enable_home_bestseller_products', '=', '1'),array('theme_layout', '!=', 'version3'),array('theme_layout', '!=', 'version4')),
                        'title' => esc_html__('Home Best seller   Url', 'bolt'),
                        'subtitle' => esc_html__('Home Best seller  Url.', 'bolt'),
                    ),
                      array(
                        'id' => 'bestseller_per_page',
                        'type' => 'text',
                        'required' => array('enable_home_bestseller_products', '=', '1'),
                        'title' => esc_html__('Number of Bestseller Products', 'bolt'),
                        'subtitle' => esc_html__('Number of Bestseller products on home page.', 'bolt')
                    ), 
                 
                           
                    array(
                        'id' => 'enable_home_featured_products',
                        'required' => array('theme_layout', '!=', 'version2'),
                        'type' => 'switch',
                        'title' => esc_html__('Show Featured Products', 'bolt'),
                        'subtitle' => esc_html__('You can show featured products on home page.', 'bolt')
                    ),
                    
                    array(
                            'id' => 'enable_home_featured_products-text',
                            'type' => 'text',
                            'required' =>array(array('enable_home_featured_products', '=', '1'),array('theme_layout', '=', 'default')),
                            'title' => esc_html__('Home Featured Products Text', 'bolt'),
                            'desc' => esc_html__('', 'bolt'),
                            'subtitle' => esc_html__('home page featured products text ', 'bolt')
                    ),

                     
                    array(
                        'id' => 'featured_product_url',
                        'type' => 'text',
                        'required' => array(array('enable_home_featured_products', '=', '1'),array('theme_layout', '!=', 'version3'),array('theme_layout', '!=', 'version4')),
                        'title' => esc_html__('Home Featured  Url', 'bolt'),
                        'subtitle' => esc_html__('Home Featured  Url.', 'bolt'),
                    ),
   
                    array(
                        'id' => 'featured_per_page',
                        'type' => 'text',
                        'required' => array('enable_home_featured_products', '=', '1'),
                        'title' => esc_html__('Number of Featured Products', 'bolt'),
                        'subtitle' => esc_html__('Number of Featured products on home page.', 'bolt')
                    ),                             

                 
                    array(
                        'id' => 'enable_home_related_products',
                        'type' => 'switch',
                        'title' => esc_html__('Show Related Products', 'bolt'),
                        'subtitle' => esc_html__('You can show Related products on home page.', 'bolt')
                    ),
                    array(
                        'id' => 'related_products-text',
                        'type' => 'text',
                        'required' => array(array('enable_home_related_products', '=', '1'),array('theme_layout', '=', 'default')),   
                        'title' => esc_html__('Related products text', 'bolt'),
                         'desc' => esc_html__('', 'bolt'),
                         'subtitle' => esc_html__('related products-text ', 'bolt')
                    
                    ),
                   array(
                        'id' => 'related_product_url',
                        'type' => 'text',
                       'required' => array(array('enable_home_related_products', '=', '1'),array('theme_layout', '=', 'default')),
                        'title' => esc_html__('Home Related   Url', 'bolt'),
                        'subtitle' => esc_html__('Home Related  Url.', 'bolt'),
                    ),
                    array(
                        'id' => 'related_per_page',
                        'type' => 'text',
                       'required' => array('enable_home_related_products', '=', '1'), 
                        'title' => esc_html__('Number of Related Products', 'bolt'),
                        'subtitle' => esc_html__('Number of Related products on home page.', 'bolt')
                    ),
                    

                    array(
                        'id' => 'enable_home_upsell_products',
                        'type' => 'switch',
                        'title' => esc_html__('Show Upsell Products', 'bolt'),
                        'subtitle' => esc_html__('You can show Upsell products on home page.', 'bolt')
                    ),
                    array(
                        'id' => 'upsell_products-text',
                        'type' => 'text',
                        'required' => array(array('enable_home_upsell_products', '=', '1'),array('theme_layout', '=', 'default')),
                        'title' => esc_html__('Upsell Products text', 'bolt'),
                         'desc' => esc_html__('', 'bolt'),
                         'subtitle' => esc_html__('upsell products text ', 'bolt')
                    
                    ),
                   array(
                        'id' => 'upsell_product_url',
                        'type' => 'text',
                        'required' => array(array('enable_home_upsell_products', '=', '1'),array('theme_layout', '=', 'default')),
                        'title' => esc_html__('Home Upsell   Url', 'bolt'),
                        'subtitle' => esc_html__('Home Upsell  Url.', 'bolt'),
                    ),
                    array(
                        'id' => 'upsell_per_page',
                        'type' => 'text',
                        'required' => array('enable_home_upsell_products', '=', '1'), 
                        'title' => esc_html__('Number of Upsell Products', 'bolt'),
                        'subtitle' => esc_html__('Number of Upsell products on home page.', 'bolt')
                    ),

                    array(
                        'id' => 'enable_cross_sells_products',
                        'type' => 'switch',
                        'title' => esc_html__('Show cross sells Products', 'bolt'),
                        'subtitle' => esc_html__('You can show cross sells products.', 'bolt')
                    ),

                    array(
                        'id' => 'cross_per_page',
                        'type' => 'text',
                        'required' => array('enable_cross_sells_products', '=', '1'), 
                        'title' => esc_html__('Number of cross sells Products', 'bolt'),
                        'subtitle' => esc_html__('Number of cross sells Products', 'bolt')
                    ),

                    array(
                        'id'       => 'enable_testimonial',
                        'required' => array('theme_layout', '=', 'version2'),
                        'type'     => 'switch',                    
                        'title'    => esc_html__( 'Enable Testimonial ', 'bolt' ),
                        'subtitle' => esc_html__( 'You can enable/disable Testimonial Uploads', 'bolt' ),
                          'default' => '0'
                    ),                   
                    array(
                        'id' => 'all_testimonial',
                        'type' => 'slides',
                        'required' => array('enable_testimonial', '=', '1'),
                        'title' => esc_html__('Add Testimonial here', 'bolt'),
                        'subtitle' => esc_html__('Unlimited testimonial.', 'bolt'),
                        'placeholder' => array(
                            'title' => esc_html__('This is a title', 'bolt'),
                            'description' => esc_html__('Description Here', 'bolt'),
                            'url' => esc_html__('Give us a link!', 'bolt'),
                        ),
                        ),
                    
                 array(
                        'id' => 'enable_home_blog_posts',
                        'type' => 'switch',
                        'title' => esc_html__('Show Home Blog', 'bolt'),
                        'subtitle' => esc_html__('You can show latest blog post on home page.', 'bolt')
                    ),

                ), // fields array ends
            ) );
       // Edgesettings: General Settings Tab
    Redux::setSection( $opt_name, array(
                'icon' => 'el-icon-cogs',
                'title' => esc_html__('General Settings', 'bolt'),
                'fields' => array(
                               array(
                    'id'       => 'category_pagelayout',
                    'type'     => 'radio',
                    'title'    => __('Set Category Page Default Layout', 'bolt'), 
                    'subtitle' => __('Set Category Page Default Layout', 'bolt'),
                    'desc'     => __('You cans set Category Page Default Layout here ', 'bolt'),
                    //Must provide key => value pairs for radio options
                    'options'  => array(
                        'grid' => 'Grid', 
                        'list' => 'List', 
                     
                    ),
                    'default' => 'grid'
                ),
                    
                                                                                               
                     array(
                     'id'       => 'category_item',
                     'type'     => 'spinner', 
                     'title'    => esc_html__('Product display in product category page', 'bolt'),
                     'subtitle' => esc_html__('Number of item display in product category page','bolt'),
                     'desc'     => esc_html__('Number of item display in product category page', 'bolt'),
                     'default'  => '9',
                     'min'      => '0',
                     'step'     => '1',
                     'max'      => '100',
                     ),
                           
                     array(
                        'id'       => 'enable_brand_logo',
                        'type'     => 'switch',                    
                        'title'    => esc_html__( 'Enable Company Logo Uploads', 'bolt' ),
                        'subtitle' => esc_html__( 'You can enable/disable Company Logo Uploads', 'bolt' ),
                          'default' => '0'
                    ),                   
                    array(
                        'id' => 'all-company-logos',
                        'type' => 'slides',
                        'required' => array('enable_brand_logo', '=', '1'),
                        'title' => esc_html__('Company Logo Uploads', 'bolt'),
                        'subtitle' => esc_html__('Unlimited Logo uploads with drag and drop sortings.', 'bolt'),
                        'placeholder' => array(
                            'title' => esc_html__('This is a title', 'bolt'),
                            'description' => esc_html__('Description Here', 'bolt'),
                            'url' => esc_html__('Give us a link!', 'bolt'),
                        ),
                    ),

                    array(
                        'id' => 'header_show_info_banner',
                        'type' => 'switch',
                        'required' => array('theme_layout', '=', 'version3'), 
                        'title' => esc_html__('Show Info Banners', 'bolt'),
                          'default' => '0'
                    ),

                 
                    array(
                        'id' => 'header_shipping_banner',
                        'type' => 'text',
                        'required' => array('header_show_info_banner', '=', '1'),
                        'title' => esc_html__('Shipping Banner', 'bolt'),
                    ),

                    array(
                        'id' => 'header_shipping_banner_dsc',
                        'type' => 'text',
                        'required' => array('header_show_info_banner', '=', '1'),
                        'title' => esc_html__('Shipping Banner Text', 'bolt'),
                    ),

                    array(
                        'id' => 'header_customer_support_banner',
                        'type' => 'text',
                        'required' => array('header_show_info_banner', '=', '1'),
                        'title' => esc_html__('Customer Support Banner', 'bolt'),
                    ),

                    array(
                        'id' => 'header_customer_support_banner_dsc',
                        'type' => 'text',
                        'required' => array('header_show_info_banner', '=', '1'),
                        'title' => esc_html__('Customer Support Banner Text', 'bolt'),
                    ),

                    array(
                        'id' => 'header_returnservice_banner',
                        'type' => 'text',
                        'required' => array('header_show_info_banner', '=', '1'),
                        'title' => esc_html__('Return service Banner', 'bolt'),
                    ),

                     array(
                        'id' => 'header_returnservice_banner_dsc',
                        'type' => 'text',
                        'required' => array('header_show_info_banner', '=', '1'),
                        'title' => esc_html__('Return service Banner Text', 'bolt'),
                    ),


                      array(
                        'id'       => 'enable_product_socialshare',
                        'type'     => 'switch',                    
                        'title'    => esc_html__( 'Enable Product Page Social Share ', 'bolt' ),
                        'subtitle' => esc_html__( 'You can enable/disable Product Page Social Share', 'bolt' ),
                          'default' => '0'
                    ), 

                    array(
                        'id' => 'back_to_top',
                        'type' => 'switch',
                        'title' => esc_html__('Back To Top Button', 'bolt'),
                        'subtitle' => esc_html__('Toggle whether or not to enable a back to top button on your pages.', 'bolt'),
                        'default' => true,
                    ),

                    
                    
                )
            ) );

  // Edgesettings: General Options -> Styling Options Settings Tab
    Redux::setSection( $opt_name, array(
                'icon' => 'el-icon-website',
                'title' => esc_html__('Styling Options', 'bolt'),
               
                'fields' => array(
                        

                    array(
                        'id' => 'set_body_background_img_color',
                        'type' => 'switch',
                        'title' => esc_html__('Set Body Background', 'bolt'),
                        'subtitle' => esc_html__('', 'bolt'),
                        'default' => 0,
                        'on' => 'On',
                        'off' => 'Off',
                    ),
                    array(
                        'id' => 'opt-background',
                        'type' => 'background',
                        'required' => array('set_body_background_img_color', '=', '1'),
                        'output' => array('body'),
                        'title' => esc_html__('Body Background', 'bolt'),
                        'subtitle' => esc_html__('Body background with image, color, etc.', 'bolt'),               
                        'transparent' => false,
                    ),                   
                    array(
                        'id' => 'opt-color-footer',
                        'type' => 'color',
                        'title' => esc_html__('Footer Background Color', 'bolt'),
                        'subtitle' => esc_html__('Pick a background color for the footer.', 'bolt'),
                        'validate' => 'color',
                        'transparent' => false,
                        'mode' => 'background',
                        'output' => array('footer','.footer-bottom')
                    ),
                    array(
                        'id' => 'opt-color-rgba',
                        'type' => 'color',
                        'title' => esc_html__('Header Nav Menu Background', 'bolt'),
                        'output' => array('.mgk-main-menu'),
                        'mode' => 'background',
                        'validate' => 'color',
                        'transparent' => false,
                    ),
                    array(
                        'id' => 'opt-color-header',
                        'type' => 'color',
                        'title' => esc_html__('Header Background', 'bolt'),
                        'transparent' => false,
                        'output' => array('header','.header-container','.header-top'),
                        'mode' => 'background',
                    ),  
                                      
                )
            ));

// Edgesettings: Header Tab
    Redux::setSection( $opt_name, array(
                'icon' => 'el-icon-file-alt',
                'title' => esc_html__('Header', 'bolt'),
                'heading' => esc_html__('All header related options are listed here.', 'bolt'),
                'desc' => esc_html__('', 'bolt'),
                'fields' => array(
                    
                    array(
                        'id' => 'enable_home_header_top_banner',
                        'type' => 'switch',
                        'required' => array('theme_layout', '=', 'default'),              
                        'title' => esc_html__('Enable Home Page Top Banner', 'bolt'),
                        'subtitle' => esc_html__('You can enable/disable Home page Top Banner', 'bolt')
                    ),  

                    array(
                        'id' => 'home_header_top_banner',
                        'type' => 'media',
                        'required' => array('enable_home_header_top_banner', '=', '1'),
                        'title' => esc_html__('Home Page Top Banner', 'bolt'),
                        'desc' => esc_html__('', 'bolt'),
                        'subtitle' => esc_html__('Upload Home Page Top Banner image', 'bolt'),
                    ),

                    array(
                        'id' => 'home_top_banner_url',
                        'type' => 'text',
                        'required' => array('enable_home_header_top_banner', '=', '1'),
                        'title' => esc_html__('Home Page Top Banner URL', 'bolt'),
                        'subtitle' => esc_html__('URL For Home Page Top Banner.', 'bolt'),
                    ),

                    array(
                        'id' => 'enable_header_currency',
                        'type' => 'switch',
                        'title' => esc_html__('Show Currency HTML', 'bolt'),
                        'subtitle' => esc_html__('You can show Currency in the header.', 'bolt')
                    ),
                    array(
                        'id' => 'enable_header_language',
                        'type' => 'switch',
                        'title' => esc_html__('Show Language HTML', 'bolt'),
                        'subtitle' => esc_html__('You can show Language in the header.', 'bolt')
                    ),
                    array(
                        'id' => 'header_use_imagelogo',
                        'type' => 'checkbox',
                        'title' => esc_html__('Use Image for Logo?', 'bolt'),
                        'subtitle' => esc_html__('If left unchecked, plain text will be used instead (generated from site name).', 'bolt'),
                        'desc' => esc_html__('', 'bolt'),
                        'default' => '1'
                    ),
                    array(
                        'id' => 'header_logo',
                        'type' => 'media',
                        'required' => array('header_use_imagelogo', '=', '1'),
                        'title' => esc_html__('Logo Upload', 'bolt'),
                        'desc' => esc_html__('', 'bolt'),
                        'subtitle' => esc_html__('Upload your logo here and enter the height of it below', 'bolt'),
                    ),
                    array(
                        'id' => 'header_logo_height',
                        'type' => 'text',
                        'required' => array('header_use_imagelogo', '=', '1'),
                        'title' => esc_html__('Logo Height', 'bolt'),
                        'subtitle' => esc_html__('Don\'t include "px" in the string. e.g. 30', 'bolt'),
                        'desc' => '',
                        'validate' => 'numeric'
                    ),
                    array(
                        'id' => 'header_logo_width',
                        'type' => 'text',
                        'required' => array('header_use_imagelogo', '=', '1'),
                        'title' => esc_html__('Logo Width', 'bolt'),
                        'subtitle' => esc_html__('Don\'t include "px" in the string. e.g. 30', 'bolt'),
                        'desc' => '',
                        'validate' => 'numeric'
                    ),    
                                 
                    array(
                        'id' => 'header_remove_header_search',
                        'type' => 'checkbox',
                        'title' => esc_html__('Remove Header Search', 'bolt'),
                        'subtitle' => esc_html__('Active to remove the search functionality from your header', 'bolt'),
                        'desc' => esc_html__('', 'bolt'),
                        'default' => '0'
                    ),
                     
                   
                 
                   
                ) //fields end
            ) );
      // Edgesettings: Menu Tab
    Redux::setSection( $opt_name,  array(
                'icon' => 'el el-website icon',
                'title' => esc_html__('Menu', 'bolt'),
                'heading' => esc_html__('All Menu related options are listed here.', 'bolt'),
                'desc' => esc_html__('', 'bolt'),
                'fields' => array(
                   array(
                        'id' => 'show_menu_arrow',
                        'type' => 'switch',
                        'title' => esc_html__('Show Menu Arrow', 'bolt'),
                        'desc'  => esc_html__('Show arrow in menu.', 'bolt'),
                        
                    ),               
                   array(
                    'id'       => 'login_button_pos',
                    'type'     => 'radio',
                    'title'    => esc_html__('Show Login/sign and logout link', 'bolt'),                   
                    'desc'     => esc_html__('Please Select any option from above.', 'bolt'),
                     //Must provide key => value pairs for radio options
                    'options'  => array(
                    'none' => 'None', 
                   'toplinks' => 'In Top Menu', 
                   'main_menu' => 'In Main Menu'
                    ),
                   'default' => 'none'
                    ),
                   array(
                        'id' => 'enable_home_page_menu_banner',
                        'type' => 'switch',
                        'required' => array('theme_layout', '=', 'version4'),
                        'title' => esc_html__('Enable Home Page menu Banner', 'bolt'),
                        'subtitle' => esc_html__('You can enable/disable Home Page side menu Banner', 'bolt')
                    ),

                    array(
                        'id' => 'home_menu_banner',
                        'type' => 'media',
                        'required' => array('enable_home_page_menu_banner', '=', '1'),
                        'title' => esc_html__('Home Page menu Banner', 'bolt'),
                        'desc' => esc_html__('', 'bolt'),
                        'subtitle' => esc_html__('Upload Home Page menu Banner image', 'bolt'),
                    ),

                    array(
                        'id' => 'home_menu_banner_url',
                        'type' => 'text',
                        'required' => array('enable_home_page_menu_banner', '=', '1'),
                        'title' => esc_html__('Home Page menu Banner URL', 'bolt'),
                        'subtitle' => esc_html__('URL For Home Page menu Banner.', 'bolt'),
                    )
                  
                ) // fields ends here
            ));
 // Edgesettings: Footer Tab
    Redux::setSection( $opt_name, array(
                'icon' => 'el-icon-file-alt',
                'title' => esc_html__('Footer', 'bolt'),
                'heading' => esc_html__('All footer related options are listed here.', 'bolt'),
                'desc' => esc_html__('', 'bolt'),
                'fields' => array(
                     array(
                        'id'       => 'enable_mailchimp_form',
                        'type'     => 'switch',                    
                        'title'    => esc_html__( 'Enable Mailchimp Form', 'bolt' ),
                        'subtitle' => esc_html__( 'You can enable/disable Mailchimp Form', 'bolt' ),
                          'default' => '0'
                    ), 
                    array(
                        'id' => 'footer_color_scheme',
                        'type' => 'switch',
                        'title' => esc_html__('Custom Footer Color Scheme', 'bolt'),
                        'subtitle' => esc_html__('', 'bolt')
                    ),               
                    array(
                        'id' => 'footer_copyright_background_color',
                        'type' => 'color',
                        'required' => array('footer_color_scheme', '=', '1'),
                        'transparent' => false,
                        'title' => esc_html__('Footer Copyright Background Color', 'bolt'),
                        'subtitle' => esc_html__('', 'bolt'),
                        'validate' => 'color',
                    ),
                    array(
                        'id' => 'footer_copyright_font_color',
                        'type' => 'color',
                        'required' => array('footer_color_scheme', '=', '1'),
                        'transparent' => false,
                        'title' => esc_html__('Footer Copyright Font Color', 'bolt'),
                        'subtitle' => esc_html__('', 'bolt'),
                        'validate' => 'color',
                    ), 
                    
                    array(
                        'id' => 'footer_show_info_banner',
                        'type' => 'switch',
                        'required' => array('theme_layout', '!=', 'version3'),
                        'title' => esc_html__('Show footer service', 'bolt'),
                          'default' => '0'
                    ),

                 
                    array(
                        'id' => 'footer_shipping_banner',
                        'type' => 'text',
                        'required' => array('footer_show_info_banner', '=', '1'),
                        'title' => esc_html__('Shipping Banner Text', 'bolt'),
                    ),

                     array(
                        'id' => 'footer_returnservice_banner',
                        'type' => 'text',
                        'required' => array('footer_show_info_banner', '=', '1'),
                        'title' => esc_html__('Return service Banner Text', 'bolt'),
                    ),
                   
                    array(
                        'id' => 'footer_customer_support_banner',
                        'type' => 'text',
                        'required' => array('footer_show_info_banner', '=', '1'),
                        'title' => esc_html__('Customer Support Banner Text', 'bolt'),
                    ),

                    array(
                        'id' => 'footer_moneyback_banner',
                        'type' => 'text',
                        'required' => array('footer_show_info_banner', '=', '1'),
                        'title' => esc_html__('Warrant/Gaurantee Banner Text', 'bolt'),
                    ),
           
                    array(
                        'id' => 'enable_footer_middle',
                        'type' => 'switch', 
                        'required' => array('theme_layout', '!=', 'version4'),                      
                        'title' => esc_html__('Enable footer middle', 'bolt'),
                        'subtitle' => esc_html__('You can enable/disable Footer Middle', 'bolt')
                    ),

                    array(
                        'id' => 'footer_middle',
                        'type' => 'editor',
                        'title' => esc_html__('Footer Middle Text ', 'bolt'), 
                        'required' => array('enable_footer_middle', '=', '1'),               
                       'subtitle' => esc_html__('You can use the following shortcodes in your footer text: [wp-url] [site-url] [theme-url] [login-url] [logout-url] [site-title] [site-tagline] [current-year]', 'bolt'),
                        'default' => '',
                    ),    
                                            
                    array(
                        'id' => 'bottom-footer-text',
                        'type' => 'editor',
                        'title' => esc_html__('Bottom Footer Text', 'bolt'),
                        'subtitle' => esc_html__('You can use the following shortcodes in your footer text: [wp-url] [site-url] [theme-url] [login-url] [logout-url] [site-title] [site-tagline] [current-year]', 'bolt'),
                        'default' => esc_html__('Powered by Magik', 'bolt'),
                    ),
                    
                    
                ) // fields ends here
            ));

 //Edgesettings: Blog Tab
    Redux::setSection( $opt_name, array(
                'icon' => 'el-icon-pencil',
                'title' => esc_html__('Blog Page', 'bolt'),
                'fields' => array( 
                       array(
                        'id' => 'blog-page-layout',
                        'type' => 'image_select',
                        'title' => esc_html__('Blog Page Layout', 'bolt'),
                        'subtitle' => esc_html__('Select main blog listing and category page layout from the available blog layouts.', 'bolt'),
                        'options' => array(
                            '1' => array(
                                'alt' => 'Left sidebar',
                                'img' => get_template_directory_uri() . '/images/magik_col/category-layout-1.png'

                            ),
                            '2' => array(
                                'alt' => 'Right Right',
                                'img' => get_template_directory_uri() . '/images/magik_col/category-layout-2.png'
                            ),
                            '3' => array(
                                'alt' => '2 Column Right',
                                'img' => get_template_directory_uri() . '/images/magik_col/category-layout-3.png'
                            )                                                                                 
                          
                        ),
                        'default' => '2'
                    ), 
                     array(
                        'id' => 'blog_show_authors_bio',
                        'type' => 'switch',
                        'title' => esc_html__('Author\'s Bio', 'bolt'),
                        'subtitle' => esc_html__('Show Author Bio on Blog page.', 'bolt'),
                         'default' => true,
                        'desc' => esc_html__('', 'bolt')
                    ),                  
                    array(
                        'id' => 'blog_show_post_by',
                        'type' => 'switch',
                        'title' => esc_html__('Display Post By', 'bolt'),
                         'default' => true,
                        'subtitle' => esc_html__('Display Psot by Author on Listing Page', 'bolt')
                    ),
                    array(
                        'id' => 'blog_display_tags',
                        'type' => 'switch',
                        'title' => esc_html__('Display Tags', 'bolt'),
                         'default' => true,
                        'subtitle' => esc_html__('Display tags at the bottom of posts.', 'bolt')
                    ),
                    array(
                        'id' => 'blog_full_date',
                        'type' => 'switch',
                        'title' => esc_html__('Display Full Date', 'bolt'),
                        'default' => true,
                        'subtitle' => esc_html__('This will add date of post meta on all blog pages.', 'bolt')
                    ),
                    array(
                        'id' => 'blog_display_comments_count',
                        'type' => 'switch',
                        'default' => true,
                        'title' => esc_html__('Display Comments Count', 'bolt'),
                        'subtitle' => esc_html__('Display Comments Count on Blog Listing.', 'bolt')
                    ),
                    array(
                        'id' => 'blog_display_category',
                        'type' => 'switch',
                        'title' => esc_html__('Display Category', 'bolt'),
                         'default' => true,
                        'subtitle' => esc_html__('Display Comments Category on Blog Listing.', 'bolt')
                    ),
                    array(
                        'id' => 'blog_display_view_counts',
                        'type' => 'switch',
                        'title' => esc_html__('Display View Counts', 'bolt'),
                         'default' => true,
                        'subtitle' => esc_html__('Display View Counts on Blog Listing.', 'bolt')
                    ),                  
                )
            ));
 // Edgesettings: Social Media Tab
    Redux::setSection( $opt_name, array(
                'icon' => 'el-icon-file',
                'title' => esc_html__('Social Media', 'bolt'),
                'fields' => array(
                     array(
                        'id'       => 'enable_social_link_footer',
                        'type'     => 'switch',                    
                        'title'    => esc_html__( 'Enable Social Link In Footer', 'bolt' ),                        
                        'default' => '0'
                    ),
                    array(
                        'id' => 'social_facebook',
                        'type' => 'text',
                        'title' => esc_html__('Facebook URL', 'bolt'),
                        'subtitle' => esc_html__('Please enter in your Facebook URL.', 'bolt'),
                    ),
                    array(
                        'id' => 'social_twitter',
                        'type' => 'text',
                        'title' => esc_html__('Twitter URL', 'bolt'),
                        'subtitle' => esc_html__('Please enter in your Twitter URL.', 'bolt'),
                    ),
                    array(
                        'id' => 'social_googlep',
                        'type' => 'text',
                        'title' => esc_html__('Google+ URL', 'bolt'),
                        'subtitle' => esc_html__('Please enter in your Google Plus URL.', 'bolt'),
                    ),
                  
                    array(
                        'id' => 'social_pinterest',
                        'type' => 'text',
                        'title' => esc_html__('Pinterest URL', 'bolt'),
                        'subtitle' => esc_html__('Please enter in your Pinterest URL.', 'bolt'),
                    ),
                    array(
                        'id' => 'social_youtube',
                        'type' => 'text',
                        'title' => esc_html__('Youtube URL', 'bolt'),
                        'subtitle' => esc_html__('Please enter in your Youtube URL.', 'bolt'),
                    ),
                    array(
                        'id' => 'social_linkedin',
                        'type' => 'text',
                        'title' => esc_html__('LinkedIn URL', 'bolt'),
                        'subtitle' => esc_html__('Please enter in your LinkedIn URL.', 'bolt'),
                    ),
                    array(
                        'id' => 'social_instagram',
                        'type' => 'text',
                        'title' => esc_html__('Instagram URL', 'bolt'),
                        'subtitle' => esc_html__('Please enter in your Instagram URL.', 'bolt'),
                    ),
                    array(
                        'id' => 'social_rss',
                        'type' => 'text',
                        'title' => esc_html__('RSS URL', 'bolt'),
                        'subtitle' => esc_html__('Please enter in your RSS URL.', 'bolt'),
                    )                   
                )
            ));

   

    if ( file_exists( dirname( __FILE__ ) . '/../README.md' ) ) {
        $section = array(
            'icon'   => 'el el-list-alt',
            'title'  => esc_html__( 'Documentation', 'bolt' ),
            'fields' => array(
                array(
                    'id'       => '17',
                    'type'     => 'raw',
                    'markdown' => true,
                    'content_path' => dirname( __FILE__ ) . '/../README.md', // FULL PATH, not relative please
                    //'content' => 'Raw content here',
                ),
            ),
        );
        Redux::setSection( $opt_name, $section );
    }
    /*
     * <--- END SECTIONS
     */


    /*
     *
     * YOU MUST PREFIX THE FUNCTIONS BELOW AND ACTION FUNCTION CALLS OR ANY OTHER CONFIG MAY OVERRIDE YOUR CODE.
     *
     */

    /*
    *
    * --> Action hook examples
    *
    */

    // If Redux is running as a plugin, this will remove the demo notice and links
    //add_action( 'redux/loaded', 'remove_demo' );

    // Function to test the compiler hook and demo CSS output.
    // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
    //add_filter('redux/options/' . $opt_name . '/compiler', 'compiler_action', 10, 3);

    // Change the arguments after they've been declared, but before the panel is created
    //add_filter('redux/options/' . $opt_name . '/args', 'change_arguments' );

    // Change the default value of a field after it's been set, but before it's been useds
    //add_filter('redux/options/' . $opt_name . '/defaults', 'change_defaults' );

    // Dynamically add a section. Can be also used to modify sections/fields
    //add_filter('redux/options/' . $opt_name . '/sections', 'dynamic_section');

    /**
     * This is a test function that will let you see when the compiler hook occurs.
     * It only runs if a field    set with compiler=>true is changed.
     * */
    if ( ! function_exists( 'compiler_action' ) ) {
        function compiler_action( $options, $css, $changed_values ) {
            echo '<h1>The compiler hook has run!</h1>';
            echo "<pre>";
            print_r( $changed_values ); // Values that have changed since the last save
            echo "</pre>";
            //print_r($options); //Option values
            //print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )
        }
    }

    /**
     * Custom function for the callback validation referenced above
     * */
    if ( ! function_exists( 'redux_validate_callback_function' ) ) {
        function redux_validate_callback_function( $field, $value, $existing_value ) {
            $error   = false;
            $warning = false;

            //do your validation
            if ( $value == 1 ) {
                $error = true;
                $value = $existing_value;
            } elseif ( $value == 2 ) {
                $warning = true;
                $value   = $existing_value;
            }

            $return['value'] = $value;

            if ( $error == true ) {
                $return['error'] = $field;
                $field['msg']    = 'your custom error message';
            }

            if ( $warning == true ) {
                $return['warning'] = $field;
                $field['msg']      = 'your custom warning message';
            }

            return $return;
        }
    }

    /**
     * Custom function for the callback referenced above
     */
    if ( ! function_exists( 'redux_my_custom_field' ) ) {
        function redux_my_custom_field( $field, $value ) {
            print_r( $field );
            echo '<br/>';
            print_r( $value );
        }
    }

    /**
     * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
     * Simply include this function in the child themes functions.php file.
     * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
     * so you must use get_template_directory_uri() if you want to use any of the built in icons
     * */
    if ( ! function_exists( 'dynamic_section' ) ) {
        function dynamic_section( $sections ) {
            //$sections = array();
            $sections[] = array(
                'title'  => esc_html__( 'Section via hook', 'bolt' ),
                'desc'   => esc_html__( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'bolt' ),
                'icon'   => 'el el-paper-clip',
                // Leave this as a blank section, no options just some intro text set above.
                'fields' => array()
            );

            return $sections;
        }
    }

    /**
     * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
     * */
    if ( ! function_exists( 'change_arguments' ) ) {
        function change_arguments( $args ) {
            //$args['dev_mode'] = true;

            return $args;
        }
    }

    /**
     * Filter hook for filtering the default value of any given field. Very useful in development mode.
     * */
    if ( ! function_exists( 'change_defaults' ) ) {
        function change_defaults( $defaults ) {
            $defaults['str_replace'] = 'Testing filter hook!';

            return $defaults;
        }
    }

    /**
     * Removes the demo link and the notice of integrated demo from the redux-framework plugin
     */
    if ( ! function_exists( 'remove_demo' ) ) {
        function remove_demo() {
            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                remove_filter( 'plugin_row_meta', array(
                    ReduxFrameworkPlugin::instance(),
                    'plugin_metalinks'
                ), null, 2 );

                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
            }
        }
    }

