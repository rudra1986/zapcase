<!DOCTYPE html>
<html <?php language_attributes(); ?> id="parallax_scrolling">
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
 <?php wp_head(); ?>
</head>
<?php
 $MagikBolt = new MagikBolt(); ?>
<body <?php body_class(); ?> >
  <div id="page" class="page catalog-category-view">


<header>
    <div class="header-container">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-sm-3 col-xs-12"> 
            
            <div class="logo">
                <?php magikBolt_logo_image();?>
            </div>

        <div class="nav-icon">
          <div class="mega-container visible-lg visible-md visible-sm">
          
              <?php if(has_nav_menu('main_menu'))
                  { ?> 

                     <div class="navleft-container">
                        <div class="mega-menu-title">
                           <h3>
                            <i class="fa fa-navicon"></i><?php esc_attr_e('Categories', 'bolt'); ?>
                          </h3>
                        </div>
                       <div class="mega-menu-category">
                           <?php echo magikBolt_home_mobile_menu(); ?>  

                            <?php magikBolt_side_banner_menu();?>
                        </div>
                      </div>                      
            
              <?php  }?>
                
              

            </div>
          </div>
        </div>

      <div class="col-lg-9 col-sm-9 col-xs-12 rhs_coloumn">

             <div class="mm-toggle-wrap">
                  <div class="mm-toggle">  <a class="mobile-toggle"><i class="fa fa-reorder"></i></a></div>
          </div>

            <div class="header-right">
              <div class="search_cart_block">
                
                 <?php echo magikBolt_search_form(); ?>

                <div class="right_menu">
                  <div class="menu_top">


                  <?php if (class_exists('WooCommerce')) :?>

                    <div class="top-cart-contain pull-right"> 
                      <?php  magikBolt_mini_cart(); ?>
                     
                    </div>

                  <?php endif;?>

                  </div>
                  
                  <div class="lang-curr hidden-xs">
                     <?php echo magikBolt_currency_language(); ?>
                  </div>
                   
                </div>
              </div>
              <div class="top_section hidden-xs">
                <div class="toplinks"> 
                  
                  
                  <div class="welcome-msg hidden-xs">
                    <?php echo magikBolt_msg(); ?>
                  </div>
                 
                   <?php if ( has_nav_menu( 'toplinks' ) )
                    { ?>
                         <div class="links">
                           <?php echo magikBolt_top_navigation(); ?>
                         </div>
                    <?php  }?>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </header>


 
  <?php if (class_exists('WooCommerce') && is_woocommerce()) : ?>
    
    <div class="breadcrumbs">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
                  <?php woocommerce_breadcrumb(); ?>
              </div>
          <!--col-xs-12--> 
        </div>
        <!--row--> 
      </div>
      <!--container--> 
    </div>
           <?php endif; ?>