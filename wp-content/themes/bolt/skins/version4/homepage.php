<?php magikBolt_home_page_banner(); ?>
<div class="main-container col2-left-layout">
    <div class="container">
      <div class="row">
        
        <div class="col-sm-8 col-sm-push-4 col-md-9 col-md-push-3 main-inner"> 

        <div class="col-main">
           <?php magikBolt_new_products();?>
        </div>

         <?php magikBolt_featured_products();?>
         <?php magikBolt_home_blog_posts();?>

        </div>

        <div class="col-left sidebar col-sm-4 col-xs-12 col-sm-pull-8 col-md-3 col-md-pull-9">
           
           <?php magikBolt_hotdeal_product();?>

           <?php magikBolt_sidebar_banner();?>

          <?php magikBolt_bestseller_products();?>

          
        </div>

      </div>
    </div>
     <?php magikBolt_home_customsection();?>
  </div>
 
  