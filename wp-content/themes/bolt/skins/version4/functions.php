<?php
if ( ! function_exists ( 'magikBolt_currency_language' ) ) {
function magikBolt_currency_language()
{ 
     global $bolt_Options;



        if(isset($bolt_Options['enable_header_language']) && ($bolt_Options['enable_header_language']!=0))
        { ?>

          <div class="form-language"> 
            <ul class="lang">
              <li><a href="#" title="<?php esc_attr_e('English', 'bolt');?>"><img src="<?php echo esc_url(get_template_directory_uri()) ;?>/images/english.png" alt="<?php esc_attr_e('English', 'bolt');?>"><span><?php esc_attr_e('English', 'bolt');?></span></a></li>
              <li><a href="#" title="<?php esc_attr_e('Francais', 'bolt');?>"><img src="<?php echo esc_url(get_template_directory_uri()) ;?>/images/francais.png" alt="<?php esc_attr_e('French', 'bolt');?>"><span><?php esc_attr_e('Francais', 'bolt');?></span></a></li>
              <li><a href="#" title="<?php esc_attr_e('German', 'bolt');?>"><img src="<?php echo esc_url(get_template_directory_uri()) ;?>/images/german.png" alt="<?php esc_attr_e('German', 'bolt');?>"><span><?php esc_attr_e('German', 'bolt');?></span></a></li>
            </ul>
          </div>
        <?php  
        } ?>


   <?php if(isset($bolt_Options['enable_header_currency']) && ($bolt_Options['enable_header_currency']!=0))
    { 

     global $wp,$wp_query;
     $cururl=magikBolt_curPageURL();
     if(class_exists('WOOCS'))
     {
       $usd_url =  add_query_arg( array("currency"=>"USD"), $cururl);
       $gbp_url =  add_query_arg( array("currency"=>"GBP"),  $cururl);

       $WOOCS=new WOOCS();

     $currency_selected=$WOOCS->current_currency;
     }
     else{
      
      $usd_url = "#";
      $gbp_url ="#";
      $currency_selected="USD";
    }

    if((isset($_GET['currency']) && $_GET['currency']=='GBP') || $currency_selected=='GBP')
    {
     
     $elected_currency='<a role="button" data-toggle="dropdown" data-target="#" class="block-currency dropdown-toggle" href="'. esc_url($gbp_url).'">'  
     . esc_attr("POUND", "bolt").' <span class="caret"></span></a>';
     
   }
   else{
     $elected_currency='<a role="button" data-toggle="dropdown" data-target="#" class="block-currency dropdown-toggle" href="'.esc_url($usd_url).'">'  
     . esc_attr("USD", "bolt").' <span class="caret"></span></a>';
   }


   ?>


          <div class="form-currency"> 
            <ul class="currencies_list">
               <li>
        <a role="menuitem" tabindex="-1" href="<?php echo esc_url($usd_url);?>">
          <?php esc_attr_e('$', 'bolt');?>
        </a>
      </li>
      
      <li role="presentation">
        <a role="menuitem" tabindex="-1" href="<?php echo esc_url($gbp_url) ;?>">
          <?php esc_attr_e('&#163;', 'bolt');?>
        </a>
      </li>
        <li role="presentation">
        <a role="menuitem" tabindex="-1" href="#">
          <?php esc_attr_e('£', 'bolt');?>
        </a>
      </li>
     </ul>
          </div>
  <?php  
} 
}
}
        
      

if ( ! function_exists ( 'magikBolt_msg' ) ) {
function magikBolt_msg()
{ 
     global $bolt_Options;

          if (isset($bolt_Options['enable_welcome_msg']) && $bolt_Options['enable_welcome_msg'])
             {
           if (is_user_logged_in()) {
            global $current_user;
          
              if(isset($bolt_Options['welcome_msg'])) {
            echo esc_attr_e('You are logged in as', 'bolt'). '   <b>'. esc_attr($current_user->display_name) .'</b>';
          }
          }
          else{
            if(isset($bolt_Options['welcome_msg']) && ($bolt_Options['welcome_msg']!='')){
            echo htmlspecialchars_decode($bolt_Options['welcome_msg']);
            }
          } 
        }
}
}

if ( ! function_exists ( 'magikBolt_logo_image' ) ) {
function magikBolt_logo_image()
{ 
     global $bolt_Options;
    
        $logoUrl = get_template_directory_uri() . '/images/logo.png';
        
        if (isset($bolt_Options['header_use_imagelogo']) && $bolt_Options['header_use_imagelogo'] == 0) {           ?>
        <a class="logo-title"  title="<?php bloginfo('name'); ?>" href="<?php echo esc_url(get_home_url()); ?> ">
        <?php bloginfo('name'); ?>
        </a>
        <?php
        } else if (isset($bolt_Options['header_logo']['url']) && !empty($bolt_Options['header_logo']['url'])) { 
                  $logoUrl = $bolt_Options['header_logo']['url'];
                  ?>
        <a title="<?php bloginfo('name'); ?>" href="<?php echo esc_url(get_home_url()); ?> "> <img
                      alt="<?php bloginfo('name'); ?>" src="<?php echo esc_url($logoUrl); ?>"
                      height="<?php echo !empty($bolt_Options['header_logo_height']) ? esc_html($bolt_Options['header_logo_height']) : ''; ?>"
                      width="<?php echo !empty($bolt_Options['header_logo_width']) ? esc_html($bolt_Options['header_logo_width']) : ''; ?>"> </a>
        <?php
        } else { ?>
        <a title="<?php bloginfo('name'); ?>" href="<?php echo esc_url(get_home_url()); ?> "> 
          <img src="<?php echo esc_url($logoUrl) ;?>" alt="<?php bloginfo('name'); ?>"> </a>
        <?php }  

}
}

if ( ! function_exists ( 'magikBolt_mobile_search' ) ) {
function magikBolt_mobile_search()
{ global $bolt_Options;
  $MagikBolt = new MagikBolt();
    if (isset($bolt_Options['header_remove_header_search']) && !$bolt_Options['header_remove_header_search']) : 
        echo'<div class="mobile-search">';
         echo magikBolt_mobile_search_form();
         echo'<div class="search-autocomplete" id="search_autocomplete1" style="display: none;"></div></div>';
         endif;
}
}


if ( ! function_exists ( 'magikBolt_search_form' ) ) {
 function magikBolt_search_form()
  {  
    global $bolt_Options;
  $MagikBolt = new MagikBolt();
  ?>
  <?php if (isset($bolt_Options['header_remove_header_search']) && !$bolt_Options['header_remove_header_search']) : ?>

<div class="search-box hidden-xs">
  
 <form name="myform"  method="GET" action="<?php echo esc_url(home_url('/')); ?>">
    
    <?php if (class_exists('WooCommerce')) : ?>
      <?php 
      if(isset($_REQUEST['product_cat']) && !empty($_REQUEST['product_cat']))
      {
       $optsetlect=$_REQUEST['product_cat'];
      }
      else{
      $optsetlect=0;  
      }
             $args = array(
                        'show_option_all' => esc_html__( 'All Categories', 'bolt' ),
                        'hierarchical' => 1,
                        'class' => 'cat',
                        'echo' => 1,
                        'orderby'=> 'product_cat',
                        'order' => 'asc',
                        'value_field' => 'slug',
                        'selected' => $optsetlect
                    );
              $args['taxonomy'] = 'product_cat';
              $args['name'] = 'product_cat';              
              $args['class'] = 'cate-dropdown hidden-xs';
              wp_dropdown_categories($args);

       ?>

      <input type="hidden" value="product" name="post_type">
    <?php endif; ?>
               <input type="text" class="mgksearch" name="s" maxlength="128" value="<?php echo get_search_query(); ?>" placeholder="<?php esc_attr_e('Search entire store here...', 'bolt'); ?>">
    
    <button type="submit" title="<?php esc_attr_e('Search', 'bolt'); ?>" class="search-btn-bg"><span><?php esc_attr_e('Search','bolt');?></span></button>
             
              

  </form>

</div>

   <?php  endif; ?>
  <?php
  }
}

if ( ! function_exists ( 'magikBolt_mobile_search_form' ) ) {
 function magikBolt_mobile_search_form()
  {  
    global $bolt_Options;
  $MagikBolt = new MagikBolt();
  ?>
  <?php if (isset($bolt_Options['header_remove_header_search']) && !$bolt_Options['header_remove_header_search']) : ?>

 <form name="myform"  method="GET" action="<?php echo esc_url(home_url('/')); ?>">
    
    <?php if (class_exists('WooCommerce')) : ?> 

      <input type="hidden" value="product" name="post_type">
    <?php endif; ?>
  <input type="text" class="mgksearch" name="s" maxlength="128" value="<?php echo get_search_query(); ?>" placeholder="<?php esc_attr_e('Search ...', 'bolt'); ?>">   
    <button type="submit" title="<?php esc_attr_e('Search', 'bolt'); ?>" class="btn btn-default"><i class="fa fa-search"></i></button>
                       
  </form>

   <?php  endif; ?>
  <?php
  }
}


if ( ! function_exists ( 'magikBolt_home_page_banner' ) ) {
function magikBolt_home_page_banner()
{
    global $bolt_Options;
     
        ?>
 <div class="container home-revslider">
      <div class="row">
        <div class="col-lg-3 col-md-4 hidden-xs">
          
        </div>

        <div class="col-lg-9 col-md-9 col-xs-12 home-slider">

        <?php  if(isset($bolt_Options['enable_home_gallery']) && $bolt_Options['enable_home_gallery']  && isset($bolt_Options['home-page-slider']) && !empty($bolt_Options['home-page-slider'])) { ?>
        
          <div id='rev_slider_4_wrapper' class='rev_slider_wrapper fullwidthbanner-container' >
            <div id='rev_slider_4' class='rev_slider fullwidthabanner'>
              <ul>
            <?php foreach ($bolt_Options['home-page-slider'] as $slide) : ?>
                               <li data-transition='random' data-slotamount='7' data-masterspeed='1000' data-thumb='<?php echo esc_url($slide['thumb']); ?>'>
                                       <img
                                        src="<?php echo esc_url($slide['image']); ?>" data-bgposition='left top' data-bgfit='cover' data-bgrepeat='no-repeat'
                                        alt="<?php echo esc_attr($slide['title']); ?>"/> <?php echo htmlspecialchars_decode($slide['description']); ?>
                                        <a class="s-link" href="<?php echo !empty($slide['url']) ? esc_url($slide['url']) : '#' ?>"></a>
                                </li>
                           
             <?php endforeach; ?>
            </ul>
            
          </div>
        </div>
    
      <?php } ?>
     </div>
    </div>
  

<script type='text/javascript'>
        jQuery(document).ready(function(){
            "use strict";
            jQuery('#rev_slider_4').show().revolution({
                dottedOverlay: 'none',
                delay: 5000,
                startwidth: 870,
                startheight: 490,

                hideThumbs: 200,
                thumbWidth: 200,
                thumbHeight: 50,
                thumbAmount: 2,

                navigationType: 'thumb',
                navigationArrows: 'solo',
                navigationStyle: 'round',

                touchenabled: 'on',
                onHoverStop: 'on',
                
                swipe_velocity: 0.7,
                swipe_min_touches: 1,
                swipe_max_touches: 1,
                drag_block_vertical: false,
            
                spinner: 'spinner0',
                keyboardNavigation: 'off',

                navigationHAlign: 'center',
                navigationVAlign: 'bottom',
                navigationHOffset: 0,
                navigationVOffset: 20,

                soloArrowLeftHalign: 'left',
                soloArrowLeftValign: 'center',
                soloArrowLeftHOffset: 20,
                soloArrowLeftVOffset: 0,

                soloArrowRightHalign: 'right',
                soloArrowRightValign: 'center',
                soloArrowRightHOffset: 20,
                soloArrowRightVOffset: 0,

                shadow: 0,
                fullWidth: 'on',
                fullScreen: 'off',

                stopLoop: 'off',
                stopAfterLoops: -1,
                stopAtSlide: -1,

                shuffle: 'off',

                autoHeight: 'off',
                forceFullWidth: 'on',
                fullScreenAlignForce: 'off',
                minFullScreenHeight: 0,
                hideNavDelayOnMobile: 1500,
            
                hideThumbsOnMobile: 'off',
                hideBulletsOnMobile: 'off',
                hideArrowsOnMobile: 'off',
                hideThumbsUnderResolution: 0,

                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                startWithSlide: 0,
                fullScreenOffsetContainer: ''
            });
        });
        </script> 

 
</div>

<?php 
   
}
}

if ( ! function_exists ( 'magikBolt_footer_service' ) ) {
function magikBolt_footer_service()
{
    global $bolt_Options;

if (isset($bolt_Options['footer_show_info_banner']) && !empty($bolt_Options['footer_show_info_banner'])) :
                  ?>
 <div class="our-features-box ">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <div class="feature-box">
            <div class="icon-truck"></div>
            <div class="content">
              <?php echo htmlspecialchars_decode($bolt_Options['footer_shipping_banner']); ?>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <div class="feature-box">
            <div class="icon-support"></div>
            <div class="content">
             <?php echo htmlspecialchars_decode($bolt_Options['footer_moneyback_banner']); ?>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <div class="feature-box">
            <div class="icon-money"></div>
            <div class="content">
             <?php echo htmlspecialchars_decode($bolt_Options['footer_customer_support_banner']); ?>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <div class="feature-box">
            <div class="icon-return"></div>
            <div class="content">
              <?php echo htmlspecialchars_decode($bolt_Options['footer_returnservice_banner']); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

    <?php
   
     endif;
}
}


if ( ! function_exists ( 'magikBolt_footer_signupform' ) ) {
function magikBolt_footer_signupform()
{
  global $bolt_Options;
if (isset($bolt_Options['enable_mailchimp_form']) && !empty($bolt_Options['enable_mailchimp_form'])) {
 if( function_exists('mc4wp_show_form'))
  {
  ?> 
        
  <div class="newsletter-wrap">
  <?php
    mc4wp_show_form();
   ?>
           
   </div>
  <?php
    } 
    }  

}
}

if ( ! function_exists ( 'magikBolt_side_banner_menu' ) ) {
function magikBolt_side_banner_menu()
{
  global $bolt_Options;
 ?>
  <div class="side-banner">

                <?php  if (isset($bolt_Options['enable_home_page_menu_banner']) && $bolt_Options['enable_home_page_menu_banner']){  ?>
                  
                   <a href="<?php echo !empty($bolt_Options['home_menu_banner_url']) ? esc_url($bolt_Options['home_menu_banner_url']) : '#' ?>">
                     <img src="<?php echo esc_url($bolt_Options['home_menu_banner']['url']); ?>" 
                    alt="<?php esc_attr_e('banner', 'bolt'); ?>">
                   </a> 
                  
               <?php } ?>
                

  </div>

 <?php 
          
}
}

if ( ! function_exists ( 'magikBolt_sidebar_banner' ) ) {
function magikBolt_sidebar_banner()
{
  global $bolt_Options;
  if (isset($bolt_Options['enable_home_page_sidebar_banner']) && $bolt_Options['enable_home_page_sidebar_banner']){
           ?>

          <div class="sidebar-banner">
            <a href="<?php echo !empty($bolt_Options['home_sidebar_banner_url']) ? esc_url($bolt_Options['home_sidebar_banner_url']) : '#' ?>">
            <img src="<?php echo esc_url($bolt_Options['home_sidebar_banner']['url']); ?>" 
                    alt="<?php esc_attr_e('banner', 'bolt'); ?>">
            </a> 
          </div>

          <?php } 

}
}


if ( ! function_exists ( 'magikBolt_footer_banner_section' ) ) {
function magikBolt_footer_banner_section()
{
  global $bolt_Options;
  if (isset($bolt_Options['enable_footer_banner_section']) && $bolt_Options['enable_footer_banner_section']){
           ?>

  <div class="top-banner-section">
    <div class="container">
      <div class="row">
        <div class="top-banner-inner">
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class="col">
              <a href="<?php echo !empty($bolt_Options['footer_banner_url1']) ? esc_url($bolt_Options['footer_banner_url1']) : '#' ?>">
                <img src="<?php echo esc_url($bolt_Options['footer_banner1']['url']); ?>" alt="<?php esc_attr_e('banner', 'bolt'); ?>">
              </a>
            </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class="col">
              <a href="<?php echo !empty($bolt_Options['footer_banner_url2']) ? esc_url($bolt_Options['footer_banner_url2']) : '#' ?>">
                <img src="<?php echo esc_url($bolt_Options['footer_banner2']['url']); ?>" alt="<?php esc_attr_e('banner', 'bolt'); ?>">
              </a>
            </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class="col">
               <a href="<?php echo !empty($bolt_Options['footer_banner_url3']) ? esc_url($bolt_Options['footer_banner_url3']) : '#' ?>">
                <img src="<?php echo esc_url($bolt_Options['footer_banner3']['url']); ?>" alt="<?php esc_attr_e('banner', 'bolt'); ?>">
              </a>
            </div>
          </div>
          <div class="ccol-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class="col">
               <a href="<?php echo !empty($bolt_Options['footer_banner_url4']) ? esc_url($bolt_Options['footer_banner_url4']) : '#' ?>">
                <img src="<?php echo esc_url($bolt_Options['footer_banner4']['url']); ?>" alt="<?php esc_attr_e('banner', 'bolt'); ?>">
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

          <?php } 

}
}

if ( ! function_exists ( 'magikBolt_featured_products' ) ) {
function magikBolt_featured_products()
{
    global $bolt_Options;
    if (isset($bolt_Options['enable_home_featured_products']) && !empty($bolt_Options['enable_home_featured_products'])) {
        ?>

<div class="featured-pro">
  <div class="slider-items-products">
      <div class="new_title center">
          <h2><?php esc_attr_e('Featured Product', 'bolt'); ?></h2>
      </div>
  <div id="new-products-slider" class="product-flexslider hidden-buttons">
   <div class="slider-items slider-width-col4 products-grid">

                
                <?php
                $args = array(
                    'post_type' => 'product',
                    'post_status' => 'publish',                  
                    'posts_per_page' => $bolt_Options['featured_per_page'], 
                    'tax_query' => array(
                      array(
                        'taxonomy' => 'product_visibility',
                        'field'    => 'name',
                        'terms'    => 'featured',
                    ),
                ),                 
                );
                $loop = new WP_Query($args);
                if ($loop->have_posts()) {
                    while ($loop->have_posts()) : $loop->the_post();
                        magikBolt_productitem_template();
                    endwhile;
                } else {
                    esc_attr_e('No products found','bolt');
                }

                wp_reset_postdata();
                ?>

          </div>
      </div>
    </div>
  </div>
    <?php
    }
}
}


if ( ! function_exists ( 'magikBolt_bestseller_products' ) ) {
function magikBolt_bestseller_products()
{
   global $bolt_Options;

if (isset($bolt_Options['enable_home_bestseller_products']) && !empty($bolt_Options['enable_home_bestseller_products'])) { 
  ?>

<div class="featured-products">
  <div class="slider-items-products">
     <div class="new_title center">
         <h2><?php esc_attr_e('Top Seller', 'bolt'); ?></h2>
      </div>
    <div id="featured-products-slider" class="product-flexslider hidden-buttons">
    <div class="slider-items slider-width-col4 products-grid">

       <?php
                
                              $args = array(
                              'post_type'       => 'product',
                              'post_status'       => 'publish',
                              'ignore_sticky_posts'   => 1,
                              'posts_per_page' => $bolt_Options['bestseller_per_page'],      
                              'meta_key'        => 'total_sales',
                              'orderby'         => 'meta_value_num',
                              'order' => 'DESC' 
                              );

                                $loop = new WP_Query( $args );
                             
                                if ( $loop->have_posts() ) {
                                while ( $loop->have_posts() ) : $loop->the_post();                  
                                magikBolt_productitem_template();
                                endwhile;
                                } else {
                                esc_attr_e( 'No products found', 'bolt' );
                                }

                               wp_reset_postdata();
                             
                             
?>

         
     </div>
    </div>
  </div>
</div>
 <?php  } ?>
<?php 

}
}

if ( ! function_exists ( 'magikBolt_new_products' ) ) {
function magikBolt_new_products()
{
   global $bolt_Options;

if (isset($bolt_Options['enable_home_new_products']) && !empty($bolt_Options['enable_home_new_products'])) { 
  ?>

 <div class="accessories-pro">
  <div class="slider-items-products">
      <div class="new_title center">
          <h2><?php esc_attr_e('New Products', 'bolt'); ?></h2>
      </div>
  <div id="accessories-slider" class="product-flexslider hidden-buttons">
    <div class="slider-items slider-width-col4 products-grid">

<?php

                              $args = array(
                              'post_type'       => 'product',
                              'post_status'       => 'publish',
                              'ignore_sticky_posts'   => 1,
                              'posts_per_page' => $bolt_Options['new_products_per_page'],      
                              'orderby' => 'date',
                              'order' => 'DESC',
                              
                              );

                                $loop = new WP_Query( $args );
                             
                                if ( $loop->have_posts() ) {
                                while ( $loop->have_posts() ) : $loop->the_post();                  
                                magikBolt_productitem_template();
                                endwhile;
                                } else {
                                esc_attr_e( 'No products found', 'bolt' );
                                }

                               wp_reset_postdata();


                             
?>
     </div>
    </div>
   </div>
  </div>

<?php 
}
}
}


if ( ! function_exists ( 'magikBolt_hotdeal_product' ) ) {
function magikBolt_hotdeal_product()
{
   global $bolt_Options;
if (isset($bolt_Options['enable_home_hotdeal_products']) && !empty($bolt_Options['enable_home_hotdeal_products'])) { 
  
  ?>
    <div class="hot-deal">
            <ul class="products-grid">
  <?php
       $args = array(
            'post_type'      => 'product',
            'posts_per_page' => 1,
            'meta_key' => 'hotdeal_on_home',
             'meta_value' => 'yes',
            'meta_query'     => array(
          
              array(
                    'relation' => 'OR',
                    array( // Simple products type
                        'key'           => '_sale_price',
                        'value'         => 0,
                        'compare'       => '>',
                        'type'          => 'numeric'
                    ),
                  
                    array( // Variable products type
                        'key'           => '_min_variation_sale_price',
                        'value'         => 0,
                        'compare'       => '>',
                        'type'          => 'numeric'
                    )
                    )
                 
                )
        );

        $loop = new WP_Query( $args );
        if ( $loop->have_posts() ) {
            while ( $loop->have_posts() ) : $loop->the_post();
              magikBolt_hotdeal_template();
            
            endwhile;
        } else {
        esc_attr_e( 'No products found', 'bolt' );
        }
        wp_reset_postdata();
    ?>


  </ul>
</div>
         
  <?php
}
}
}

if ( ! function_exists ( 'magikBolt_productitem_template' ) ) {
function magikBolt_productitem_template()
{
  
  $MagikBolt = new MagikBolt();
  global $product, $yith_wcwl,$post;
   $imageUrl = wc_placeholder_img_src();
   if (has_post_thumbnail())
      $imageUrl =  wp_get_attachment_image_src(get_post_thumbnail_id(),'magikBolt-product-size-large');
   
   ?>

<div class="item">
  <div class="item-inner">
      <div class="item-img">
         <div class="item-img-info">

              <a href="<?php the_permalink(); ?>" title="<?php echo esc_html($post->post_title); ?>" class="product-image">
                <figure class="img-responsive">
                     <img alt="<?php echo esc_html($post->post_title); ?>" src="<?php echo esc_url($imageUrl[0]); ?>">
                </figure>
               </a>
         
                 <?php if ($product->is_on_sale()) : ?>
                     <div class="sale-label sale-top-right">
                         <?php esc_attr_e('Sale', 'bolt'); ?>
                     </div>
                 <?php endif; ?>
                

                 <?php if (class_exists('YITH_WCQV_Frontend')) { ?>
                    <a class="yith-wcqv-button link-quickview quickview-btn" href="#"
                      data-product_id="<?php echo esc_html($product->get_id()); ?>"><span><?php esc_attr_e('Quick View', 'bolt'); ?></span></a>
                  <?php } ?>
             </div>
          </div>
    <div class="item-info">
      <div class="info-inner">

          <div class="item-title">
            <a href="<?php the_permalink(); ?>"
               title="<?php echo esc_html($post->post_title); ?>"> <?php echo esc_html($post->post_title); ?> </a>
            </div>

           <div class="item-content">
              <div class="rating">
                 <div class="ratings">
                    <div class="rating-box">
                       <?php $average = $product->get_average_rating(); ?>
                          <div style="width:<?php echo esc_html(($average / 5) * 100); ?>%" class="rating"> 
                          </div>
                      </div>
                  </div>       
               </div>

            <div class="item-price">
              <div class="price-box">
                 <span class="regular-price"> 
                    <?php echo htmlspecialchars_decode($product->get_price_html()); ?>
                  </span>
              </div>
            </div>

           <div class="actions">

                  <?php if (isset($yith_wcwl) && is_object($yith_wcwl)) {
                      $classes = get_option('yith_wcwl_use_button') == 'yes' ? 'class="add_to_wishlist link-wishlist"' : 'class="add_to_wishlist link-wishlist"';
                  ?>
                  <a  title="<?php esc_attr_e('Add to Wishlist', 'bolt'); 
                              ?>" href="<?php echo esc_url(add_query_arg('add_to_wishlist', $product->get_id())) ?>"  data-product-id="<?php echo esc_html($product->get_id()); ?>"
                    data-product-type="<?php echo esc_html($product->get_type()); ?>" <?php echo htmlspecialchars_decode($classes); ?>></a> 
                  <?php }?> 

                  <?php magikBolt_woocommerce_product_add_to_cart_text() ;?>

                   <?php if (class_exists('YITH_Woocompare_Frontend')) {
                    $mgk_yith_cmp = new YITH_Woocompare_Frontend; 
                    ?>      
                    <a title="<?php esc_attr_e('Add to Compare', 'bolt'); 
                              ?>" href="<?php echo esc_url($mgk_yith_cmp->add_product_url($product->get_id())); ?>" class="compare link-compare add_to_compare" data-product_id="<?php echo esc_html($product->get_id()); ?>"></a>
                   <?php } ?>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>





<?php

}
}


if ( ! function_exists ( 'magikBolt_related_upsell_template' ) ) {
function magikBolt_related_upsell_template()
{
  $MagikBolt = new MagikBolt();
 global $product, $yith_wcwl,$post;


$imageUrl = wc_placeholder_img_src();
if (has_post_thumbnail())
    $imageUrl =  wp_get_attachment_image_src(get_post_thumbnail_id(),'magikBolt-product-size-large');  
?>


<div class="item">
  <div class="item-inner">
      <div class="item-img">
         <div class="item-img-info">

              <a href="<?php the_permalink(); ?>" title="<?php echo esc_html($post->post_title); ?>" class="product-image">
                <figure class="img-responsive">
                     <img alt="<?php echo esc_html($post->post_title); ?>" src="<?php echo esc_url($imageUrl[0]); ?>">
                </figure>
               </a>
         
                 <?php if ($product->is_on_sale()) : ?>
                     <div class="sale-label sale-top-right">
                         <?php esc_attr_e('Sale', 'bolt'); ?>
                     </div>
                 <?php endif; ?>
                

                 <?php if (class_exists('YITH_WCQV_Frontend')) { ?>
                    <a class="yith-wcqv-button link-quickview quickview-btn" href="#"
                      data-product_id="<?php echo esc_html($product->get_id()); ?>"><span><?php esc_attr_e('Quick View', 'bolt'); ?></span></a>
                  <?php } ?>
             </div>
          </div>
    <div class="item-info">
      <div class="info-inner">

          <div class="item-title">
            <a href="<?php the_permalink(); ?>"
               title="<?php echo esc_html($post->post_title); ?>"> <?php echo esc_html($post->post_title); ?> </a>
            </div>

           <div class="item-content">
              <div class="rating">
                 <div class="ratings">
                    <div class="rating-box">
                       <?php $average = $product->get_average_rating(); ?>
                          <div style="width:<?php echo esc_html(($average / 5) * 100); ?>%" class="rating"> 
                          </div>
                      </div>
                  </div>       
               </div>

            <div class="item-price">
              <div class="price-box">
                 <span class="regular-price"> 
                    <?php echo htmlspecialchars_decode($product->get_price_html()); ?>
                  </span>
              </div>
            </div>

           <div class="actions">

                  <?php if (isset($yith_wcwl) && is_object($yith_wcwl)) {
                      $classes = get_option('yith_wcwl_use_button') == 'yes' ? 'class="add_to_wishlist link-wishlist"' : 'class="add_to_wishlist link-wishlist"';
                  ?>
                  <a  title="<?php esc_attr_e('Add to Wishlist', 'bolt'); 
                              ?>" href="<?php echo esc_url(add_query_arg('add_to_wishlist', $product->get_id())) ?>"  data-product-id="<?php echo esc_html($product->get_id()); ?>"
                    data-product-type="<?php echo esc_html($product->get_type()); ?>" <?php echo htmlspecialchars_decode($classes); ?>></a> 
                  <?php }?> 

                  <?php magikBolt_woocommerce_product_add_to_cart_text() ;?>

                   <?php if (class_exists('YITH_Woocompare_Frontend')) {
                    $mgk_yith_cmp = new YITH_Woocompare_Frontend; 
                    ?>      
                    <a title="<?php esc_attr_e('Add to Compare', 'bolt'); 
                              ?>" href="<?php echo esc_url($mgk_yith_cmp->add_product_url($product->get_id())); ?>" class="compare link-compare add_to_compare" data-product_id="<?php echo esc_html($product->get_id()); ?>"></a>
                   <?php } ?>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<?php
}
}


if ( ! function_exists ( 'magikBolt_hotdeal_template' ) ) {
function magikBolt_hotdeal_template()
{
$MagikBolt = new MagikBolt();
 global $product, $yith_wcwl,$post;
   $imageUrl = wc_placeholder_img_src();
   if (has_post_thumbnail())
          $imageUrl =  wp_get_attachment_image_src(get_post_thumbnail_id(),'magikBolt-product-size-large');

 $product_type = $product->get_type();
            
              if($product_type=='variable')
              {
               $available_variations = $product->get_available_variations();
               $variation_id=$available_variations[0]['variation_id'];
                $newid=$variation_id;
              }
              else
              {
                $newid=$post->ID;
           
              }                                    
               $sales_price_to = get_post_meta($newid, '_sale_price_dates_to', true);  
               if(!empty($sales_price_to))
               {
               $sales_price_date_to = date("Y/m/d", $sales_price_to);
               } 
               else{
                $sales_price_date_to='';
              } 
               $curdate=date("m/d/y h:i:s A");                         
?> 

<li class="right-space two-height item">
  <div class="item-inner">

    <div class="item-img">

       <div class="item-img-info">

        <a href="<?php the_permalink(); ?>" title="<?php echo esc_html($post->post_title); ?>" class="product-image">
                    <figure class="img-responsive">
                      <img alt="<?php echo esc_html($post->post_title); ?>" src="<?php echo esc_url($imageUrl[0]); ?>">
                    </figure>
         </a>

                    <?php if ($product->is_on_sale()) : ?>
                          <div class="hot-label hot-top-left">
                             <?php esc_attr_e('Hot Deal', 'bolt'); ?>
                          </div>
                    <?php endif; ?>
  
          <div class="box-timer">
             <div class="countbox_1 timer-grid"  data-time="<?php echo esc_html($sales_price_date_to) ;?>">
              </div>
          </div>
       </div>
     </div>

  <div class="item-info">
      <div class="info-inner">

          <div class="item-title">
            <a href="<?php the_permalink(); ?>"
               title="<?php echo esc_html($post->post_title); ?>"> <?php echo esc_html($post->post_title); ?> </a>
            </div>

           <div class="item-content">
              <div class="rating">
                 <div class="ratings">
                    <div class="rating-box">
                       <?php $average = $product->get_average_rating(); ?>
                          <div style="width:<?php echo esc_html(($average / 5) * 100); ?>%" class="rating"> 
                          </div>
                      </div>
                  </div>       
               </div>

            <div class="item-price">
              <div class="price-box">
                 <span class="regular-price"> 
                    <?php echo htmlspecialchars_decode($product->get_price_html()); ?>
                  </span>
              </div>
            </div>

           <div class="actions">

                  <?php if (isset($yith_wcwl) && is_object($yith_wcwl)) {
                      $classes = get_option('yith_wcwl_use_button') == 'yes' ? 'class="add_to_wishlist link-wishlist"' : 'class="add_to_wishlist link-wishlist"';
                  ?>
                  <a  title="<?php esc_attr_e('Add to Wishlist', 'bolt'); 
                              ?>" href="<?php echo esc_url(add_query_arg('add_to_wishlist', $product->get_id())) ?>"  data-product-id="<?php echo esc_html($product->get_id()); ?>"
                    data-product-type="<?php echo esc_html($product->get_type()); ?>" <?php echo htmlspecialchars_decode($classes); ?>></a> 
                  <?php }?> 

                  <?php magikBolt_woocommerce_product_add_to_cart_text() ;?>

                   <?php if (class_exists('YITH_Woocompare_Frontend')) {
                    $mgk_yith_cmp = new YITH_Woocompare_Frontend; 
                    ?>      
                    <a title="<?php esc_attr_e('Add to Compare', 'bolt'); 
                              ?>" href="<?php echo esc_url($mgk_yith_cmp->add_product_url($product->get_id())); ?>" class="compare link-compare add_to_compare" data-product_id="<?php echo esc_html($product->get_id()); ?>"></a>
                   <?php } ?>

          </div>
        </div>
      </div>
    </div>

 </div>
</li>   


<?php
}
}


if ( ! function_exists ( 'magikBolt_footer_brand_logo' ) ) {
function magikBolt_footer_brand_logo()
  {
    global $bolt_Options;
    if (isset($bolt_Options['enable_brand_logo']) && $bolt_Options['enable_brand_logo'] && !empty($bolt_Options['all-company-logos'])) : ?>

<div class="container brand-logo-block">
  <div class="brand-logo">
    <div class="new_title center">
      <h2><?php esc_attr_e('TOP BRANDS', 'bolt'); ?></h2>
     </div>
    <div class="slider-items-products">
      <div id="brand-logo-sliderv4" class="product-flexslider hidden-buttons">
        <div class="slider-items slider-width-col6">  

                 <?php foreach ($bolt_Options['all-company-logos'] as $_logo) : ?>
                  <div class="item">
                    <a href="<?php echo esc_url($_logo['url']); ?>" target="_blank"> <img
                        src="<?php echo esc_url($_logo['image']); ?>" 
                        alt="<?php echo esc_attr($_logo['title']); ?>"> </a>
                  </div>
                  <?php endforeach; ?>

                      
         </div>
        </div>
      </div>
    </div>
  </div>

    
  <?php endif;
  }
}

if ( ! function_exists ( 'magikBolt_home_blog_posts' ) ) {
function magikBolt_home_blog_posts()
{
    $count = 0;
    global $bolt_Options;
    $MagikBolt = new MagikBolt();
    if (isset($bolt_Options['enable_home_blog_posts']) && !empty($bolt_Options['enable_home_blog_posts'])) {
        ?>


 <div class="latest-blog">
    <div class="new_title center">
        <h2><?php esc_attr_e('Latest Blog', 'bolt'); ?></h2>
     </div>
 <div class="row">
    <div class="blog-outer-container">
       <div class="blog-inner">

        <?php

        $args = array('posts_per_page' => 2, 'post__not_in' => get_option('sticky_posts'));
        $the_query = new WP_Query($args);
           $i=1;  
        if ($the_query->have_posts()) :
            while ($the_query->have_posts()) : $the_query->the_post(); ?>
            

             <div class="col-xs-12 col-sm-6 blog-preview_item">
                    <div class="entry-thumb image-hover2"> 

                      <a href="<?php the_permalink(); ?>">
                        <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'single-post-thumbnail'); ?>
                        <img src="<?php echo esc_url($image[0]); ?>" alt="<?php the_title(); ?>"></a> 

                    </div>

                    <h4 class="blog-preview_title">
                      <a href="<?php the_permalink(); ?>"><?php esc_html(the_title()); ?>
                     </a>
                    </h4>

                    <div class="blog-preview_info">
                      <ul class="post-meta">

                         <li><i class="fa fa-user"></i><?php esc_attr_e('posted by ', 'bolt'); ?> <a href="<?php comments_link(); ?>"><?php the_author(); ?></a></li>

                          <li><i class="fa fa-comment"></i><a href="<?php comments_link(); ?>"><?php comments_number('0 Comment', '1 Comment', '% Comments'); ?></a>
                          </li>

                          <li><i class="fa fa-calendar"></i><?php esc_html(the_time(get_option('date_format'))); ?>
                         </li>

                      </ul>

                      <div class="blog-preview_desc"><?php the_excerpt(); ?></div>

                   </div>
             </div>

              
            <?php    $i++;
             endwhile; ?>
            <?php wp_reset_postdata(); ?>
        <?php else: ?>
            <p>
                <?php esc_attr_e('Sorry, no posts matched your criteria.', 'bolt'); ?>
            </p>
        <?php endif;
        ?>

                  
        </div>
     </div>
    </div>
  </div>


<?php
    }
}
}



// new ccustom code section added

if(!function_exists('magikBolt_home_customsection'))
{
 function magikBolt_home_customsection()
 {
   global $bolt_Options;
   ?>

   <div class="container">
    <div class="row">
     <div class="mgk_custom_block">
       <div class="col-md-12">
       <?php the_content(); ?>
     </div>
     </div>
   </div>
 </div>
 <?php
}

}