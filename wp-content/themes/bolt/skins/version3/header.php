<!DOCTYPE html>
<html <?php language_attributes(); ?> id="parallax_scrolling">
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
 <?php wp_head(); ?>
</head>
<?php
 $MagikBolt = new MagikBolt(); ?>
<body <?php body_class(); ?> >
  <div id="page" class="page catalog-category-view">


<header>
    <div class="header-container">
      <div class="header-top">
        <div class="container">
          <div class="row"> 
            <!-- Header Language -->
            <div class="col-md-7 col-sm-5 col-xs-12">
              <?php echo magikBolt_currency_language(); ?>

                 <div class="welcome-msg hidden-xs">  
                   <?php echo magikBolt_msg(); ?>
                 </div>
            </div>
            <div class="col-md-5 col-sm-7 hidden-xs"> 
              
              <!-- Header Top Links -->
              <div class="toplinks">
                  <?php if ( has_nav_menu( 'toplinks' ) )
                    { ?>
                         <div class="links">
                           <?php echo magikBolt_top_navigation(); ?>
                         </div>
                    <?php  }?>
              </div>
              <!-- End Header Top Links --> 
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>

 <nav>
    <div class="container">
      <div class="row">
        <div class="col-lg-2 col-sm-2 col-xs-12"> 
          <!-- Header Logo -->
          <div class="logo">
            <?php magikBolt_logo_image();?>
          </div>
          <!-- End Header Logo --> 
        </div>
            <div class="mm-toggle-wrap">
            <div class="mm-toggle">  <a class="mobile-toggle"><i class="fa fa-reorder"></i></a></div>
          </div>
        <div class="nav-inner col-lg-10 col-md-10 col-sm-10">
           <div class="mgk-main-menu">
              <div id="main-menu">
                <?php if(has_nav_menu('main_menu'))
                  { ?> 

                      <?php echo magikBolt_main_menu(); ?>                  
            
            <?php  }?>
           </div>
      </div>
            <?php
              if (class_exists('WooCommerce')) :?>
            <div class="top-cart-contain pull-right"> 
                 <?php  magikBolt_mini_cart(); ?>
                  
           </div>
          <?php endif;?>
             
           <?php magikBolt_search_form();?>
       

       </div>
      </div>
    </div>
  </nav>


    <?php if (class_exists('WooCommerce') && is_woocommerce()) : ?>
     <div class="page-heading">

       <?php if(is_product_category() || is_shop()){?>
        <div class="page-title">
             <?php if (apply_filters('woocommerce_show_page_title', true)) : ?>
                            <h2>
                                <?php esc_html(woocommerce_page_title()); ?>
                            </h2>
                        <?php endif; ?>
             </div>              
         <?php } ?>
 
      
          <div class="breadcrumbs">
            <div class="container">
              <div class="row">
                <div class="col-xs-12">
                  <?php woocommerce_breadcrumb(); ?>
              </div>
 </div>
           </div>
       </div>
       
       </div>
      <?php endif; ?> 
        