<?php

if ( ! function_exists ( 'magikBolt_currency_language' ) ) {
function magikBolt_currency_language()
{ 
     global $bolt_Options;


        if(isset($bolt_Options['enable_header_language']) && ($bolt_Options['enable_header_language']!=0))
        { ?>
          <div class="dropdown block-language-wrapper"> 
            <a role="button" data-toggle="dropdown" data-target="#" class="block-language dropdown-toggle" href="#"> 
              <img src="<?php echo esc_url(get_template_directory_uri()) ;?>/images/english.png" alt="<?php esc_attr_e('English', 'bolt');?>">  
              <?php esc_attr_e('English', 'bolt');?><span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
              <li role="presentation"><a role="menuitem" tabindex="-1" href="#"><img src="<?php echo esc_url(get_template_directory_uri()) ;?>/images/english.png" alt="<?php esc_attr_e('English', 'bolt');?>">    <?php esc_attr_e('English', 'bolt');?></a></li>
              <li role="presentation"><a role="menuitem" tabindex="-1" href="#"><img src="<?php echo esc_url(get_template_directory_uri()) ;?>/images/francais.png" alt="<?php esc_attr_e('French', 'bolt');?>"> <?php esc_attr_e('French', 'bolt');?> </a></li>
              <li role="presentation"><a role="menuitem" tabindex="-1" href="#"><img src="<?php echo esc_url(get_template_directory_uri()) ;?>/images/german.png" alt="<?php esc_attr_e('German', 'bolt');?>">   <?php esc_attr_e('German', 'bolt');?></a></li>
            </ul>
          </div>
        <?php  
        } ?>
        
            
   <?php if(isset($bolt_Options['enable_header_currency']) && ($bolt_Options['enable_header_currency']!=0))
    { 

     global $wp,$wp_query;
     $cururl=magikBolt_curPageURL();
     if(class_exists('WOOCS'))
     {
       $usd_url =  add_query_arg( array("currency"=>"USD"), $cururl);
       $gbp_url =  add_query_arg( array("currency"=>"GBP"),  $cururl);

       $WOOCS=new WOOCS();

     $currency_selected=$WOOCS->current_currency;
     }
     else{
      
      $usd_url = "#";
      $gbp_url ="#";
      $currency_selected="USD";
    }

    if((isset($_GET['currency']) && $_GET['currency']=='GBP') || $currency_selected=='GBP')
    {
     
     $elected_currency='<a role="button" data-toggle="dropdown" data-target="#" class="block-currency dropdown-toggle" href="'. esc_url($gbp_url).'">'  
     . esc_attr("POUND", "bolt").' <span class="caret"></span></a>';
     
   }
   else{
     $elected_currency='<a role="button" data-toggle="dropdown" data-target="#" class="block-currency dropdown-toggle" href="'.esc_url($usd_url).'">'  
     . esc_attr("USD", "bolt").' <span class="caret"></span></a>';
   }


   ?>

   <div class="dropdown block-currency-wrapper"> 
     <?php echo $elected_currency;?>
     <ul class="dropdown-menu" role="menu">
      <li role="presentation">
        <a role="menuitem" tabindex="-1" href="<?php echo esc_url($usd_url);?>">
          <?php esc_attr_e('$ - Dollar', 'bolt');?>
        </a>
      </li>
      
      <li role="presentation">
        <a role="menuitem" tabindex="-1" href="<?php echo esc_url($gbp_url) ;?>">
          <?php esc_attr_e('&#163; - Pound', 'bolt');?>
        </a>
      </li>
    </ul>
  </div>
  <?php  
} 
}
}

if ( ! function_exists ( 'magikBolt_msg' ) ) {
function magikBolt_msg()
{ 
     global $bolt_Options;

        if (isset($bolt_Options['enable_welcome_msg']) && $bolt_Options['enable_welcome_msg'])
             {
           if (is_user_logged_in()) {
            global $current_user;
          
              if(isset($bolt_Options['welcome_msg'])) {
            echo esc_attr_e('You are logged in as', 'bolt'). '   <b>'. esc_attr($current_user->display_name) .'</b>';
          }
          }
          else{
            if(isset($bolt_Options['welcome_msg']) && ($bolt_Options['welcome_msg']!='')){
            echo htmlspecialchars_decode($bolt_Options['welcome_msg']);
            }
          } 
        }
}
}

if ( ! function_exists ( 'magikBolt_logo_image' ) ) {
function magikBolt_logo_image()
{ 
     global $bolt_Options;
    
        $logoUrl = get_template_directory_uri() . '/images/logo.png';
        
        if (isset($bolt_Options['header_use_imagelogo']) && $bolt_Options['header_use_imagelogo'] == 0) {           ?>
        <a class="logo-title"  title="<?php bloginfo('name'); ?>" href="<?php echo esc_url(get_home_url()); ?> ">
        <?php bloginfo('name'); ?>
        </a>
        <?php
        } else if (isset($bolt_Options['header_logo']['url']) && !empty($bolt_Options['header_logo']['url'])) { 
                  $logoUrl = $bolt_Options['header_logo']['url'];
                  ?>
        <a title="<?php bloginfo('name'); ?>" href="<?php echo esc_url(get_home_url()); ?> "> <img
                      alt="<?php bloginfo('name'); ?>" src="<?php echo esc_url($logoUrl); ?>"
                      height="<?php echo !empty($bolt_Options['header_logo_height']) ? esc_html($bolt_Options['header_logo_height']) : ''; ?>"
                      width="<?php echo !empty($bolt_Options['header_logo_width']) ? esc_html($bolt_Options['header_logo_width']) : ''; ?>"> </a>
        <?php
        } else { ?>
        <a title="<?php bloginfo('name'); ?>" href="<?php echo esc_url(get_home_url()); ?> "> 
          <img src="<?php echo esc_url($logoUrl) ;?>" alt="<?php bloginfo('name'); ?>"> </a>
        <?php }  

}
}

if ( ! function_exists ( 'magikBolt_mobile_search' ) ) {
function magikBolt_mobile_search()
{ global $bolt_Options;
  $MagikBolt = new MagikBolt();
    if (isset($bolt_Options['header_remove_header_search']) && !$bolt_Options['header_remove_header_search']) : 
        echo'<div class="mobile-search">';
         echo magikBolt_mobile_search_form();
         echo'<div class="search-autocomplete" id="search_autocomplete1" style="display: none;"></div></div>';
         endif;
}
}

if ( ! function_exists ( 'magikBolt_search_form' ) ) {
 function magikBolt_search_form()
  {  
    global $bolt_Options;
  $MagikBolt = new MagikBolt();
  ?>
  <?php if (isset($bolt_Options['header_remove_header_search']) && !$bolt_Options['header_remove_header_search']) : ?>

        <div class="top-search">
            <div class="block-icon pull-right"> 

              <a data-target=".bs-example-modal-lg" data-toggle="modal" class="search-focus dropdown-toggle links"> <i class="fa fa-search"></i> </a>

              <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button aria-label="Close" data-dismiss="modal" class="close" type="button"><img src="<?php echo esc_url(get_template_directory_uri()) ;?>/images/interstitial-close.png" alt="<?php esc_attr_e('close', 'bolt') ;?>"> </button>
                    </div>
                    <div class="modal-body">

                      <form class="navbar-form" role="search" name="myform"  method="GET" action="<?php echo esc_url(home_url('/')); ?>">
                        <div class="mgksearch">
                          <div class="input-group">
                            <input type="text" placeholder="<?php esc_attr_e('Search','bolt');?>"  name="s" class="mgksearch" value="<?php echo get_search_query(); ?>">
                              <?php if (class_exists('WooCommerce')) : ?>    
                               <input type="hidden" value="product" name="post_type">
                             <?php endif; ?>  
                            <button type="submit" class="btn-search"><i class="fa fa-search"></i></button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div> 



   <?php  endif; ?>
  <?php
  }
}


if ( ! function_exists ( 'magikBolt_mobile_search_form' ) ) {
   function magikBolt_mobile_search_form()
  {  
    global $bolt_Options;
  $MagikBolt = new MagikBolt();
  ?>
  <?php if (isset($bolt_Options['header_remove_header_search']) && !$bolt_Options['header_remove_header_search']) : ?>

    <form class="navbar-form" role="search" name="myform"  method="GET" action="<?php echo esc_url(home_url('/')); ?>">
                        <div class="mgksearch">
                          <div class="input-group">
                            <input type="text" placeholder="<?php esc_attr_e('Search','bolt');?>"  name="s" class="mgksearch" value="<?php echo get_search_query(); ?>">
                              <?php if (class_exists('WooCommerce')) : ?>    
                               <input type="hidden" value="product" name="post_type">
                             <?php endif; ?>  
                            <button type="submit" class="btn-search"><i class="fa fa-search"></i></button>
                          </div>
                        </div>
                      </form>
   <?php  endif; ?>
  <?php
  }
}
  

if ( ! function_exists ( 'magikBolt_home_page_banner' ) ) {  
function magikBolt_home_page_banner()
{
    global $bolt_Options;
     
        ?>

 <div id="magik-slideshow" class="magik-slideshow">
   <div class="container">
        <?php  if(isset($bolt_Options['enable_home_gallery']) && $bolt_Options['enable_home_gallery']  && isset($bolt_Options['home-page-slider']) && !empty($bolt_Options['home-page-slider'])) { ?>
        
          <div id="rev_slider_4_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="classicslider1" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;"> 
        
             <div id="rev_slider_4_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
              <ul>
                
                   <?php foreach($bolt_Options['home-page-slider'] as $slide):
                    $imgtag='<img src="'. esc_url($slide['image']).'"  alt="'.esc_html($slide['title']).'"  />';
                    $thumb=  str_replace("{IMGTHUMB}",$slide['thumb'],$slide['description']);

                    $title=  str_replace("{IMGTITLE}",$slide['title'],$thumb);
                    $newdesc= str_replace("{IMGSRC}",$imgtag,$title);
                   ?>
                   <?php echo htmlspecialchars_decode($newdesc); ?> 
                   <?php endforeach; ?>

             </ul>
            <div class="tp-static-layers"></div>
          <div class="tp-bannertimer" style="height: 7px; background-color: rgba(255, 255, 255, 0.25);"></div>
        </div>
      </div>
      <?php } ?>
</div> 

<script type="text/javascript">
          var tpj=jQuery;     
          var revapi4;
          tpj(document).ready(function() {
              "use strict";
            if(tpj("#rev_slider_4_1").revolution == undefined){
              revslider_showDoubleJqueryError("#rev_slider_4_1");
            }else{
              revapi4 = tpj("#rev_slider_4_1").show().revolution({
                sliderType:"standard",
                sliderLayout:"fullwidth",
                dottedOverlay:"none",
                delay:9000,
                navigation: {
                  keyboardNavigation:"off",
                  keyboard_direction: "horizontal",
                  mouseScrollNavigation:"off",
                  onHoverStop:"off",
                  touch:{
                    touchenabled:"on",
                    swipe_threshold: 75,
                    swipe_min_touches: 1,
                    swipe_direction: "horizontal",
                    drag_block_vertical: false
                  }
                  ,
                  arrows: {
                    style:"zeus",
                    enable:true,
                    hide_onmobile:true,
                    hide_under:750,
                    hide_onleave:true,
                    hide_delay:200,
                    hide_delay_mobile:1200,
                    tmp:'<div class="tp-title-wrap">    <div class="tp-arr-imgholder"></div> </div>',
                    left: {
                      h_align:"left",
                      v_align:"center",
                      h_offset:30,
                      v_offset:0
                    },
                    right: {
                      h_align:"right",
                      v_align:"center",
                      h_offset:30,
                      v_offset:0
                    }
                  }
                  ,
                  bullets: {
                    enable:true,
                    hide_onmobile:true,
                    hide_under:600,
                    style:"metis",
                    hide_onleave:true,
                    hide_delay:200,
                    hide_delay_mobile:1200,
                    direction:"horizontal",
                    h_align:"center",
                    v_align:"bottom",
                    h_offset:0,
                    v_offset:30,
                    space:5,
                    tmp:'<span class="tp-bullet-img-wrap">  <span class="tp-bullet-image"></span></span><span class="tp-bullet-title">{{title}}</span>'
                  }
                },
                viewPort: {
                  enable:true,
                  outof:"pause",
                  visible_area:"80%"
                },
                responsiveLevels:[1240,1024,778,480],
                gridwidth:[768,1024,778,480],
                gridheight:[750,600,500,400],
                lazyType:"none",
                parallax: {
                  type:"mouse",
                  origo:"slidercenter",
                  speed:2000,
                  levels:[2,3,4,5,6,7,12,16,10,50],
                },
                shadow:0,
                spinner:"off",
                stopLoop:"off",
                stopAfterLoops:-1,
                stopAtSlide:-1,
                shuffle:"off",
                autoHeight:"off",
                hideThumbsOnMobile:"off",
                hideSliderAtLimit:0,
                hideCaptionAtLimit:0,
                hideAllCaptionAtLilmit:0,
                debugMode:false,
                fallbacks: {
                  simplifyAll:"off",
                  nextSlideOnWindowFocus:"off",
                  disableFocusListener:false,
                }
              });
            }
          }); /*ready*/
        </script>
 
</div>

<?php 
   
}
}

if ( ! function_exists ( 'magikBolt_header_service' ) ) {
function magikBolt_header_service()
{
    global $bolt_Options;

if (isset($bolt_Options['header_show_info_banner']) && !empty($bolt_Options['header_show_info_banner'])) :
                  ?>


 <div class="our-features-box">
    <div class="container">
      <ul>
        <li>
          <div class="feature-box"> <span class="icon-globe-alt"></span>
            <div class="content">
              <h3><?php echo htmlspecialchars_decode($bolt_Options['header_shipping_banner']); ?></h3>
              <p><?php echo htmlspecialchars_decode($bolt_Options['header_shipping_banner_dsc']); ?></p>
            </div>
          </div>
        </li>
        <li class="seprator-line"></li>
        <li>
          <div class="feature-box"> <span class="icon-support"></span>
            <div class="content">
              <h3><?php echo htmlspecialchars_decode($bolt_Options['header_customer_support_banner']); ?></h3>
              <p><?php echo htmlspecialchars_decode($bolt_Options['header_customer_support_banner_dsc']); ?></p>
            </div>
          </div>
        </li>
        <li class="seprator-line"></li>
        <li class="last">
          <div class="feature-box"> <span class="icon-share-alt"></span>
            <div class="content">
              <h3><?php echo htmlspecialchars_decode($bolt_Options['header_returnservice_banner']); ?></h3>
              <p><?php echo htmlspecialchars_decode($bolt_Options['header_returnservice_banner_dsc']); ?></p>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>

    <?php
   
     endif;
}
}

if ( ! function_exists ( 'magikBolt_home_offer_banners' ) ) {
function magikBolt_home_offer_banners()
{
    global $bolt_Options;
  if ($bolt_Options['enable_home_offer_banners'] && $bolt_Options['enable_home_offer_banners']!=''){
        ?>
        
  <div class="top-banner-section">
    <div class="container">
      <div class="row">
          

        <?php if (isset($bolt_Options['home-offer-banner1']) && !empty($bolt_Options['home-offer-banner1']['url'])) : ?>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 wow bounceup animated" style="visibility: visible;"> 
              <div class="col">        
                 <a href="<?php echo !empty($bolt_Options['home-offer-banner1-url']) ? esc_url($bolt_Options['home-offer-banner1-url']) : '#' ?>" title="<?php esc_attr_e('link', 'bolt');?>">
            
                     <img alt="<?php esc_attr_e('offer banner1', 'bolt'); ?>" src="<?php echo esc_url($bolt_Options['home-offer-banner1']['url']); ?>">
                 </a>        
              </div>
            </div>

         <?php endif; ?>

         <?php if (isset($bolt_Options['home-offer-banner2']) && !empty($bolt_Options['home-offer-banner2']['url'])) : ?>

            <div class="ccol-lg-4 col-md-4 col-sm-4 col-xs-12 wow bounceup animated" style="visibility: visible;">
               <div class="col">         
                  <a href="<?php echo !empty($bolt_Options['home-offer-banner2-url']) ? esc_url($bolt_Options['home-offer-banner2-url']) : '#' ?>" title="<?php  esc_attr_e('link', 'bolt');?>">
            
                  <img alt="<?php esc_attr_e('offer banner2', 'bolt'); ?>" src="<?php echo esc_url($bolt_Options['home-offer-banner2']['url']); ?>">
                  </a>    
              </div>
            </div>

        <?php endif; ?>
        

        <?php if (isset($bolt_Options['home-offer-banner3']) && !empty($bolt_Options['home-offer-banner3']['url'])) : ?>
            <div class="ccol-lg-4 col-md-4 col-sm-4 col-xs-12 wow bounceup animated" style="visibility: visible;">
              <div class="col">
                   <a href="<?php echo !empty($bolt_Options['home-offer-banner3-url']) ? esc_url($bolt_Options['home-offer-banner3-url']) : '#' ?>" title="<?php esc_attr_e('link', 'bolt');?>">
            
                   <img alt="<?php esc_attr_e('offer banner3', 'bolt'); ?>" src="<?php echo esc_url($bolt_Options['home-offer-banner3']['url']); ?>">
                   </a>
              </div>
            </div>
        <?php endif; ?>


      </div>
    </div>
  </div>

    <?php } 
}
}

if ( ! function_exists ( 'magikBolt_home_offer_slider' ) ) {
function magikBolt_home_offer_slider()
{
    global $bolt_Options;
  if ($bolt_Options['enable_home_offer_slider'] && $bolt_Options['enable_home_offer_slider']!=''){
        ?>

  <div class="offer-slider wow animated parallax parallax-2">
    <div class="container">

       <?php echo htmlspecialchars_decode($bolt_Options['home_offer_slider_text']);?>
               
      <a class="shop-now" href="<?php echo !empty($bolt_Options['enable_home_offer_slider']) ? esc_url($bolt_Options['home_offer_slider_url']) : '#' ?>"><?php esc_attr_e('Shop Now','bolt'); ?></a>
           
     </div>
  </div>

    <?php } 
}
}

if ( ! function_exists ( 'magikBolt_footer_signupform' ) ) {
function magikBolt_footer_signupform()
{
  global $bolt_Options;
if (isset($bolt_Options['enable_mailchimp_form']) && !empty($bolt_Options['enable_mailchimp_form'])) {
 if( function_exists('mc4wp_show_form'))
  {
  ?> 
        
  <div class="newsletter-wrap">
  <?php
    mc4wp_show_form();
   ?>
           
   </div>
  <?php
    } 
    }  

}
}


if ( ! function_exists ( 'magikBolt_footer_middle' ) ) {
function magikBolt_footer_middle()
{
  global $bolt_Options;
 
 if (isset($bolt_Options['enable_footer_middle']) && !empty($bolt_Options['footer_middle']))
  {?>
     
  <?php echo htmlspecialchars_decode($bolt_Options['footer_middle']);?>
            
   <?php  
    }  
}
}


if ( ! function_exists ( 'magikBolt_product_tabs' ) ) {
function magikBolt_product_tabs()
{
    global $bolt_Options;
    ?>

<div class="container">
    <div class="row">
      <div class="products-grid">
        <div class="col-md-12">
          <div class="std">
            <div class="home-tabs">
              <div class="producttabs">
                <div id="magik_producttabs1" class="magik-producttabs"> 
                  
                  <div class="magik-pdt-container"> 
                  
                    <div class="magik-pdt-nav">
                      <ul class="pdt-nav">
                        
                        <?php 
                        if (isset($bolt_Options['enable_home_bestseller_products']) && !empty($bolt_Options['enable_home_bestseller_products'])) { ?>


                        <li class="item-nav <?php if (!$bolt_Options['enable_home_recommended_products'] ) {echo' tab-loaded tab-nav-actived';} ?>" data-type="order" data-catid="" data-orderby="best_sales" data-href="pdt_best_sales"><span class="title-navi"><?php esc_attr_e('Best Seller', 'bolt'); ?></span></li>

                        <?php }

                         if (isset($bolt_Options['enable_home_recommended_products']) && !empty($bolt_Options['enable_home_recommended_products'])) { ?>

                        <li class="item-nav tab-loaded tab-nav-actived" data-type="order" data-catid="" data-orderby="new_arrivals" data-href="pdt_new_arrivals"><span class="title-navi"><?php esc_attr_e('Recommended', 'bolt'); ?></span></li>

                         <?php }
                         if (isset($bolt_Options['enable_home_featured_products']) && !empty($bolt_Options['enable_home_featured_products'])) {   ?>

                        <li class="item-nav <?php if (!$bolt_Options['enable_home_bestseller_products'] && !$bolt_Options['enable_home_recommended_products']) {echo' tab-loaded tab-nav-actived';} ?>" data-type="order" data-catid="" data-orderby="featured" data-href="pdt_featured"><span class="title-navi"><?php esc_attr_e('Featured', 'bolt'); ?></span></li>

                        <?php } ?>

                      </ul>
                    </div>

                    <div class="magik-pdt-content wide-5">

                      <?php if (isset($bolt_Options['enable_home_bestseller_products']) && !empty($bolt_Options['enable_home_bestseller_products'])) { ?>

                         <div class="pdt-content is-loaded pdt_best_sales is-loaded <?php if (!$bolt_Options['enable_home_recommended_products'] ) {echo' tab-content-actived';} ?>">                            
                           <?php magikBolt_bestseller_products(); ?>
                         </div>

                       <?php } ?>
                  
                      <?php if (isset($bolt_Options['enable_home_recommended_products']) && !empty($bolt_Options['enable_home_recommended_products'])) { ?>

                           <div class="pdt-content pdt_new_arrivals is-loaded  tab-content-actived">                        
                              <?php magikBolt_recommended_products(); ?>
                          </div>

                       <?php } ?>
                  
                      <?php if (isset($bolt_Options['enable_home_featured_products']) && !empty($bolt_Options['enable_home_featured_products'])) { ?>
                      
                            <div class="pdt-content pdt_featured is-loaded <?php if (!$bolt_Options['enable_home_bestseller_products'] && !$bolt_Options['enable_home_recommended_products']) {echo' tab-content-actived';} ?>">
                              <?php magikBolt_featured_products(); ?>
                           </div>
                      
                      <?php } ?>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php
}
}

if ( ! function_exists ( 'magikBolt_featured_products' ) ) {
function magikBolt_featured_products()
{
    global $bolt_Options,$boltcount_loop;
     $boltcount_loop=1;
        ?>

<ul class="pdt-list products-grid zoomOut play">
                
                <?php
                $args = array(
                    'post_type' => 'product',
                    'post_status' => 'publish',                  
                    'posts_per_page' => $bolt_Options['featured_per_page'], 
                    'tax_query' => array(
                      array(
                        'taxonomy' => 'product_visibility',
                        'field'    => 'name',
                        'terms'    => 'featured',
                    ),
                ),                 
                );
                $loop = new WP_Query($args);
                if ($loop->have_posts()) {
                    while ($loop->have_posts()) : $loop->the_post();
                        magikBolt_productitem_template();
                        $boltcount_loop++;
                    endwhile;
                } else {
                    esc_attr_e('No products found','bolt');
                }

                wp_reset_postdata();
                ?>
</ul>
    <?php
   
}
}


if ( ! function_exists ( 'magikBolt_bestseller_products' ) ) {
function magikBolt_bestseller_products()
{
   global $bolt_Options,$boltcount_loop;
     $boltcount_loop=1;
  ?>

<ul class="pdt-list products-grid zoomOut play">

       <?php
                
                              $args = array(
                              'post_type'       => 'product',
                              'post_status'       => 'publish',
                              'ignore_sticky_posts'   => 1,
                              'posts_per_page' => $bolt_Options['bestseller_per_page'],      
                              'meta_key'        => 'total_sales',
                              'orderby'         => 'meta_value_num',
                              'order' => 'DESC' 
                              );

                                $loop = new WP_Query( $args );
                             
                                if ( $loop->have_posts() ) {
                                while ( $loop->have_posts() ) : $loop->the_post();                  
                                magikBolt_productitem_template();
                                $boltcount_loop++;
                               
                               
                                endwhile;
                                } else {
                                esc_attr_e( 'No products found', 'bolt' );
                                }

                               wp_reset_postdata();
                             
                             
?>

              
</ul>
<?php 

}
}


if ( ! function_exists ( 'magikBolt_recommended_products' ) ) {
function magikBolt_recommended_products()
{
   global $bolt_Options,$boltcount_loop;
   $boltcount_loop=1;
  ?>
 <ul class="pdt-list products-grid zoomOut play">
                
                <?php
                $args = array(
                    'post_type' => 'product',
                    'post_status' => 'publish',
                    'meta_key' => 'recommended',                 
                    'posts_per_page' => $bolt_Options['recommended_products_per_page'],
                    'orderby'         => 'meta_value_num',
                    'order' => 'DESC' 
                );
                $loop = new WP_Query($args);
                if ($loop->have_posts()) {
                    while ($loop->have_posts()) : $loop->the_post();
                        magikBolt_productitem_template();
                        $boltcount_loop++;
                    endwhile;
                } else {
                    esc_attr_e('No products found','bolt');
                }

                wp_reset_postdata();
                ?>
</ul>
<?php 

}
}


if ( ! function_exists ( 'magikBolt_productitem_template' ) ) {
function magikBolt_productitem_template()
{
  
  $MagikBolt = new MagikBolt();


  global $product, $yith_wcwl,$post;
  global $bolt_loop_class,$boltcount_loop;
   
   $imageUrl = wc_placeholder_img_src();
   if (has_post_thumbnail())
      $imageUrl =  wp_get_attachment_image_src(get_post_thumbnail_id(),'magikBolt-product-size-large');
   if($boltcount_loop%4==1){ $bolt_loop_class='wide-first'; }
   elseif($boltcount_loop%4==0){$bolt_loop_class='last'; } 
   else{$bolt_loop_class=''; } 

   ?>


<li class="item item-animate <?php echo esc_attr($bolt_loop_class);?>">
  <div class="item-inner">
      <div class="item-img">
          <div class="item-img-info">
            <a href="<?php the_permalink(); ?>" title="<?php echo esc_html($post->post_title); ?>" class="product-image">
                        <figure class="img-responsive">
                        <img alt="<?php echo esc_html($post->post_title); ?>" src="<?php echo esc_url($imageUrl[0]); ?>">
                         </figure>
                       </a>

                       <?php if ($product->is_on_sale()) : ?>
                         <div class="new-label new-top-left">
                          <?php esc_attr_e('Sale', 'bolt'); ?>
                         </div>
                       <?php endif; ?>

              <div class="actions">

                      <?php if (class_exists('YITH_WCQV_Frontend')) { ?>
                            <div class="quick-view-btn">
                                   <a class="yith-wcqv-button" href="#"
                                    data-toggle="tooltip" data-placement="right" title="<?php esc_attr_e('Quick View', 'bolt'); ?>"
                                    data-product_id="<?php echo esc_html($product->get_id()); ?>"><span><?php esc_attr_e('Quick View', 'bolt'); ?></span></a>
                            </div>
                     <?php } ?>
                  


                      <?php if (isset($yith_wcwl) && is_object($yith_wcwl)) {
                            $classes = get_option('yith_wcwl_use_button') == 'yes' ? 'class="add_to_wishlist link-wishlist"' : 'class="add_to_wishlist link-wishlist"'; ?>

                          <div class="add_to_wishlist link-wishlist">
                            <a href="<?php echo esc_url(add_query_arg('add_to_wishlist', $product->get_id())) ?>" data-toggle="tooltip" data-placement="right" title="<?php esc_attr_e('Wishlist', 'bolt'); ?>" data-product-id="<?php echo esc_html($product->get_id()); ?>" data-product-type="<?php echo esc_html($product->get_type()); ?>" <?php echo htmlspecialchars_decode($classes); ?>
                                ><span><?php esc_attr_e('Add to Wishlist', 'bolt'); ?></span></a>
                          </div>

                      <?php } ?>


                      <?php if (class_exists('YITH_Woocompare_Frontend')) {
                             $mgk_yith_cmp = new YITH_Woocompare_Frontend; ?> 

                            <div class="link-compare">
                              <a href="<?php echo esc_url($mgk_yith_cmp->add_product_url($product->get_id())); ?>" data-toggle="tooltip" data-placement="right" title="<?php esc_attr_e('Compare', 'bolt'); ?>" class="compare  add_to_compare" data-product_id="<?php echo esc_html($product->get_id()); ?>"><span><?php esc_attr_e('Add to Compare', 'bolt'); 
                              ?></span></a>
                             </div>

                      <?php  } ?> 
                 
                    <?php magikBolt_woocommerce_product_add_to_cart_text() ;?>
                   
              </div>


                <div class="rating">
                     <div class="ratings">
                        <div class="rating-box">
                          <?php $average = $product->get_average_rating(); ?>
                            <div style="width:<?php echo esc_html(($average / 5) * 100); ?>%" class="rating"> 
                            </div>
                        </div>
                     </div>
                  </div>
                </div>
             </div>
            <div class="item-info">
              <div class="info-inner">
                  <div class="item-title">
                    <a href="<?php the_permalink(); ?>" title="<?php echo esc_html($post->post_title); ?>"> <?php echo esc_html($post->post_title); ?> </a> 
                  </div>
                  <div class="item-content">
                    <div class="item-price">
                      <div class="price-box">
                        <span class="regular-price"> 
                            <?php echo htmlspecialchars_decode($product->get_price_html()); ?>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
          </div>
        </li>





<?php

}
}


if ( ! function_exists ( 'magikBolt_new_products' ) ) {
function magikBolt_new_products()
{
   global $bolt_Options;

 if (isset($bolt_Options['enable_home_new_products']) && !empty($bolt_Options['enable_home_new_products'])) { ?>


<div class="featured-pro container">
    <div class="slider-items-products">
         <div class="new_title">
            <h2><span><?php esc_attr_e('Fresh Arrivals', 'bolt'); ?></span></h2>
         </div>
      <div id="featured-slider-fresh" class="product-flexslider hidden-buttons">
        <div class="slider-items slider-width-col4 products-grid">

                
                <?php
                $args = array(
                              'post_type'       => 'product',
                              'post_status'       => 'publish',
                              'ignore_sticky_posts'   => 1,
                              'posts_per_page' => $bolt_Options['new_products_per_page'],      
                              'orderby' => 'date',
                              'order' => 'DESC',
                              
                              );
                $loop = new WP_Query($args);
                if ($loop->have_posts()) {
                    while ($loop->have_posts()) : $loop->the_post();
                        magikBolt_new_products_template();
                    endwhile;
                } else {
                    esc_attr_e('No products found','bolt');
                }

                wp_reset_postdata();
                ?>

          
      </div>
    </div>
  </div>
 </div>


<?php 
}
}
}


if ( ! function_exists ( 'magikBolt_new_products_template' ) ) {
function magikBolt_new_products_template()
{
  $MagikBolt = new MagikBolt();
  global $product, $yith_wcwl,$post;
   $imageUrl = wc_placeholder_img_src();
   if (has_post_thumbnail())
      $imageUrl =  wp_get_attachment_image_src(get_post_thumbnail_id(),'magikBolt-product-size-large');
   
   ?>
       

    <div class="item">
            <div class="item-inner">
              <div class="item-img">
                <div class="item-img-info"> 

                      <a href="<?php the_permalink(); ?>" title="<?php echo esc_html($post->post_title); ?>" class="product-image">
                        <figure class="img-responsive">
                        <img alt="<?php echo esc_html($post->post_title); ?>" src="<?php echo esc_url($imageUrl[0]); ?>">
                         </figure>
                       </a>

                       <?php if ($product->is_on_sale()) : ?>
                         <div class="new-label new-top-left">
                          <?php esc_attr_e('Sale', 'bolt'); ?>
                         </div>
                       <?php endif; ?>

                  <div class="actions">

                     <?php if (class_exists('YITH_WCQV_Frontend')) { ?>
                            <div class="quick-view-btn">
                                   <a class="yith-wcqv-button" href="#"
                                    data-toggle="tooltip" data-placement="right" title="<?php esc_attr_e('Quick View', 'bolt'); ?>"
                                    data-product_id="<?php echo esc_html($product->get_id()); ?>"><span><?php esc_attr_e('Quick View', 'bolt'); ?></span></a>
                            </div>
                     <?php } ?>
                  


                      <?php if (isset($yith_wcwl) && is_object($yith_wcwl)) {
                            $classes = get_option('yith_wcwl_use_button') == 'yes' ? 'class="add_to_wishlist link-wishlist"' : 'class="add_to_wishlist link-wishlist"'; ?>

                          <div class="add_to_wishlist link-wishlist">
                            <a href="<?php echo esc_url(add_query_arg('add_to_wishlist', $product->get_id())) ?>" data-toggle="tooltip" data-placement="right" title="<?php esc_attr_e('Wishlist', 'bolt'); ?>" data-product-id="<?php echo esc_html($product->get_id()); ?>" data-product-type="<?php echo esc_html($product->get_type()); ?>" <?php echo htmlspecialchars_decode($classes); ?>
                                ><span><?php esc_attr_e('Add to Wishlist', 'bolt'); ?></span></a>
                          </div>

                      <?php } ?>


                      <?php if (class_exists('YITH_Woocompare_Frontend')) {
                             $mgk_yith_cmp = new YITH_Woocompare_Frontend; ?> 

                            <div class="link-compare">
                              <a href="<?php echo esc_url($mgk_yith_cmp->add_product_url($product->get_id())); ?>" data-toggle="tooltip" data-placement="right" title="<?php esc_attr_e('Compare', 'bolt'); ?>" class="compare  add_to_compare" data-product_id="<?php echo esc_html($product->get_id()); ?>"><span><?php esc_attr_e('Add to Compare', 'bolt'); 
                              ?></span></a>
                             </div>

                      <?php  } ?> 
                   
                        <?php magikBolt_woocommerce_product_add_to_cart_text() ;?>
                  
                  </div>

                  <div class="rating">
                     <div class="ratings">
                        <div class="rating-box">
                          <?php $average = $product->get_average_rating(); ?>
                            <div style="width:<?php echo esc_html(($average / 5) * 100); ?>%" class="rating"> 
                            </div>
                        </div>
                     </div>
                  </div>
                </div>
              </div>
              <div class="item-info">
                <div class="info-inner">
                  <div class="item-title"> 
                       <a href="<?php the_permalink(); ?>" title="<?php echo esc_html($post->post_title); ?>"> <?php echo esc_html($post->post_title); ?> </a>
                 </div>
                  <div class="item-content">
                    <div class="item-price">
                      <div class="price-box">
                        <span class="regular-price"> 
                            <?php echo htmlspecialchars_decode($product->get_price_html()); ?>
                        </span>
                    
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>



<?php

}
}


if ( ! function_exists ( 'magikBolt_related_upsell_template' ) ) {
function magikBolt_related_upsell_template()
{
  $MagikBolt = new MagikBolt();
 global $product, $yith_wcwl,$post;


$imageUrl = wc_placeholder_img_src();
if (has_post_thumbnail())
    $imageUrl =  wp_get_attachment_image_src(get_post_thumbnail_id(),'magikBolt-product-size-large');  
?>
 <div class="item">
            <div class="item-inner">
              <div class="item-img">
                <div class="item-img-info"> 

                      <a href="<?php the_permalink(); ?>" title="<?php echo esc_html($post->post_title); ?>" class="product-image">
                        <figure class="img-responsive">
                        <img alt="<?php echo esc_html($post->post_title); ?>" src="<?php echo esc_url($imageUrl[0]); ?>">
                         </figure>
                       </a>

                       <?php if ($product->is_on_sale()) : ?>
                         <div class="new-label new-top-left">
                          <?php esc_attr_e('Sale', 'bolt'); ?>
                         </div>
                       <?php endif; ?>

                  <div class="actions">

                     <?php if (class_exists('YITH_WCQV_Frontend')) { ?>
                            <div class="quick-view-btn">
                                   <a class="yith-wcqv-button" href="#"
                                    data-toggle="tooltip" data-placement="right" title="<?php esc_attr_e('Quick View', 'bolt'); ?>"
                                    data-product_id="<?php echo esc_html($product->get_id()); ?>"><span><?php esc_attr_e('Quick View', 'bolt'); ?></span></a>
                            </div>
                     <?php } ?>
                  


                      <?php if (isset($yith_wcwl) && is_object($yith_wcwl)) {
                            $classes = get_option('yith_wcwl_use_button') == 'yes' ? 'class="add_to_wishlist link-wishlist"' : 'class="add_to_wishlist link-wishlist"'; ?>

                          <div class="add_to_wishlist link-wishlist">
                            <a href="<?php echo esc_url(add_query_arg('add_to_wishlist', $product->get_id())) ?>" data-toggle="tooltip" data-placement="right" title="<?php esc_attr_e('Wishlist', 'bolt'); ?>" data-product-id="<?php echo esc_html($product->get_id()); ?>" data-product-type="<?php echo esc_html($product->get_type()); ?>" <?php echo htmlspecialchars_decode($classes); ?>
                                ><span><?php esc_attr_e('Add to Wishlist', 'bolt'); ?></span></a>
                          </div>

                      <?php } ?>


                      <?php if (class_exists('YITH_Woocompare_Frontend')) {
                             $mgk_yith_cmp = new YITH_Woocompare_Frontend; ?> 

                            <div class="link-compare">
                              <a href="<?php echo esc_url($mgk_yith_cmp->add_product_url($product->get_id())); ?>" data-toggle="tooltip" data-placement="right" title="<?php esc_attr_e('Compare', 'bolt'); ?>" class="compare  add_to_compare" data-product_id="<?php echo esc_html($product->get_id()); ?>"><span><?php esc_attr_e('Add to Compare', 'bolt'); 
                              ?></span></a>
                             </div>

                      <?php  } ?> 
                   
                      <?php magikBolt_woocommerce_product_add_to_cart_text() ;?>
                  
                  </div>

                  <div class="rating">
                     <div class="ratings">
                        <div class="rating-box">
                          <?php $average = $product->get_average_rating(); ?>
                            <div style="width:<?php echo esc_html(($average / 5) * 100); ?>%" class="rating"> 
                            </div>
                        </div>
                     </div>
                  </div>
                </div>
              </div>
              <div class="item-info">
                <div class="info-inner">
                  <div class="item-title"> 
                       <a href="<?php the_permalink(); ?>" title="<?php echo esc_html($post->post_title); ?>"> <?php echo esc_html($post->post_title); ?> </a>
                 </div>
                  <div class="item-content">
                    <div class="item-price">
                      <div class="price-box">
                        <span class="regular-price"> 
                            <?php echo htmlspecialchars_decode($product->get_price_html()); ?>
                        </span>
                    
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
<?php
}
}



if ( ! function_exists ( 'magikBolt_footer_brand_logo' ) ) {
function magikBolt_footer_brand_logo()
  {
    global $bolt_Options;
    if (isset($bolt_Options['enable_brand_logo']) && $bolt_Options['enable_brand_logo'] && !empty($bolt_Options['all-company-logos'])) : ?>
    

  <div class="brand-logo">
    <div class="container">
      <div class="slider-items-products">
        <div id="brand-logo-slider" class="product-flexslider hidden-buttons">
          <div class="slider-items slider-width-col6"> 

             <?php foreach ($bolt_Options['all-company-logos'] as $_logo) : ?>
                  <div class="item">
                    <a href="<?php echo esc_url($_logo['url']); ?>" target="_blank"> <img
                        src="<?php echo esc_url($_logo['image']); ?>" 
                        alt="<?php echo esc_attr($_logo['title']); ?>"> </a>
                  </div>
                  <?php endforeach; ?>

                      
          </div>
        </div>
      </div>
    </div>
  </div>

    
  <?php endif;
  }
}


if ( ! function_exists ( 'magikBolt_home_blog_posts' ) ) {
function magikBolt_home_blog_posts()
{
    $count = 0;
    global $bolt_Options;
    $MagikBolt = new MagikBolt();
    if (isset($bolt_Options['enable_home_blog_posts']) && !empty($bolt_Options['enable_home_blog_posts'])) {
        ?>


<div class="latest-blog">
  <div class="container">
    <div class="slider-items-products">
      <div class="new_title">
        <h2><span><?php esc_attr_e('Latest Blog', 'bolt'); ?></span></h2>
      </div>
      <div id="latest-blog-slider" class="product-flexslider hidden-buttons">
        <div class="slider-items slider-width-col4 products-grid"> 
            
      <?php $args = array('posts_per_page' => 4, 'post__not_in' => get_option('sticky_posts'));
        $the_query = new WP_Query($args);
           $i=1;  
        if ($the_query->have_posts()) :
            while ($the_query->have_posts()) : $the_query->the_post(); ?>
            <div class="item">
              <div class="item-inner">
                <div class="blog_inner">
                  <div class="blog-img"> 
                    <a href="<?php the_permalink(); ?>"> <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'single-post-thumbnail'); ?>
                                <img src="<?php echo esc_url($image[0]); ?>" alt="<?php the_title(); ?>">
                      </a>
                    <div class="mask"> 
                      <a class="info" href="<?php the_permalink(); ?>"><?php esc_attr_e('Read More','bolt'); ?></a>

                    </div>
                  </div>

                  <div class="blog-preview_info">
                     
                     <div class="blog-date">
                      <div class="date_divs"><?php echo esc_html(get_the_date('d')); ?></div>
                      <div class="month_div"><?php echo esc_html(get_the_date('M')); ?></div>
                      <div class="post_type"><i class="fa fa-calendar"></i></div>
                    </div>

                    <div class="blog-info-inner">

                      <h4 class="blog-preview_title">
                        <a class="blog-title" href="<?php the_permalink(); ?>"><?php esc_html(the_title()); ?>
                        </a>
                      </h4>

                      <ul class="post-meta">
                        <li><i class="fa fa-user"></i><?php esc_attr_e('posted by ', 'bolt'); ?><a href="<?php comments_link(); ?>"><?php the_author(); ?></a> </li>
                        <li><i class="fa fa-comments"></i><a href="<?php comments_link(); ?>"><?php comments_number('0 Comment', '1 Comment', '% Comments'); ?></a></li>
                      </ul>
                      <div class="blog-preview_desc"><?php the_excerpt(); ?></div>
                         <a class="blog-preview_btn" href="<?php the_permalink(); ?>"><?php esc_attr_e('READ MORE','bolt'); ?></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            

              
            <?php    $i++;
             endwhile; ?>
            <?php wp_reset_postdata(); ?>
        <?php else: ?>
            <p>
                <?php esc_attr_e('Sorry, no posts matched your criteria.', 'bolt'); ?>
            </p>
        <?php endif;
        ?>


            
          </div>
        </div>
      </div>
    </div>
</div>


<?php
    }
}
}


// new ccustom code section added

if(!function_exists('magikBolt_home_customsection'))
{
 function magikBolt_home_customsection()
 {
   global $bolt_Options;
   ?>

   <div class="container">
    <div class="row">
     <div class="mgk_custom_block">
       <div class="col-md-12">
       <?php the_content(); ?>
     </div>
     </div>
   </div>
 </div>
 <?php
}

}