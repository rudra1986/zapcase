<?php 

$MagikBolt = new MagikBolt();?>
<?php magikBolt_footer_brand_logo();?>


 <footer>
    <div class="footer-inner">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-xs-12 col-lg-8">

                <?php if (is_active_sidebar('footer-sidebar-1')) : ?>
                    <div class="footer-column pull-left">
                      <?php dynamic_sidebar('footer-sidebar-1'); ?>
                    </div>
                <?php endif; ?>

            
           
               <?php if (is_active_sidebar('footer-sidebar-2')) : ?>
                    <div class="footer-column pull-left">
                      <?php dynamic_sidebar('footer-sidebar-2'); ?>
                     </div>
                <?php endif; ?>

           
              <?php if (is_active_sidebar('footer-sidebar-3')) : ?>
                   <div class="footer-column pull-left">
                     <?php dynamic_sidebar('footer-sidebar-3'); ?>
                    </div>
              <?php endif; ?>
              
          </div>
          <div class="col-xs-12 col-lg-4">
            <div class="footer-column-last">
              <?php magikBolt_footer_signupform();?>

              <?php magikBolt_social_media_links(); ?>
               
               <?php if (is_active_sidebar('footer-sidebar-4')) : ?>
                  <?php dynamic_sidebar('footer-sidebar-4'); ?>
               <?php endif; ?>

            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-middle">
      <?php magikBolt_footer_middle();?>
    </div>
    <div class="footer-bottom">
      <div class="container">
        <div class="row">
          <?php magikBolt_footer_text() ; ?> 
        </div>
      </div>
    </div>
  </footer>

    </div>
    <?php magikBolt_backtotop();?>
   
    
    
<div class="menu-overlay"></div>
<?php // navigation panel
require_once(MAGIKBOLT_THEME_PATH .'/menu_panel.php');
 ?>
   
    <?php wp_footer(); ?>
    </body></html>
