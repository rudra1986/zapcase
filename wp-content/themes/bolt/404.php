<?php

get_header(); ?>
 <?php if (isset($bolt_Options['theme_layout']) && $bolt_Options['theme_layout']=='version3')
      {?>
<div class="page-heading">
  <div class="page-title">
       <h2>
       <?php $MagikBolt->magikBolt_page_title(); ?>
       </h2>
       </div>
       <div class="breadcrumbs">
            <div class="container">
              <div class="row">
                <div class="col-xs-12">
                   <?php $MagikBolt->magikBolt_breadcrumbs(); ?>
              </div>
            </div>
           </div>
       </div>    
</div>
    <?php }
    else { ?>
 <div class="breadcrumbs">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
          <?php $MagikBolt->magikBolt_breadcrumbs(); ?>
          </div>
          <!--col-xs-12--> 
        </div>
        <!--row--> 
      </div>
      <!--container--> 
    </div>
<?php } ?>

<div class="content-wrapper">
    <div class="container">
      
      <?php if(isset($bolt_Options['theme_layout']) && $bolt_Options['theme_layout']=='version3')
      {
        ?>
        <?php } 
       else{
       ?>
        <div class="page-title">
      <h2 class="entry-title">
        <?php $MagikBolt->magikBolt_page_title(); ?>
      </h2>
        </div>
        <?php

       }
        ?>
        <div class="std">
            <div class="page-not-found wow bounceInRight animated">
                <h2><?php  esc_attr_e('404','bolt') ;?></h2>

                <h3><img src="<?php echo esc_url(get_template_directory_uri()) . '/images/signal.png'; ?>"
                         alt="<?php  esc_attr_e('404! Page Not Found','bolt') ;?>">
                         <?php  esc_attr_e('Oops! The Page you requested was not found!','bolt') ;?></h3>

                <div><a href="<?php echo esc_url(get_home_url()); ?>" type="button"
                        class="btn-home"><span><?php  esc_attr_e('Back To Home','bolt') ;?></span></a></div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>

