<?php
/*Define Constants */
define('MAGIKBOLT_BOLT_VERSION', '1.0');  
define('MAGIKBOLT_THEME_PATH', get_template_directory());
define('MAGIKBOLT_THEME_URI', get_template_directory_uri());
define('MAGIKBOLT_THEME_NAME', 'bolt');

define('MAGIKBOLT_CUS_PLUGIN_PATH', MAGIKBOLT_THEME_PATH.'/inc/plugins/');
define('MAGIKBOLT_CUS_PLUGIN_URI', MAGIKBOLT_THEME_URI.'/inc/plugins/');
/* Include required tgm activation */
require_once ( get_template_directory(). '/includes/tgm_activation/install-required.php');
require_once ( get_template_directory(). '/includes/reduxActivate.php');
if (file_exists( get_template_directory(). '/includes/reduxConfig.php')) {
    require_once ( get_template_directory(). '/includes/reduxConfig.php');
}

/* Include theme variation functions */   
require_once(MAGIKBOLT_THEME_PATH . '/core/mgk_framework.php');


if (!isset($content_width)) {
    $content_width = 800;
}



class MagikBolt {
   
  /**
  * Constructor
  */
  function __construct() {
    // Register action/filter callbacks
  
    add_action('after_setup_theme', array($this, 'magikBolt_setup'));
    add_action( 'init', array($this, 'magikBolt_theme'));
    add_action('wp_enqueue_scripts', array($this,'magikBolt_custom_enqueue_google_font'));
    
    add_action('admin_enqueue_scripts', array($this,'magikBolt_admin_scripts_styles'));
    add_action('wp_enqueue_scripts', array($this,'magikBolt_scripts_styles'));
  
    add_action('widgets_init', array($this,'magikBolt_widgets_init'));
    add_action('wp_enqueue_scripts', array($this,'magikBolt_enqueue_custom_css'));
    
    add_action('add_meta_boxes', array($this,'magikBolt_reg_page_meta_box'));
    add_action('save_post',array($this, 'magikBolt_save_page_layout_meta_box_values')); 
    add_action('add_meta_boxes', array($this,'magikBolt_reg_post_meta_box'));
    add_action('save_post',array($this, 'magikBolt_save_post_layout_meta_box_values')); 
 
    }

    function magikBolt_theme() {

    global $bolt_Options;

    }

  /** * Theme setup */
  function magikBolt_setup() {   
    global $bolt_Options;
     load_theme_textdomain('bolt', get_template_directory() . '/languages');
     load_theme_textdomain('woocommerce', get_template_directory() . '/languages');

      // Add default posts and comments RSS feed links to head.
      add_theme_support('automatic-feed-links');
      add_theme_support('title-tag');
      add_theme_support('post-thumbnails');
      add_image_size('magikBolt-featured_preview', 55, 55, true);
      add_image_size('magikBolt-article-home-large',1140, 450, true);
      add_image_size('magikBolt-product-size-large',290, 350, true);      
          
         
    add_theme_support( 'html5', array(
      'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
    ) );
    
    add_theme_support( 'post-formats', array(
      'aside','video','audio'
    ) );
    
    // Setup the WordPress core custom background feature.
    $default_color = trim( 'ffffff', '#' );
    $default_text_color = trim( '333333', '#' );
    
    add_theme_support( 'custom-background', apply_filters( 'magikBolt_custom_background_args', array(
      'default-color'      => $default_color,
      'default-attachment' => 'fixed',
    ) ) );
    
    add_theme_support( 'custom-header', apply_filters( 'magikBolt_custom_header_args', array(
      'default-text-color'     => $default_text_color,
      'width'                  => 1170,
      'height'                 => 450,
      
    ) ) );

    /*
     * This theme styles the visual editor to resemble the theme style,
     * specifically font, colors, icons, and column width.
     */
    add_editor_style('css/editor-style.css' );
    
    /*
    * Edge WooCommerce Declaration: WooCommerce Support and settings
    */    
    
      if (class_exists('WooCommerce')) {
        add_theme_support('woocommerce');
        require_once(MAGIKBOLT_THEME_PATH. '/woo_function.php');
        // Disable WooCommerce Default CSS if set
        if (isset($bolt_Options['woocommerce_disable_woo_css']) && !empty($bolt_Options['woocommerce_disable_woo_css'])) {
          add_filter('woocommerce_enqueue_styles', '__return_false');
          wp_enqueue_style('woocommerce_enqueue_styles', get_template_directory_uri() . '/woocommerce.css');
        }
      }
 
    // Register navigation menus
    
    register_nav_menus(
      array(
      'toplinks' => esc_html__( 'Top menu', 'bolt' ),
       'main_menu' => esc_html__( 'Main menu', 'bolt' )
      ));
    
  }
    

function magikBolt_fonts_url() {
  global $bolt_Options;
  $fonts_url = '';
  $fonts     = array();
  $subsets   = 'latin,latin-ext';


if (isset($bolt_Options['theme_layout']) && $bolt_Options['theme_layout']=='version2')
{
    
    if ( 'off' !== _x( 'on', 'Open Sans: on or off', 'bolt' ) ) {
        $fonts[]='Open Sans:700,600,800,400';
    }
   
}

elseif (isset($bolt_Options['theme_layout']) && $bolt_Options['theme_layout']=='version3')
{

   if ( 'off' !== _x( 'on', 'Open Sans: on or off', 'bolt' ) ) {
         $fonts[]='Open Sans:700,600,800,400';
        }

    if ( 'off' !== _x( 'on', 'Montserrat: on or off', 'bolt' ) ) {
         $fonts[]='Montserrat:400,700';
        }
}
elseif (isset($bolt_Options['theme_layout']) && $bolt_Options['theme_layout']=='version4')
{

   if ( 'off' !== _x( 'on', 'Montserrat: on or off', 'bolt' ) ) {
         $fonts[]='Montserrat:400,700';
        }

    if ( 'off' !== _x( 'on', 'Roboto: on or off', 'bolt' ) ) {
         $fonts[]='Roboto:400,500,300,700,900';
        }
}
else
{
    
    if ( 'off' !== _x( 'on', 'Open Sans: on or off', 'bolt' ) ) {
        $fonts[]='Open Sans:700,600,800,400';
    }
    if ( 'off' !== _x( 'on', 'Raleway: on or off', 'bolt' ) ) {
        $fonts[]=' Raleway:400,300,600,500,700,800';
       
    }
    
    
}


    if ( $fonts ) {
    $fonts_url = add_query_arg( array(
      'family' => urlencode( implode( '|', $fonts ) ),
      'subset' => urlencode( $subsets ),
    ), 'https://fonts.googleapis.com/css' );
  }
    return $fonts_url;
}
/*
Enqueue scripts and styles.
*/
function magikBolt_custom_enqueue_google_font() {

  wp_enqueue_style( 'magikBolt-Fonts', $this->magikBolt_fonts_url() , array(), '1.0.0' );
}


  function magikBolt_admin_scripts_styles()
  {  
     global $post;
    wp_enqueue_media();
   
      wp_enqueue_script('magikBolt-adminjs', MAGIKBOLT_THEME_URI . '/js/admin_menu.js', array(), '', true);
      wp_enqueue_style('magikBolt-adminmenu', MAGIKBOLT_THEME_URI . '/css/admin_menu.css', array(), '');
  }

function magikBolt_scripts_styles()
{
    global $bolt_Options,$yith_wcwl;
    /*JavaScript for threaded Comments when needed*/
    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }


     if (isset($bolt_Options['theme_layout'])  && !empty($bolt_Options['theme_layout']))
     {
      wp_enqueue_style('bootstrap', MAGIKBOLT_THEME_URI . '/skins/' . $bolt_Options['theme_layout'] . '/bootstrap.min.css', array(), '');   
 
      }else
     {
     wp_enqueue_style('bootstrap', MAGIKBOLT_THEME_URI . '/skins/default/bootstrap.min.css', array(), '');   
    }

 
  wp_enqueue_style('font-awesome', MAGIKBOLT_THEME_URI . '/css/font-awesome.css', array(), '');
  wp_enqueue_style('simple-line-icons', MAGIKBOLT_THEME_URI . '/css/simple-line-icons.css', array(), '');
   
     

  wp_enqueue_style('owl.carousel', MAGIKBOLT_THEME_URI . '/css/owl.carousel.css', array(), '');

  wp_enqueue_style('owl.theme', MAGIKBOLT_THEME_URI . '/css/owl.theme.css', array(), '');
  
  wp_enqueue_style('flexslider', MAGIKBOLT_THEME_URI . '/css/flexslider.css', array(), '');

   wp_enqueue_style('jquery.bxslider', MAGIKBOLT_THEME_URI . '/css/jquery.bxslider.css', array(), '');
  
   wp_enqueue_style('magikBolt-search', MAGIKBOLT_THEME_URI . '/css/magikautosearch.css', array(), '');
     

  wp_enqueue_style('magikBolt-style', MAGIKBOLT_THEME_URI . '/style.css', array(), '');  
    
    if (isset($bolt_Options['theme_layout']) && !empty($bolt_Options['theme_layout']))
     {
    wp_enqueue_style( 'magikBolt-blog', MAGIKBOLT_THEME_URI . '/skins/' . $bolt_Options['theme_layout'] . '/blogs.css', array(), '');
   wp_enqueue_style( 'magikBolt-revslider', MAGIKBOLT_THEME_URI . '/skins/' . $bolt_Options['theme_layout'] . '/revslider.css', array(), '');
   wp_enqueue_style('magikBolt-layout', MAGIKBOLT_THEME_URI . '/skins/' . $bolt_Options['theme_layout'] . '/style.css', array(), '');
   wp_enqueue_style( 'magikBolt-mgk_menu', MAGIKBOLT_THEME_URI . '/skins/' . $bolt_Options['theme_layout'] . '/mgk_menu.css', array(), '');  
    wp_enqueue_style('magikBolt-jquery.mobile-menu', MAGIKBOLT_THEME_URI . '/skins/' . $bolt_Options['theme_layout'] . '/jquery.mobile-menu.css', array(), '');
     }
     else
     {
   wp_enqueue_style( 'magikBolt-blog', MAGIKBOLT_THEME_URI . '/skins/default/blogs.css', array(), '');
   wp_enqueue_style( 'magikBolt-revslider', MAGIKBOLT_THEME_URI . '/skins/default/revslider.css', array(), '');
   wp_enqueue_style('magikBolt-layout', MAGIKBOLT_THEME_URI . '/skins/default/style.css', array(), '');
   wp_enqueue_style( 'magikBolt-mgk_menu', MAGIKBOLT_THEME_URI . '/skins/default/mgk_menu.css', array(), '');  
    wp_enqueue_style('magikBolt-jquery.mobile-menu', MAGIKBOLT_THEME_URI . '/skins/default/jquery.mobile-menu.css', array(), '');
     }
    
 //theme js

     wp_enqueue_script('bootstrap', MAGIKBOLT_THEME_URI . '/js/bootstrap.min.js', array('jquery'), '', true);      
     
     wp_enqueue_script('magikBolt-countdown',MAGIKBOLT_THEME_URI . '/js/countdown.js', array('jquery'), '', true);
    wp_enqueue_script('parallax',MAGIKBOLT_THEME_URI . '/js/parallax.js', array('jquery'), '', true);
   wp_enqueue_script('jquery.cookie.min', MAGIKBOLT_THEME_URI . '/js/jquery.cookie.min.js', array('jquery'), '', true);
   wp_enqueue_script('magikBolt-common-js',MAGIKBOLT_THEME_URI . '/js/common.js', array('jquery'), '', true);

  if (isset($yith_wcwl) && is_object($yith_wcwl)) { 
    wp_localize_script( 'magikBolt-common-js', 'js_bolt_wishvar', array(
            
            'MGK_ADD_TO_WISHLIST_SUCCESS_TEXT' => esc_html__('Product successfully added to wishlist','bolt').' <a href="'.esc_url($yith_wcwl->get_wishlist_url()).'">'.esc_html__('Browse Wishlist.','bolt').'</a>' ,
            'MGK_ADD_TO_WISHLIST_EXISTS_TEXT' => esc_html__('The product is already in the wishlist!','bolt').' <a href="'.esc_url($yith_wcwl->get_wishlist_url()).'">'.esc_html__('Browse Wishlist.','bolt').'</a>' ,
              'IMAGEURL' => esc_url(MAGIKBOLT_THEME_URI).'/images',
                     
        ) );
     }
    
    if (isset($bolt_Options['theme_layout']) && $bolt_Options['theme_layout']=='version3')
     {
      wp_enqueue_script('revolution-slider', MAGIKBOLT_THEME_URI . '/js/revolution-slider.js', array('jquery'), '', true);
      wp_enqueue_script('revolution.extension', MAGIKBOLT_THEME_URI . '/js/revolution.extension.js', array('jquery'), '', true);
     }
     else
     {
      wp_enqueue_script('revslider.js', MAGIKBOLT_THEME_URI . '/js/revslider.js', array('jquery'), '', true);
     }

    
    wp_enqueue_script('jquery.bxslider', MAGIKBOLT_THEME_URI . '/js/jquery.bxslider.min.js', array('jquery'), '', true);
    wp_enqueue_script('jquery.flexslider', MAGIKBOLT_THEME_URI . '/js/jquery.flexslider.js', array('jquery'), '', true);
    wp_enqueue_script('jquery.mobile-menu', MAGIKBOLT_THEME_URI . '/js/jquery.mobile-menu.min.js', array('jquery'), '', true);

    wp_enqueue_script('owl.carousel',MAGIKBOLT_THEME_URI . '/js/owl.carousel.min.js', array('jquery'), '', true);  

    if (isset($bolt_Options['theme_layout']) && $bolt_Options['theme_layout']=='version3') { 
   wp_enqueue_script('magikBolt-cloud-zoom3-js', MAGIKBOLT_THEME_URI . '/js/cloud-zoom3.js', array('jquery'), '', true);

      }
      else
      {
 
    wp_enqueue_script('magikBolt-cloud-zoom-js', MAGIKBOLT_THEME_URI . '/js/cloud-zoom.js', array('jquery'), '', true);
      }
     
     if (isset($bolt_Options['theme_layout']) && $bolt_Options['theme_layout']=='version4')
     {
     
        wp_enqueue_script('magikBolt-themejs', MAGIKBOLT_THEME_URI .'/js/mgk_menuv4.js', array('jquery'), '', true );
       

            wp_localize_script( 'magikBolt-themejs', 'js_bolt_vars', array(
            'ajax_url' => esc_url(admin_url( 'admin-ajax.php' )),
            'container_width' => 790,
            'grid_layout_width' => 20            
        ) );
     }
     else
     {

      wp_enqueue_script('magikBolt-themejs', MAGIKBOLT_THEME_URI .'/js/mgk_menu.js', array('jquery'), '', true );
       
            wp_localize_script( 'magikBolt-themejs', 'js_bolt_vars', array(
            'ajax_url' => esc_url(admin_url( 'admin-ajax.php' )),
            'container_width' => 1250,
            'grid_layout_width' => 20           
        ) );
       }
}


  //register sidebar widget
  function magikBolt_widgets_init()
  {
      register_sidebar(array(
      'name' => esc_html__('Blog Sidebar', 'bolt'),
      'id' => 'sidebar-blog',
      'description' => esc_html__('Sidebar that appears on the right of Blog and Search page.', 'bolt'),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h3 class="block-title">',
      'after_title' => '</h3>',
    ));
    register_sidebar(array(
      'name' => esc_html__('Shop Sidebar','bolt'),
      'id' => 'sidebar-shop',
      'description' => esc_html__('Main sidebar that appears on the left.', 'bolt'),
      'before_widget' => '<div id="%1$s" class="block %2$s">',
      'after_widget' => '</div>',
      'before_title' => '<div class="block-title">',
      'after_title' => '</div>',
    ));
    register_sidebar(array(
      'name' => esc_html__('Content Sidebar Left', 'bolt'),
      'id' => 'sidebar-content-left',
      'description' => esc_html__('Additional sidebar that appears on the left.','bolt'),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<div class="block-title">',
      'after_title' => '</div>',
    ));
    register_sidebar(array(
      'name' => esc_html__('Content Sidebar Right', 'bolt'),
      'id' => 'sidebar-content-right',
      'description' => esc_html__('Additional sidebar that appears on the right.', 'bolt'),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<div class="block-title">',
      'after_title' => '</div>',
    ));
   
    register_sidebar(array(
      'name' => esc_html__('Footer Widget Area 1','bolt'),
      'id' => 'footer-sidebar-1',
      'description' => esc_html__('Appears in the footer section of the site.','bolt'),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h4>',
      'after_title' => '</h4>',
    ));
    register_sidebar(array(
      'name' => esc_html__('Footer Widget Area 2', 'bolt'),
      'id' => 'footer-sidebar-2',
      'description' => esc_html__('Appears in the footer section of the site.', 'bolt'),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h4>',
      'after_title' => '</h4>',
    ));
    register_sidebar(array(
      'name' => esc_html__('Footer Widget Area 3', 'bolt'),
      'id' => 'footer-sidebar-3',
      'description' => esc_html__('Appears in the footer section of the site.','bolt'),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h4>',
      'after_title' => '</h4>',
    ));
    register_sidebar(array(
      'name' => esc_html__('Footer Widget Area 4', 'bolt'),
      'id' => 'footer-sidebar-4',
      'description' => esc_html__('Appears in the footer section of the site.', 'bolt'),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h4>',
      'after_title' => '</h4>',
    ));
    register_sidebar(array(
      'name' => esc_html__('Footer Widget Area 5', 'bolt'),
      'id' => 'footer-sidebar-5',
      'description' => esc_html__('Appears in the footer section of the site.', 'bolt'),
      'before_widget' => '<aside id="%1$s" class="widget %2$s">',
      'after_widget' => '</aside>',
      'before_title' => '<h4>',
      'after_title' => '</h4>',
    ));

  }


  function magikBolt_reg_page_meta_box() {
    $screens = array('page');

    foreach ($screens as $screen) {        
      add_meta_box(
          'magikBolt_page_layout_meta_box', esc_html__('Page Layout', 'bolt'), 
          array($this, 'magikBolt_page_layout_meta_box_cb'), $screen, 'normal', 'core'
      );
    }
  }

  function magikBolt_page_layout_meta_box_cb($post) {

    $saved_page_layout = get_post_meta($post->ID, 'magikBolt_page_layout', true);
    
    $show_breadcrumb = get_post_meta($post->ID, 'magikBolt_show_breadcrumb', true);
    
   if(empty($saved_page_layout)) {
      $saved_page_layout = 3;
    }
    $page_layouts = array(
      1 => esc_url(MAGIKBOLT_THEME_URI).'/images/magik_col/category-layout-1.png',
      2 => esc_url(MAGIKBOLT_THEME_URI).'/images/magik_col/category-layout-2.png',
      3 => esc_url(MAGIKBOLT_THEME_URI).'/images/magik_col/category-layout-3.png',
      4 => esc_url(MAGIKBOLT_THEME_URI).'/images/magik_col/category-layout-4.png',
    );  
    ?>

  <?php
    echo "<input type='hidden' name='magikBolt_page_layout_verifier' value='".wp_create_nonce('magikBolt_7a81jjde')."' />";    
    $output = '<div class="tile_img_wrap">';
      foreach ($page_layouts as $key => $img) {
        $checked = '';
        $selectedClass = '';
        if($saved_page_layout == $key){
          $checked = 'checked="checked"';
          $selectedClass = 'of-radio-img-selected';
        }
        $output .= '<span>';
        $output .= '<input type="radio" class="checkbox of-radio-img-radio" value="' . absint($key) . '" name="magikBolt_page_layout" ' . esc_html($checked). ' />';            
        $output .= '<img src="' . esc_url($img) . '" alt="" class="of-radio-img-img ' . esc_html($selectedClass) . '" />';
        $output .= '</span>';
            
      }    
    $output .= '</div>';
    echo htmlspecialchars_decode($output);
    ?>
 

  <h2><?php esc_attr_e('Show breadcrumb', 'bolt'); ?></h2>
  <p>
    <input type="radio" name="magikBolt_show_breadcrumb" value="1" <?php echo "checked='checked'"; ?> />
    <label><?php esc_attr_e('Yes','bolt'); ?></label>
    &nbsp;
    <input type="radio" name="magikBolt_show_breadcrumb" value="0"  <?php if($show_breadcrumb === '0'){ echo "checked='checked'"; } ?>/>
    <label><?php esc_attr_e('No', 'bolt'); ?></label>
  </p>
  <?php
  }

  function magikBolt_save_page_layout_meta_box_values($post_id){
    if (!isset($_POST['magikBolt_page_layout_verifier']) 
        || !wp_verify_nonce($_POST['magikBolt_page_layout_verifier'], 'magikBolt_7a81jjde') 
        || !isset($_POST['magikBolt_page_layout']) 
       
        )
      return $post_id;
    
    
    add_post_meta($post_id,'magikBolt_page_layout',sanitize_text_field( $_POST['magikBolt_page_layout']),true) or 
    update_post_meta($post_id,'magikBolt_page_layout',sanitize_text_field( $_POST['magikBolt_page_layout']));
    
    add_post_meta($post_id,'magikBolt_show_breadcrumb',sanitize_text_field( $_POST['magikBolt_show_breadcrumb']),true) or 
    update_post_meta($post_id,'magikBolt_show_breadcrumb',sanitize_text_field( $_POST['magikBolt_show_breadcrumb']));  
  }


  /*Register Post Meta Boxes for Blog Post Layouts*/

    function magikBolt_reg_post_meta_box() {
    $screens = array('post');

    foreach ($screens as $screen) {        
      add_meta_box(
          'magikBolt_post_layout_meta_box', esc_html__('Post Layout', 'bolt'), 
          array($this, 'magikBolt_post_layout_meta_box_cb'), $screen, 'normal', 'core'
      );
    }
  }

  function magikBolt_post_layout_meta_box_cb($post) {

    $saved_post_layout = get_post_meta($post->ID, 'magikBolt_post_layout', true);         
    if(empty($saved_post_layout))
    {
      $saved_post_layout = 2;
    }
    
    $post_layouts = array(
      1 => esc_url(MAGIKBOLT_THEME_URI).'/images/magik_col/category-layout-1.png',
      2 => esc_url(MAGIKBOLT_THEME_URI).'/images/magik_col/category-layout-2.png',
      3 => esc_url(MAGIKBOLT_THEME_URI).'/images/magik_col/category-layout-3.png',
      
    );  
    ?>

  <?php
    echo "<input type='hidden' name='magikBolt_post_layout_verifier' value='".wp_create_nonce('magikBolt_7a81jjde1')."' />";    
    $output = '<div class="tile_img_wrap">';
      foreach ($post_layouts as $key => $img) {
        $checked = '';
        $selectedClass = '';
        if($saved_post_layout == $key){
          $checked = 'checked="checked"';
          $selectedClass = 'of-radio-img-selected';
        }
        $output .= '<span>';
        $output .= '<input type="radio" class="checkbox of-radio-img-radio" value="' . absint($key) . '" name="magikBolt_post_layout" ' . esc_html($checked). ' />';            
        $output .= '<img src="' . esc_url($img) . '" alt="" class="of-radio-img-img ' . esc_html($selectedClass) . '" />';
        $output .= '</span>';
            
      }    
    $output .= '</div>';
    echo htmlspecialchars_decode($output);
   
  }

  function magikBolt_save_post_layout_meta_box_values($post_id){
    if (!isset($_POST['magikBolt_post_layout_verifier']) 
        || !wp_verify_nonce($_POST['magikBolt_post_layout_verifier'], 'magikBolt_7a81jjde1') 
        || !isset($_POST['magikBolt_post_layout']) 
       
        )
      return $post_id;
    
    
    add_post_meta($post_id,'magikBolt_post_layout',sanitize_text_field($_POST['magikBolt_post_layout']),true) or 
    update_post_meta($post_id,'magikBolt_post_layout',sanitize_text_field($_POST['magikBolt_post_layout']));
    
    
  }

  //custom functions 



// page title code
function magikBolt_page_title() {

    global  $post, $wp_query, $author,$bolt_Options;

    $home = esc_html__('Home', 'bolt');

  
    if ( ( ! is_home() && ! is_front_page() && ! (is_post_type_archive()) ) || is_paged() ) {

        if ( is_home() ) {
           echo htmlspecialchars_decode(single_post_title('', false));

        } else if ( is_category() ) {

            echo esc_html(single_cat_title( '', false ));

        } elseif ( is_tax() ) {

            $current_term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );

            echo htmlspecialchars_decode(esc_html( $current_term->name ));

        }  elseif ( is_day() ) {

            printf( esc_html__( 'Daily Archives: %s', 'bolt' ), get_the_date() );

        } elseif ( is_month() ) {

            printf( esc_html__( 'Monthly Archives: %s', 'bolt' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'bolt' ) ) );

        } elseif ( is_year() ) {

            printf( esc_html__( 'Yearly Archives: %s', 'bolt' ), get_the_date( _x( 'Y', 'yearly archives date format', 'bolt' ) ) );

        }   else if ( is_post_type_archive() ) {
            sprintf( esc_html__( 'Archives: %s', 'bolt' ), post_type_archive_title( '', false ) );
        } elseif ( is_single() && ! is_attachment() ) {
        
                echo esc_html(get_the_title());

            

        } elseif ( is_404() ) {

            echo esc_html__( 'Error 404', 'bolt' );

        } elseif ( is_attachment() ) {

            echo esc_html(get_the_title());

        } elseif ( is_page() && !$post->post_parent ) {

            echo esc_html(get_the_title());

        } elseif ( is_page() && $post->post_parent ) {

            echo esc_html(get_the_title());

        } elseif ( is_search() ) {

            echo htmlspecialchars_decode(esc_html__( 'Search results for &ldquo;', 'bolt' ) . get_search_query() . '&rdquo;');

        } elseif ( is_tag() ) {

            echo htmlspecialchars_decode(esc_html__( 'Posts tagged &ldquo;', 'bolt' ) . single_tag_title('', false) . '&rdquo;');

        } elseif ( is_author() ) {

            $userdata = get_userdata($author);
            echo htmlspecialchars_decode(esc_html__( 'Author:', 'bolt' ) . ' ' . $userdata->display_name);

        } elseif ( ! is_single() && ! is_page() && get_post_type() != 'post' ) {

            $post_type = get_post_type_object( get_post_type() );

            if ( $post_type ) {
                echo htmlspecialchars_decode($post_type->labels->singular_name);
            }

        }

        if ( get_query_var( 'paged' ) ) {
            echo htmlspecialchars_decode( ' (' . esc_html__( 'Page', 'bolt' ) . ' ' . get_query_var( 'paged' ) . ')');
        }
    } else {
        if ( is_home() && !is_front_page() ) {
            if ( ! empty( $home ) ) {               
                  echo htmlspecialchars_decode(single_post_title('', false));
            }
        }
    }
}

// page breadcrumbs code
function magikBolt_breadcrumbs() {
    global $post, $bolt_Options,$wp_query, $author;

    $delimiter = '<span> &frasl; </span>';
    $before = '<li>';
    $after = '</li>';
    $home = esc_html__('Home', 'bolt');
    $linkbefore='<strong>';
    $linkafter='</strong>';

  
  // breadcrumb code
   
    if ( ( ! is_home() && ! is_front_page() && ! (is_post_type_archive()) ) || is_paged() ) {
        echo '<ul>';

        if ( ! empty( $home ) ) {
            echo htmlspecialchars_decode($before . '<a class="home" href="' . esc_url(home_url() ) . '">' . $home . '</a>' . $delimiter . $after);
        }

        if ( is_home() ) {

            echo htmlspecialchars_decode($before .$linkbefore. single_post_title('', false) .$linkafter. $after);

         }      
         else if ( is_category() ) {

            if ( get_option( 'show_on_front' ) == 'page' ) {
                echo htmlspecialchars_decode($before . '<a href="' . esc_url(get_permalink( get_option('page_for_posts' ) )) . '">' . esc_html(get_the_title( get_option('page_for_posts', true) )) . '</a>' . $delimiter . $after);
            }

            $cat_obj = $wp_query->get_queried_object();
            if ($cat_obj) {
                $this_category = get_category( $cat_obj->term_id );
                if ( 0 != $this_category->parent ) {
                    $parent_category = get_category( $this_category->parent );
                    if ( ( $parents = get_category_parents( $parent_category, TRUE, $delimiter . $after . $before ) ) && ! is_wp_error( $parents ) ) {
                        echo htmlspecialchars_decode($before . substr( $parents, 0, strlen($parents) - strlen($delimiter . $after . $before) ) . $delimiter . $after);
                    }
                }
                echo htmlspecialchars_decode($before .$linkbefore. single_cat_title( '', false ) .$linkafter. $after);
            }

        } 
        elseif ( is_tax()) {      
                    
            $current_term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );

            $ancestors = array_reverse( get_ancestors( $current_term->term_id, get_query_var( 'taxonomy' ) ) );

            foreach ( $ancestors as $ancestor ) {
                $ancestor = get_term( $ancestor, get_query_var( 'taxonomy' ) );

                echo htmlspecialchars_decode($before . '<a href="' . esc_url(get_term_link( $ancestor->slug, get_query_var( 'taxonomy' ) )) . '">' . esc_html( $ancestor->name ) . '</a>' . $delimiter . $after);
            }

            echo htmlspecialchars_decode($before .$linkbefore. esc_html( $current_term->name ) .$linkafter. $after);

        } 
       
        elseif ( is_day() ) {

            echo htmlspecialchars_decode($before . '<a href="' . esc_url(get_year_link(get_the_time('Y'))) . '">' . esc_html(get_the_time('Y')) . '</a>' . $delimiter . $after);
            echo htmlspecialchars_decode($before . '<a href="' . esc_url(get_month_link(get_the_time('Y'),get_the_time('m'))) . '">' . esc_html(get_the_time('F')) . '</a>' . $delimiter . $after);
            echo htmlspecialchars_decode($before .$linkbefore. get_the_time('d') .$linkafter. $after);

        } elseif ( is_month() ) {

            echo htmlspecialchars_decode($before . '<a href="' . esc_url(get_year_link(get_the_time('Y'))) . '">' . esc_html(get_the_time('Y')) . '</a>' . $delimiter . $after);
            echo htmlspecialchars_decode($before .$linkbefore. get_the_time('F') .$linkafter. $after);

        } elseif ( is_year() ) {

            echo htmlspecialchars_decode($before .$linkbefore. get_the_time('Y') .$linkafter. $after);

        } elseif ( is_single() && ! is_attachment() ) {

         
            if ( 'post' != get_post_type() ) {
                $post_type = get_post_type_object( get_post_type() );
                $slug = $post_type->rewrite;
                echo htmlspecialchars_decode($before . '<a href="' . esc_url(get_post_type_archive_link( get_post_type() )) . '">' . esc_html($post_type->labels->singular_name) . '</a>' . $delimiter . $after);
                echo htmlspecialchars_decode($before .$linkbefore. get_the_title() .$linkafter. $after);

            } else {

                if ( 'post' == get_post_type() && get_option( 'show_on_front' ) == 'page' ) {
                    echo htmlspecialchars_decode($before . '<a href="' . esc_url(get_permalink( get_option('page_for_posts' ) )) . '">' . esc_html(get_the_title( get_option('page_for_posts', true) )) . '</a>' . $delimiter . $after);
                }

                $cat = current( get_the_category() );
              if ( ( $parents = get_category_parents( $cat, TRUE, $delimiter . $after . $before ) ) && ! is_wp_error( $parents ) ) {
                $getitle=get_the_title();
                  if(empty($getitle))
                  {
                    $newdelimiter ='';
                  }
                  else
                  {
                     $newdelimiter=$delimiter;
                  }
                    echo htmlspecialchars_decode($before . substr( $parents, 0, strlen($parents) - strlen($delimiter . $after . $before) ) . $newdelimiter . $after);
                }
                echo htmlspecialchars_decode($before .$linkbefore. get_the_title() .$linkafter. $after);

            }

        } elseif ( is_404() ) {

            echo htmlspecialchars_decode($before .$linkbefore. esc_html__( 'Error 404', 'bolt' ) .$linkafter. $after);

        } elseif ( is_attachment() ) {

            $parent = get_post( $post->post_parent );
            $cat = get_the_category( $parent->ID );
            $cat = $cat[0];
            if ( ( $parents = get_category_parents( $cat, TRUE, $delimiter . $after . $before ) ) && ! is_wp_error( $parents ) ) {
                echo htmlspecialchars_decode($before . substr( $parents, 0, strlen($parents) - strlen($delimiter . $after . $before) ) . $delimiter . $after);
            }
            echo htmlspecialchars_decode($before . '<a href="' . esc_url(get_permalink( $parent )) . '">' . esc_html($parent->post_title) . '</a>' . $delimiter . $after);
            echo htmlspecialchars_decode($before .$linkbefore. get_the_title() .$linkafter. $after);

        } elseif ( is_page() && !$post->post_parent ) {

            echo htmlspecialchars_decode($before .$linkbefore. get_the_title() .$linkafter. $after);

        } elseif ( is_page() && $post->post_parent ) {

            $parent_id  = $post->post_parent;
            $breadcrumbs = array();

            while ( $parent_id ) {
                $page = get_post( $parent_id );
                $breadcrumbs[] = '<a href="' . esc_url(get_permalink($page->ID)) . '">' . esc_html(get_the_title( $page->ID )) . '</a>';
                $parent_id  = $page->post_parent;
            }

            $breadcrumbs = array_reverse( $breadcrumbs );

            foreach ( $breadcrumbs as $crumb ) {
                echo htmlspecialchars_decode($before . $crumb . $delimiter . $after);
            }

            echo htmlspecialchars_decode($before .$linkbefore. get_the_title() .$linkafter. $after);

        } elseif ( is_search() ) {

            echo htmlspecialchars_decode($before .$linkbefore. esc_html__( 'Search results for &ldquo;', 'bolt' ) . get_search_query() . '&rdquo;' .$linkafter. $after);

        } elseif ( is_tag() ) {

            if ( 'post' == get_post_type() && get_option( 'show_on_front' ) == 'page' ) {
                echo htmlspecialchars_decode($before . '<a href="' . esc_url(get_permalink( get_option('page_for_posts' ) )) . '">' . esc_html(get_the_title( get_option('page_for_posts', true) )) . '</a>' . $delimiter . $after);
            }

            echo htmlspecialchars_decode($before .$linkbefore. esc_html__( 'Posts tagged &ldquo;', 'bolt' ) . single_tag_title('', false) . '&rdquo;' .$linkafter. $after);

        } elseif ( is_author() ) {

            $userdata = get_userdata($author);
            echo htmlspecialchars_decode($before .$linkbefore. esc_html__( 'Author:', 'bolt' ) . ' ' . $userdata->display_name .$linkafter. $after);

        } elseif ( ! is_single() && ! is_page() && get_post_type() != 'post' ) {

            $post_type = get_post_type_object( get_post_type() );

            if ( $post_type ) {
                echo htmlspecialchars_decode($before .$linkbefore. $post_type->labels->singular_name .$linkafter. $after);
            }

        }

        if ( get_query_var( 'paged' ) ) {
            echo htmlspecialchars_decode($before .$linkbefore. '&nbsp;(' . esc_html__( 'Page', 'bolt' ) . ' ' . get_query_var( 'paged' ) . ')' .$linkafter. $after);
        }

        echo '</ul>';
    } else { 
        if ( is_home() && !is_front_page() ) {
            echo '<ul>';

            if ( ! empty( $home ) ) {
                echo htmlspecialchars_decode($before . '<a class="home" href="' . esc_url(home_url()) . '">' . $home . '</a>' . $delimiter . $after);

               
                echo htmlspecialchars_decode($before .$linkbefore. single_post_title('', false) .$linkafter. $after);
            }

            echo '</ul>';
        }
    }
}
  


  function magikBolt_getPostViews($postID)
  {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ($count == '') {
      delete_post_meta($postID, $count_key);
      add_post_meta($postID, $count_key, '0');
      return "0  View";
    }
    return $count . '  Views';
  }

  function magikBolt_setPostViews($postID)
  {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ($count == '') {
      $count = 0;
      delete_post_meta($postID, $count_key);
      add_post_meta($postID, $count_key, '0');
    } else {
      $count++;
      update_post_meta($postID, $count_key, $count);
    }
  }


  function magikBolt_is_blog() {
    global  $post;
    $posttype = get_post_type($post );
    return ( ((is_archive()) || (is_author()) || (is_category()) || (is_home()) || (is_single()) || (is_tag())) && ( $posttype == 'post')  ) ? true : false ;
  }

 
 // comment display 
  function magikBolt_comment($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment; ?>

  <li <?php comment_class(); ?> id="comment-<?php comment_ID() ?>">
    <div class="comment-body">
      <div class="img-thumbnail">
        <?php echo get_avatar($comment, 80); ?>
      </div>
      <div class="comment-block">
        <div class="comment-arrow"></div>
        <span class="comment-by">
          <strong><?php echo get_comment_author_link() ?></strong>
          <span class="pt-right">
            <span> <?php edit_comment_link('<i class="fa fa-pencil"></i> ' . esc_html__('Edit', 'bolt'),'  ','') ?></span>
            <span> <?php comment_reply_link(array_merge( $args, array('reply_text' => '<i class="fa fa-reply"></i> ' . esc_html__('Reply', 'bolt'), 'add_below' => 'comment', 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?></span>
          </span>
        </span>
        <div>
          <?php if ($comment->comment_approved == '0') : ?>
            <em><?php echo esc_html__('Your comment is awaiting moderation.', 'bolt') ?></em>
            <br />
          <?php endif; ?>
          <?php comment_text() ?>
        </div>
        <span class="date pt-right"><?php printf(esc_html__('%1$s at %2$s', 'bolt'), get_comment_date(),  get_comment_time()) ?></span>
      </div>
    </div>
  </li>
  <?php }

  //css manage by admin
  function magikBolt_enqueue_custom_css() {
    global $bolt_Options;
  
     wp_enqueue_style(
        'magikBolt-custom-style',esc_url(MAGIKBOLT_THEME_URI) . '/css/custom.css'
    );

   $custom_css='';
    ?>
   
      <?php if(isset($bolt_Options['opt-color-rgba']) &&  !empty($bolt_Options['opt-color-rgba'])) {
        $custom_css=
      ".mgk-main-menu {
        background-color: ". esc_html($bolt_Options['opt-color-rgba'])." !important;
       }";  
      }

     
      if(isset($bolt_Options['footer_color_scheme']) && $bolt_Options['footer_color_scheme']) {
      if(isset($bolt_Options['footer_copyright_background_color']) && !empty($bolt_Options['footer_copyright_background_color'])) {
      
       $custom_css.=".footer-bottom {
        background-color: ". esc_html($bolt_Options['footer_copyright_background_color'])." !important }";
       }
       

       if(isset($bolt_Options['footer_copyright_font_color']) && !empty($bolt_Options['footer_copyright_font_color'])) {
      $custom_css.=".coppyright {
        color: ". esc_html($bolt_Options['footer_copyright_font_color'])." !important;}";    
       }     
       }
       

      if (isset($bolt_Options['theme_layout']) && $bolt_Options['theme_layout']=='version3') {
        if(isset($bolt_Options['header_breadcrumb']) && !empty($bolt_Options['header_breadcrumb']['url'])) {
      $custom_css.=".page-heading{
         background-image:url('".esc_url($bolt_Options['header_breadcrumb']['url'])."')}" ;      
      }
      }
       

       if(isset($bolt_Options['theme_layout']) && $bolt_Options['theme_layout']=='version3') {
      if(isset($bolt_Options['home_offer_slider_image']) && !empty($bolt_Options['home_offer_slider_image']['url'])) {
       $custom_css.=".parallax-2{
         background-image:url('".esc_url($bolt_Options['home_offer_slider_image']['url'])."')}"; }
       }

        wp_add_inline_style( 'magikBolt-custom-style', $custom_css );
  }

}

// Instantiate theme
$MagikBolt = new MagikBolt();

?>