<?php
/**
 * Single Product tabs
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
  exit;
}

/**
 * Filter tabs and allow third parties to add their own
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */

global $bolt_Options;

$tabs = apply_filters('woocommerce_product_tabs', array());

if (!empty($tabs)) : ?>
<div class="add_info">
<div class="woocommerce-tabs">
    <div class="tabs">
       <ul class="tabs nav nav-tabs product-tabs" id="product-detail-tab">
        <?php foreach ($tabs as $key => $tab) : ?>
        <li class="<?php echo $key ?>_tab">
         <a href="#tab-<?php echo $key ?>">
 <?php  if (isset($bolt_Options['theme_layout']) && $bolt_Options['theme_layout']=='version3')
      {     
    ?>
          <span><?php echo apply_filters('woocommerce_product_' . $key . '_tab_title', $tab['title'], $key) ?></span>
    <?php } 


    else{ ?>
   <?php echo apply_filters('woocommerce_product_' . $key . '_tab_title', $tab['title'], $key) ?>
   <?php  } ?>
        </a> </li>
        <?php endforeach; ?>
       </ul>
    </div>

<div id="productTabContent" class="tab-content">
  <?php foreach ($tabs as $key => $tab) : ?>
    <div class="panel entry-content" id="tab-<?php echo $key ?>">
       <?php call_user_func($tab['callback'], $key, $tab) ?>
    </div>
  <?php endforeach; ?>
</div>
</div>
</div>
<?php endif; ?>

</div>  
      </div>
   </div>
 </div>
</div>
