<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly
}



$MagikBolt = new MagikBolt();
global $product, $woocommerce_loop, $yith_wcwl,$bolt_Options;


 $shop_design = isset($_GET['layout']) ? $_GET['layout'] : '';
if (!in_array($shop_design, array('full','left','right'))) {
  
$layout_array=array(1=>"left",2=>"right",3=>"full");  
$page_id = wc_get_page_id('shop');
$design = get_post_meta($page_id ,'magikBolt_page_layout', true);

if(isset($design) && !empty($design))
{
$shop_design = $layout_array[$design];
}
else
{
$shop_design="left"; 
}
}


// Increase loop count
//$woocommerce_loop['loop']++;

// Extra post classes
$classes = array();
if (is_cart()) {

    $classes[] = 'item col-md-3 col-sm-6 col-xs-12';

} else {
  
   if($shop_design=="full")
   {
    $classes[] = 'item col-lg-3 col-md-3 col-sm-3 col-xs-3';
   }
   else{
$classes[] = 'item col-lg-4 col-md-4 col-sm-4 col-xs-6';
   }
}





?>
<?php  if (isset($bolt_Options['theme_layout']) && $bolt_Options['theme_layout']=='version4' )
 {  /*version4*/  ?>

<li <?php wc_product_class($classes); ?>>
   <div class="item-inner">
      <div class="item-img">
         <div class="item-img-info">

          <div class="pimg">
            <?php do_action('woocommerce_before_shop_loop_item'); ?>        
              <a href="<?php the_permalink(); ?>" class="product-image">
                  <?php
                     /**
                      * woocommerce_before_shop_loop_item_title hook
                      *
                      * @hooked woocommerce_show_product_loop_sale_flash - 10
                      * @hooked woocommerce_template_loop_product_thumbnail - 10
                      */
                     do_action('woocommerce_before_shop_loop_item_title');
                     ?>
              </a>
               <?php if ($product->is_on_sale()) : ?>
                     <div class="sale-label sale-top-right">
                         <?php esc_attr_e('Sale', 'bolt'); ?>
                     </div>
                 <?php endif; ?>

              <?php if (class_exists('YITH_WCQV_Frontend')) { ?>
                    <a class="yith-wcqv-button link-quickview quickview-btn" href="#"
                      data-product_id="<?php echo esc_html($product->get_id()); ?>"><span><?php esc_attr_e('Quick View', 'bolt'); ?></span></a>
                  <?php } ?>

            </div> 
          </div>
       </div>

    <div class="item-info">

      <div class="info-inner">
         <div class="item-title"> 
           <a href="<?php the_permalink(); ?>">
              <?php the_title(); ?></a>
         </div>
       
       <div class="item-content">

         <div class="rating">
           <div class="ratings">
             <div class="rating-box">
                 <?php $average = $product->get_average_rating(); ?>
                  <div style="width:<?php echo esc_html(($average / 5) * 100); ?>%" class="rating"></div>
              </div>
             </div>
           </div>
            
          <div class="item-price">
             <div class="price-box">
                <?php echo htmlspecialchars_decode($product->get_price_html()); ?>
             </div>
          </div>
             
        <div class="desc std">
            <?php echo apply_filters('woocommerce_short_description', $post->post_excerpt) ?>
        </div>

        <div class="actions">
          

                  <?php if (isset($yith_wcwl) && is_object($yith_wcwl)) {
                      $classes = get_option('yith_wcwl_use_button') == 'yes' ? 'class="add_to_wishlist link-wishlist"' : 'class="add_to_wishlist link-wishlist"';
                  ?>
                  <a title="<?php esc_attr_e('Add to Wishlist', 'bolt'); ?>" href="<?php echo esc_url(add_query_arg('add_to_wishlist', $product->get_id())) ?>"  data-product-id="<?php echo esc_html($product->get_id()); ?>"
                    data-product-type="<?php echo esc_html($product->get_type()); ?>" <?php echo htmlspecialchars_decode($classes); ?>><span><?php esc_attr_e('Add to Wishlist', 'bolt'); ?></span></a> 
                  <?php }?> 
                
                    <?php
                  /**
                   * woocommerce_after_shop_loop_item hook
                   *
                   * @hooked woocommerce_template_loop_add_to_cart - 10
                   */
                  do_action('woocommerce_after_shop_loop_item');
                  
                  ?>
                
                   <?php if (class_exists('YITH_Woocompare_Frontend')) {
                    $mgk_yith_cmp = new YITH_Woocompare_Frontend; 
                    ?>      
                    <a title="<?php esc_attr_e('Add to Compare', 'bolt'); ?>" href="<?php echo esc_url($mgk_yith_cmp->add_product_url($product->get_id())); ?>" class="compare link-compare add_to_compare" data-product_id="<?php echo esc_html($product->get_id()); ?>"><span><?php esc_attr_e('Add to Compare', 'bolt'); ?></span></a>
                   <?php } ?>


          </div>

        </div>
      </div>
    </div>
   </div>
</li>

<?php } elseif (isset($bolt_Options['theme_layout']) && $bolt_Options['theme_layout']=='version3' )
 {  /*version3*/  ?>

<?php 
  
  if (is_cart()) {

    $classes[] = 'item col-lg-2 col-md-4 col-sm-4 col-xs-6';

} else {
    
  $classes[] = 'item col-lg-3 col-md-4 col-sm-6 col-xs-6';
       
}
?>
 <li <?php wc_product_class($classes); ?>>
   <div class="item-inner">
      <div class="item-img">
         <div class="item-img-info">

          <div class="pimg">
            <?php do_action('woocommerce_before_shop_loop_item'); ?>        
              <a href="<?php the_permalink(); ?>" class="product-image">
                  <?php
                     /**
                      * woocommerce_before_shop_loop_item_title hook
                      *
                      * @hooked woocommerce_show_product_loop_sale_flash - 10
                      * @hooked woocommerce_template_loop_product_thumbnail - 10
                      */
                     do_action('woocommerce_before_shop_loop_item_title');
                     ?>
              </a>
               <?php if ($product->is_on_sale()) : ?>
                     <div class="sale-label sale-top-right">
                         <?php esc_attr_e('Sale', 'bolt'); ?>
                     </div>
                 <?php endif; ?>

            </div> 
              
            <div class="actions">

                      <?php if (class_exists('YITH_WCQV_Frontend')) { ?>
                            <div class="quick-view-btn">
                                   <a class="yith-wcqv-button" href="#"
                                    data-toggle="tooltip" data-placement="right" title="" data-original-title="<?php esc_attr_e('Quick View', 'bolt'); ?>"
                                    data-product_id="<?php echo esc_html($product->get_id()); ?>"><span><?php esc_attr_e('Quick View', 'bolt'); ?></span></a>
                            </div>
                     <?php } ?>
                  


                      <?php if (isset($yith_wcwl) && is_object($yith_wcwl)) {
                            $classes = get_option('yith_wcwl_use_button') == 'yes' ? 'class="add_to_wishlist link-wishlist"' : 'class="add_to_wishlist link-wishlist"'; ?>

                           <div class="add_to_wishlist link-wishlist">
                            <a href="<?php echo esc_url(add_query_arg('add_to_wishlist', $product->get_id())) ?>" data-toggle="tooltip" data-placement="right" title="<?php esc_attr_e('Wishlist', 'bolt'); ?>" data-product-id="<?php echo esc_html($product->get_id()); ?>" data-product-type="<?php echo esc_html($product->get_type()); ?>" <?php echo htmlspecialchars_decode($classes); ?>
                                ><span><?php esc_attr_e('Add to Wishlist', 'bolt'); ?></span></a>
                          </div>

                      <?php } ?>


                      <?php if (class_exists('YITH_Woocompare_Frontend')) {
                             $mgk_yith_cmp = new YITH_Woocompare_Frontend; ?> 

                            <div class="link-compare">
                              <a href="<?php echo esc_url($mgk_yith_cmp->add_product_url($product->get_id())); ?>" data-toggle="tooltip" data-placement="right" title="" data-original-title="<?php esc_attr_e('Compare', 'bolt'); 
                              ?>" class="compare  add_to_compare" data-product_id="<?php echo esc_html($product->get_id()); ?>"><span><?php esc_attr_e('Add to Compare', 'bolt'); 
                              ?></span></a>
                             </div>

                      <?php  } ?> 

                       <?php
                  /**
                   * woocommerce_after_shop_loop_item hook
                   *
                   * @hooked woocommerce_template_loop_add_to_cart - 10
                   */
                  do_action('woocommerce_after_shop_loop_item');
                  
                  ?>  

              </div>


                <div class="rating">
                    <div class="ratings">
                       <div class="rating-box">
                         <?php $average = $product->get_average_rating(); ?>
                         <div style="width:<?php echo esc_html(($average / 5) * 100); ?>%" class="rating"></div>
                       </div>
                     </div>
                </div>

          </div>
       </div>

    <div class="item-info">

      <div class="info-inner">
         <div class="item-title"> 
           <a href="<?php the_permalink(); ?>">
              <?php the_title(); ?></a>
         </div>
       
       <div class="item-content">
            
          <div class="item-price">
             <div class="price-box">
                <?php echo htmlspecialchars_decode($product->get_price_html()); ?>
             </div>
          </div>
             
        <div class="desc std">
            <?php echo apply_filters('woocommerce_short_description', $post->post_excerpt) ?>
        </div>

        

        </div>
      </div>
    </div>
   </div>
</li>


 <?php } else { /*default and version2*/ ?>

   <?php
  if (isset($bolt_Options['theme_layout']) && $bolt_Options['theme_layout']=='version2')
 {   
  
  if (is_cart()) {

    $classes[] = 'item col-lg-3 col-md-3 col-sm-4 col-xs-6';

} else {
    
  $classes[] = 'item col-lg-3 col-md-4 col-sm-4 col-xs-6';
       
}
}
else{
  if (is_cart()) {

    $classes[] = 'item col-lg-3 col-md-3 col-sm-4 col-xs-6';

} else {
    
  $classes[] = 'item col-lg-4 col-md-4 col-sm-4 col-xs-6';
       
}

}
?>
<li <?php wc_product_class($classes); ?>>
    <div class="item-inner">
      <div class="item-img">
        <div class="item-img-info">

          <div class="pimg">
            <?php do_action('woocommerce_before_shop_loop_item'); ?>        
              <a href="<?php the_permalink(); ?>" class="product-image">
                  <?php
                     /**
                      * woocommerce_before_shop_loop_item_title hook
                      *
                      * @hooked woocommerce_show_product_loop_sale_flash - 10
                      * @hooked woocommerce_template_loop_product_thumbnail - 10
                      */
                     do_action('woocommerce_before_shop_loop_item_title');
                     ?>
              </a>
               <?php if ($product->is_on_sale()) : ?>
                     <div class="sale-label sale-top-right">
                         <?php esc_attr_e('Sale', 'bolt'); ?>
                     </div>
                 <?php endif; ?>
          </div>


        <div class="box-hover">
            <ul class="add-to-links">
                      <li>
                                    <?php if (class_exists('YITH_WCQV_Frontend')) { ?>
                                       <a class="yith-wcqv-button link-quickview" href="#"
                                        data-product_id="<?php echo esc_html($product->get_id()); ?>"><?php esc_attr_e('Quick View', 'bolt'); ?></a>
                                     <?php } ?>
                      </li>
                      <li>
                                        <?php
                               if (isset($yith_wcwl) && is_object($yith_wcwl)) {
                            $classes = get_option('yith_wcwl_use_button') == 'yes' ? 'class="add_to_wishlist link-wishlist"' : 'class="add_to_wishlist link-wishlist"';
                            ?>
                            <a href="<?php echo esc_url(add_query_arg('add_to_wishlist', $product->get_id())) ?>"  data-product-id="<?php echo esc_html($product->get_id()); ?>"
                              data-product-type="<?php echo esc_html($product->get_type()); ?>" <?php echo htmlspecialchars_decode($classes); ?>
                                ><?php esc_attr_e('Wishlist', 'bolt'); ?></a> 
                             <?php
                               }
                               ?>
                    </li>
                    <li>
                              <?php if (class_exists('YITH_Woocompare_Frontend')) {
                               $mgk_yith_cmp = new YITH_Woocompare_Frontend; ?>      
                              <a href="<?php echo esc_url($mgk_yith_cmp->add_product_url($product->get_id())); ?>" class="compare link-compare add_to_compare" data-product_id="<?php echo esc_html($product->get_id()); ?>"
                                ><?php esc_attr_e('Compare', 'bolt'); 
                              ?></a>
                              <?php
                              }
                             ?> 
                    </li>
                </ul>
            </div>
          </div>
        </div>
      <div class="item-info">
          <div class="info-inner">

              <div class="item-title">
               <a href="<?php the_permalink(); ?>">
               <?php the_title(); ?></a>
               </div>

                <div class="item-content">

                   <div class="rating">
                        <div class="ratings">
                            <div class="rating-box">
                               <?php $average = $product->get_average_rating(); ?>
                             <div style="width:<?php echo esc_html(($average / 5) * 100); ?>%" class="rating"></div>
                            </div>
                        </div>
                    </div>

                    <div class="item-price">
                      <div class="price-box">
                          <?php echo htmlspecialchars_decode($product->get_price_html()); ?>
                      </div>
                    </div>

                     <div class="desc std">
                         <?php echo apply_filters('woocommerce_short_description', $post->post_excerpt) ?>
                    </div>
                    
                    <div class="action">
                       <?php
                  /**
                   * woocommerce_after_shop_loop_item hook
                   *
                   * @hooked woocommerce_template_loop_add_to_cart - 10
                   */
                  do_action('woocommerce_after_shop_loop_item');
                  
                  ?>
                    </div>
                </div>
              </div>
          </div>
      </div>
  </li>

<?php } ?>