<!DOCTYPE html>
<html <?php language_attributes(); ?> id="parallax_scrolling">
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
 <?php wp_head(); ?>
</head>
<?php
 $MagikBolt = new MagikBolt(); ?>
<body <?php body_class(); ?> >
<div id="page" class="page catalog-category-view">
  <header>
    <div class="header-container">

      <?php magikBolt_header_banner(); ?>
      
      <div class="header-top">
        <div class="container">
          <div class="row"> 

            <div class="col-xs-12 col-sm-6">
              <?php echo magikBolt_currency_language(); ?>

              <div class="welcome-msg hidden-xs">  
               <?php echo magikBolt_msg(); ?>
              </div>
            </div>

            <div class="col-xs-6 hidden-xs"> 
              <div class="toplinks">
                  <?php if ( has_nav_menu( 'toplinks' ) )
                    { ?>
                         <div class="links">
                           <?php echo magikBolt_top_navigation(); ?>
                         </div>
                    <?php  }?>
              </div>
            </div>

          </div>
        </div>
      </div>

      <div class="header-m">
        <div class="container">
          <div class="row">

            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 hidden-xs">
                 <?php echo magikBolt_search_form(); ?>  
            </div>

            <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12 logo-block">

                <div class="mm-toggle-wrap">
                  <div class="mm-toggle">  <a class="mobile-toggle"><i class="fa fa-align-justify"></i><span class="mm-label"><?php esc_attr_e('Menu', 'bolt'); ?></span> </a></div>
          </div>

              <div class="logo">
               <?php magikBolt_logo_image();?>
              </div>

            </div>

            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
              <?php
              if (class_exists('WooCommerce')) :?>
              <div class="top-cart-contain pull-right"> 
                  <?php  magikBolt_mini_cart(); ?>
               
              </div>
              <?php endif;?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  <!-- end header -->
<nav>
    <div class="container">
      <div class="nav-inner"> 
      

         <div class="mgk-main-menu">
              <div id="main-menu">
           <?php if(has_nav_menu('main_menu'))
                  { ?> 

                  <?php echo magikBolt_main_menu(); ?>                        
            
            <?php  }?>
         </div>
    </div>
      </div>
    </div>
  </nav>

 
  <?php if (class_exists('WooCommerce') && is_woocommerce()) : ?>
    
    <div class="breadcrumbs">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
                  <?php woocommerce_breadcrumb(); ?>
              </div>
          <!--col-xs-12--> 
        </div>
        <!--row--> 
      </div>
      <!--container--> 
    </div>
           <?php endif; ?>