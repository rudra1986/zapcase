===Shiprocket=== 

Contributors: Shiprocket
donate link: shiprocket.in
Tags: Shipping, E-commerce shipping, shipping solutions, shipping module in woo commerce,shipping plugin woocommerce,woocommerce pickup shipping,woocommerce usps shipping extension,woo commerce shipping module,custom shipping module woo commerce,woo commerce shipping methods,woocommerce shipping by postcode,woocommerce create shipping module,woocommerce logistics integration,woo commerce custom shipping method,fedex woocommerce,woo commerce per product shipping,shipment tracking woocommerce,woocommerce freight service,woocommerce shipping methods not showing,woocommerce auto select shipping method,woocommerce ecom express,woocommerce delivery service,woocommerce delivery integration,woo commerce aramex,woo commerce shipping method per product,woocommerce logistic,shipping per product woocommerce,woocommerce logistic service,woocommerce logistics service,shipping module in woocommerce,woocommerce multiple shipping methods,woocommerce cash delivery,woo commerce shipping method extension, Shipping india
Requires at least: 2.2
Tested up to: 4.9
Stable tag: 4.3
License: GPLv3

Auto Sync your woocommerce store orders & ship them at lowest shipping rates. Automate your shipping, save time & money.

==Description==
<strong>About ShipRocket</strong>
<p>The ShipRocket-WooCommerce shipping plugin brings all your logistics, fulfilment and inventory management for your online business on one platform. It reduces your shipment costs, connects with reliable multiple courier partners, gives multiple shipping options with order tracking, import inventory and orders from various sales channels. Simplify your eCommerce shipping today.</p>
<a href="https://www.shiprocket.in/features/"><strong>Features</strong></a> |
<a href="https://www.shiprocket.in/pricing/"><strong>Pricing</strong></a> |
<a href="https://www.shiprocket.in/how-do-i-integrate-woocommerce-with-shiprocket/"><strong>FAQ's</strong></a> |
<a href="https://www.shiprocket.in/contact/"><strong>Contact Us</strong></a>
<p>With ShipRocket’s shipping extension, you can start delivering products in absolutely no time with features like - cash on delivery or pre-paid, in a most cost efficient and secure manner. ShipRocket is considered as one of the best logistics companies for eCommerce in India. You can integrate your WooCommerce website with its shipping plugin for FREE.</p>
<strong>How to integrate your Woocommerce store with ShipRocket?</strong>

<p>Integrate your eCommerce store with our platform and streamline Woocommerce shipping and order fulfilment process now. Proceed with the following steps to connect your online business store with our shipping plugin for best logistics experience in India.</p>
<strong>USING THE WOOCOMMERCE ADMIN PANEL</strong>
<p>1. Login to the Woocommerce Admin Panel <br>
2. Goto WooCommerce Tab-> Settings -> API -> Keys/Apps -> Click “Add Key” Button. <br>
3. In Key Details put Description as “Shiprocket” & Set Permission to “Read/Write”.<br>
4. Click “Generate API key”.<br>
5. Here, you will get Consumer Key & Consumer Secret for WooCommerce. Copy them.</p>
<strong>USING THE SHIPROCKET PANEL</strong>
<p>1. Login to the ShipRocket panel.
2. Goto Settings – Channels.
3. Click on “Add New Channel” Button.
4. Click on WooCommerce -> Integrate.
5. Switch “On” the Order and Inventory Sync.
6. Fill in the Parameters as saved from the WooCommerce Panel.
7. Click on ‘Save Channel & Test Connection’.
8. The Green Icon indicates that the channel has been successfully configured. Congratulations. You successfully integrated your Woocommerce Store with ShipRocket.<br>
</p>
<strong>Key feature of the Woocommerce shipping plugin:</strong>
<p><strong>1. Automatic Order Sync </strong>– Integrating your eCommerce store with the ShipRocket shipping panel allows you to automatically sync your orders from the Woocommerce panel into the system, with just one click. You would get access to the complete order data, including buyer’s address & product details on the panel.</p>

<p><strong>2. Automatic Shipping Status Sync</strong> – For the Woocommerce store’s products shipped to the customers via the ShipRocket panel, the status will automatically be updated on Woocommerce.</p>

<p><strong>3. Catalog & inventory Sync</strong> – All the active products on the Woocommerce panel will automatically be fetched into the system, and correspondingly the stock count for products on Woocommerce can be managed from ShipRocket Panel.<br></p>

==Installation==
1. Unzip and upload the plugin’s folder to your /wp-content/plugins/ directory
2. Activate the extension through the ‘Plugins’ menu in WordPress
3. Go to WooCommerce > Shiprocket to use the plugin

==ScreenShots==
1. Step A:<br>
(a) Login to WooCommerce admin panel.
2. (b) Goto WooCommerce Tab-> Settings -> API -> Keys/Apps -> Click “Add Key” Button.
3. (c) In Key Details put Description as “Shiprocket”  & Set Permission to “Read/Write”.<br>
(d) Click “Generate API key”
4. (e) Here, you will get Consumer Key & Consumer Secret for WooCommerce. Copy them.
5. Step B:<br>
(a) Login to ShipRocket panel.<br>
(b) Goto Settings – Channels.<br>
(c) Click on “Add New Channel” Button.
6. (d) Switch “On” the Order and Inventory Sync.<br>
(e) Fill in the Parameters as saved from the WooCommerce Panel.
7. (f) Click “Save Channel & Test Connection”
8. (g)Green icon indicates that the channel have been successfully configured.