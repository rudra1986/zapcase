<?php
/**
 * Plugin Name: Shiprocket
 * Plugin URI: https://www.google.com/url?q=https%3A%2F%2Fapp.shiprocket.in%2Fregister%3Futm_source%3DWoocommerce-Marketplace%26utm_medium%3DReferal%26utm_campaign%3DWoocommerce-App-Download&sa=D&sntz=1&usg=AFQjCNFef_e6gw5o_9e6cfge9nylBp6OMg
 * Description: This plugin allows you to ship your orders at cheaper rates by Shiprocket.
 * Version: 1.0.0
 * Author: Shiprocket
 * Author URI: http://shiprocket.in
 */
 
 if ( ! defined( 'ABSPATH' ) ) { 
    exit; // Exit if accessed directly
}

/**
 * Check if WooCommerce is active
 **/
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
    // Put your plugin code here
	add_action('admin_menu', 'register_my_shiprocket_submenu_page');

function register_my_shiprocket_submenu_page() {
    add_submenu_page( 'woocommerce', 'Shiprocket', 'Shiprocket', 'manage_options', 'my-custom-submenu-page', 'my_shiprocket_submenu_page_callback',  plugins_url( 'shiprocket/assests/shiprocket-icon.png' )); 
}

function my_shiprocket_submenu_page_callback() {
	$sample = '<b>Shiprocket Features</b><br /><br />1.   Auto Sync your WooCommerce orders to Shiprocket.<br /><br />2. <b>  Lowest shipping rates -</b> Reduce your shipping cost by 50%.<br /><br />3.   <b>Maximum COD pincodes covered across India -</b> Over 13000+ pin codes.<br /><br />4.   <b>Ship with multiple courier partners –</b> Aramex, FedEx, EcomExpress, Delhivery & Ups.<br /><br />5.   <b>Fully Automated Shipping –</b> Automate your shipping completely.<br /><br />6. <b>   Courier recommendation engine –</b> Choose the best courier partner to ship your orders.<br /><br />7. <b> COD Reconciliation - </b>Automated COD reconciliation and regular remittance.<br /><br /><a href="https://www.google.com/url?q=https%3A%2F%2Fapp.shiprocket.in%2Fregister%3Futm_source%3DWoocommerce-Marketplace%26utm_medium%3DReferal%26utm_campaign%3DWoocommerce-App-Download&sa=D&sntz=1&usg=AFQjCNFef_e6gw5o_9e6cfge9nylBp6OMg" target="_blank">Start saving your shipping cost now.</a>';
	$temp = '<iframe src="https://www.google.com/url?q=https%3A%2F%2Fapp.shiprocket.in%2Fregister%3Futm_source%3DWoocommerce-Marketplace%26utm_medium%3DReferal%26utm_campaign%3DWoocommerce-App-Download&sa=D&sntz=1&usg=AFQjCNFef_e6gw5o_9e6cfge9nylBp6OMg" height="750" width="1500"></iframe>';
    echo $temp;

}
}
